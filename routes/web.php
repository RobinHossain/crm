<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/where', 'TestController@where');

Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::get('/register', 'PublicController@where');
Route::get('/logout', 'AdministratorController@logout');


// Clients Controller
Route::get('/clients', 'UserController@manage');
Route::get('/clients/manage', 'UserController@manage');
Route::get('/clients/add', 'UserController@addAccountGet');
Route::post('/clients/add', 'UserController@addAccountPost');
Route::get('/clients/edit/{id}', 'UserController@editAccountGet');
Route::post('/clients/edit/{id}', 'UserController@editAccountPost');
Route::post('/clients/delete', 'UserController@deleteAccountGet');
Route::get('/clients/groups', 'UserController@clientsGroupList');
Route::get('/clients/groups/add', 'UserController@clientsGroupAddGet');
Route::post('/clients/groups/delete', 'UserController@clientsGroupDeletePost');
Route::get('/clients/groups/edit/{id}', 'UserController@clientsGroupEditGet');
Route::post('/clients/groups/edit/{id}', 'UserController@clientsGroupEditPost');
Route::post('/clients/groups/add', 'UserController@clientsGroupAddPost');
Route::get('/clients/bulk-email', 'UserController@clientsBulkMail');
Route::post('/clients/bulk-email', 'MailController@clientsBulkMailPost');

// Account On User Controller
Route::get('/accounts', 'UserController@allAccounts');
Route::get('/account/add', 'UserController@addAccountGetAcc');
Route::post('/account/add', 'UserController@addAccountPostAcc');
Route::get('/account/edit/{id}', 'UserController@editAccountGetAcc');
Route::post('/account/edit/{id}', 'UserController@editAccountPostAcc');
//Route::edit('/account/delete/{id}', 'UserController@deleteAccountGetAcc');


// Orders Controller
Route::get('/order', 'OrderController@manage');
Route::get('/order/add', 'OrderController@add');
Route::post('/order/add_confirm', 'OrderController@addConfirm');
Route::post('/order/add_confirmed', 'OrderController@addConfirmed');
Route::get('/order/manage/{id}', 'OrderController@manageOrder');
Route::post('/order/manage/accept', 'OrderController@manageOrderStatus');
Route::post('/order/manage/pending', 'OrderController@manageOrderStatus');
Route::post('/order/manage/cancel', 'OrderController@manageOrderStatus');
Route::post('/order/manage/fraud', 'OrderController@manageOrderStatus');
Route::post('/order/manage/delete', 'OrderController@manageOrderStatus');
Route::post('/order/save_note', 'OrderController@manageOrderNote');
Route::get('/order/{name}', 'OrderController@manage');

// Products Controller
Route::get('/products', 'ProductController@index');
Route::get('/product/edit/{id}', 'ProductController@editProduct');
Route::post('/product/edit/{id}', 'ProductController@editProductPost');
Route::get('/product/add', 'ProductController@add');
Route::post('/product/add', 'ProductController@addProduct');
Route::post('/product/delete', 'ProductController@deleteProduct');
Route::get('/product/categories', 'ProductController@getProductCategories');
Route::get('/product/category/add', 'ProductController@getAddCategory');
Route::post('/product/category/add', 'ProductController@postAddCategory');
Route::get('/product/category/edit/{id}', 'ProductController@getEditCategory');
Route::post('/product/category/edit/{id}', 'ProductController@postEditCategory');
Route::post('/product/category/delete', 'ProductController@postDeleteCategory');

// Administrator Controller
Route::get('/administrator', 'AdministratorController@index');
Route::get('/administrator/add', 'AdministratorController@add');
Route::post('/administrator/add', 'AdministratorController@addUser');
Route::post('/administrator/delete', 'AdministratorController@deleteAdministrator');
Route::get('/administrator/edit/{id}', 'AdministratorController@editUser');
Route::post('/administrator/edit/{id}', 'AdministratorController@editUserPost');
Route::get('/administrator/roles', 'AdministratorController@getRoles');
Route::get('/administrator/role/add', 'AdministratorController@roleAdd');
Route::post('/administrator/role/add', 'AdministratorController@roleAddPost');
Route::get('/administrator/role/edit/{id}', 'AdministratorController@editRole');
Route::post('/administrator/role/edit/{id}', 'AdministratorController@editRolePost');
Route::get('/administrator/role/delete/{id}', 'AdministratorController@deleteRoleGet');

// Account Activity
Route::get('/account-activity/{id}', 'AccountController@activityAll');


// Transition
Route::get('/transactions', 'TransactionController@index');
Route::get('/transaction/income', 'TransactionController@income');
Route::post('/transaction/edit', 'TransactionController@editPost');
Route::post('/transaction/delete', 'TransactionController@deletePost');
// Income
Route::get('/transaction/income', 'TransactionController@income');
Route::get('/transaction/income/add', 'TransactionController@incomeAdd');
Route::post('/transaction/income/add', 'TransactionController@incomeAddPost');
// Expense
Route::get('/transaction/expense', 'TransactionController@expense');
Route::get('/transaction/expense/add', 'TransactionController@expenseAdd');
Route::post('/transaction/expense/add', 'TransactionController@expenseAddPost');
// Transfer
Route::get('/transaction/transfer', 'TransactionController@transfer');
Route::get('/transaction/transfer/add', 'TransactionController@transferAdd');
Route::post('/transaction/transfer/add', 'TransactionController@transferAddPost');

// Balance Sheet
Route::get('/balance-sheet', 'AccountController@balanceSheet');

// Revenue Graph
Route::get('/revenue', 'AccountController@revenue');



// Invoice
Route::get('/invoices', 'InvoiceController@index');
Route::get('/invoices/add', 'InvoiceController@addGet');
Route::post('/invoices/add', 'InvoiceController@addPost');
Route::get('/invoice/manage/{id}', 'InvoiceController@manageGet');

Route::get('/invoice/mark_cancel/{id}', 'InvoiceController@markCancelGet');
Route::get('/invoice/remove_cancel/{id}', 'InvoiceController@removeCancelGet');
Route::get('/invoice/mark_unpaid/{id}', 'InvoiceController@markUnpaidGet');
Route::get('/invoice/mark_paid/{id}', 'InvoiceController@markPaidGet');
Route::get('/invoice/send_email/{id}', 'InvoiceController@sendEmailGet');
Route::get('/invoice/download/{id}', 'InvoiceController@downloadInvoice');
Route::get('/invoice/view_pdf/{id}', 'InvoiceController@viewInvoicePdf');
Route::get('/invoice/edit/{id}', 'InvoiceController@editInvoiceGet');
Route::post('/invoice/edit/{id}', 'InvoiceController@editInvoicePost');
Route::post('/invoice/delete', 'InvoiceController@deleteInvoice');



// Recurring
Route::get('/recurring', 'RecurringController@index');
Route::get('/recurring/add', 'RecurringController@addGet');
Route::post('/recurring/add', 'RecurringController@addPost');



// jSon Return
Route::get('/json/products', 'JsonController@getProductJson');
Route::get('/json/product/price/{id}', 'JsonController@getProductPriceJson');
Route::get('/json/product/price/', 'JsonController@getProductPriceJson');


// Account Activity
Route::get('/json/account-activity/{id}', 'JsonController@activityGet');
Route::get('/json/transaction/{id}', 'JsonController@transactionGet');
Route::get('/json/order/{id}', 'JsonController@orderGet');
Route::get('/json/invoice/{id}', 'JsonController@invoiceGet');
Route::get('/json/ticket/{id}', 'JsonController@TicketGet');


Route::get('/json/transaction/edit/{id}', 'JsonController@getTransactionById');



// Todo Application
Route::get('/json/todo', 'JsonController@todoGet');
Route::get('/json/todo/{name}', 'JsonController@todogetName');

// Payment
Route::get('/payments', 'PaymentController@index');
Route::get('/payment/add', 'PaymentController@addGet');
Route::get('/payment/manage/{id}', 'PaymentController@manageGet');
Route::post('/payment/manage/{id}', 'PaymentController@managePost');
Route::post('/payment/add', 'PaymentController@addPost');
Route::get('/payment/delete/{id}', 'PaymentController@deleteGet');

//Email Tempates
Route::get('/email-templates', 'EmailTemplateController@index');
Route::get('/template/manage/{id}', 'EmailTemplateController@manageGet');
Route::post('/template/manage/{id}', 'EmailTemplateController@managePost');

// Support Departments
Route::get('/support', 'SupportController@index');
Route::post('/support/add', 'SupportController@addPost');
Route::get('/support/edit/{id}', 'SupportController@index');
Route::get('/support/delete/{id}', 'SupportController@delete');
Route::post('/support/edit/{id}', 'SupportController@editPost');

// System Settings
Route::get('/settings', 'SettingController@index');
Route::post('/settings', 'SettingController@updatePost');
//Route::get('/documentation', 'SettingController@devDocs');



// Ticket Routing
Route::get('/ticket', 'TicketController@index');
//Route::get('/ticket/category/{id}', 'TicketController@categoryGet');
Route::get('/ticket/active', 'TicketController@categoryActiveGet');
Route::get('/ticket/close', 'TicketController@categoryCloseGet');
Route::get('/ticket/answered', 'TicketController@answeredGet');
Route::get('/ticket/customer/replied', 'TicketController@clientRepliedGet');
Route::get('/ticket/add', 'TicketController@add');
Route::post('/ticket/add', 'TicketController@addPost');
Route::get('/ticket/answer', 'TicketController@answerGet');
Route::get('/ticket/history/{id}', 'TicketController@historyGet');
Route::get('/ticket/reply/{id}', 'TicketController@replyGet');
Route::post('/ticket/reply/message/{id}', 'TicketController@replyPost');
Route::post('/ticket/delete', 'TicketController@deleteTicket');

// Ticket Categories
Route::get('/ticket/categories', 'TicketController@categories');
Route::get('/ticket/category/add', 'TicketController@categoryAdd');
Route::post('/ticket/category/add', 'TicketController@categoryAddPost');
Route::get('/ticket/category/edit/{id}', 'TicketController@categoryEdit');
Route::post('/ticket/category/edit/{id}', 'TicketController@categoryEditPost');
Route::post('/ticket/category/delete', 'TicketController@categoryDeletePost');

// Article Routing
Route::get('/article', 'ArticleController@index');
Route::get('/article/add', 'ArticleController@add');
Route::post('/article/add', 'ArticleController@addPost');
Route::get('/article/edit/{id}', 'ArticleController@edit');
Route::post('/article/edit/{id}', 'ArticleController@editPost');
Route::get('/article/delete/{id}', 'ArticleController@delete');

// Faq Routing
Route::get('/faq', 'ArticleController@faqGet');
Route::get('/faq/add', 'ArticleController@faqAddGet');
Route::post('/faq/add', 'ArticleController@faqAddPost');
Route::get('/faq/edit/{id}', 'ArticleController@faqEditGet');
Route::post('/faq/edit/{id}', 'ArticleController@faqEditPost');
Route::get('/faq/delete/{id}', 'ArticleController@faqDeleteGet');

// Article Categories
Route::get('/article/categories', 'ArticleController@categories');
Route::get('/article/category/add', 'ArticleController@categoryAdd');
Route::post('/article/category/add', 'ArticleController@categoryAddPost');
Route::get('/article/category/edit/{id}', 'ArticleController@categoryEdit');
Route::post('/article/category/edit/{id}', 'ArticleController@categoryEditPost');
Route::post('/article/category/delete', 'ArticleController@categoryDeletePost');


// Export Routing
Route::get('/export/invoice', 'ExportController@invoice');


// Applications Routing

// todo application
Route::get('/apps/todo', 'ApplicationController@todoget');
Route::post('/apps/todo/create', 'ApplicationController@todoCreatePost');
Route::get('/apps/todo/delete/{id}', 'ApplicationController@todoDeleteGet');
Route::post('/apps/todo/changeStatus/{id}', 'ApplicationController@todoStatusUpdatePost');
Route::post('/apps/todo/update/{id}', 'ApplicationController@todoUpdatePost');

// Sticky Application
Route::get('/apps/sticky', 'ApplicationController@stickyNoteGet');

// Notice Board Application
Route::get('/apps/noticeboard', 'ApplicationController@noticeBoardGet');
Route::post('/notice/add', 'ApplicationController@noticeAddPost');
Route::get('/apps/notice/{id}', 'ApplicationController@noticeGetById');
Route::get('/apps/notice/delete/{id}', 'ApplicationController@noticeDeleteById');
Route::post('/apps/notice/edit', 'ApplicationController@editNoticePost');

// Slider Setup
Route::get('/sliders', 'SettingController@getSliders');
Route::get('/slider/add', 'SettingController@getAddSlider');




// Home Routing
Route::get('/home', 'HomeController@index');

// Mail Routing
Route::get('/test/send/mail', 'MailController@sendMail');


// Test Purpose
Route::get('/test/pdf', 'TestController@pdfGet');
Route::get('/test/some', 'TestController@some');
Route::get('/test/send/mail', 'TestController@sendMail');
