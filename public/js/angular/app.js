var app = angular.module('crmApplication', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('accActivityCtrl', function ($scope, $http, $window) {
    $scope.baseUrl = window.location.origin;
    $scope.userId = $window.id;

        $http.get($scope.baseUrl+'/json/account-activity/'+$scope.userId).success(function(data) {
           $scope.user = data;
        })

    $scope.showAccountInfo = function () {
        $http.get($scope.baseUrl+'/json/account-activity/'+$scope.userId).success(function(data) {
            $scope.user = data;
        })
    }

    $scope.showTransactionInfo = function () {
        $http.get($scope.baseUrl+'/json/transaction/'+$scope.userId).success(function(data) {
            $scope.transactions = data;
        })
    }

    $scope.showOrderInfo = function () {
        $http.get($scope.baseUrl+'/json/order/'+$scope.userId).success(function(data) {
            $scope.orders = data;
        })
    }

    $scope.showInvoiceInfo = function () {
        $http.get($scope.baseUrl+'/json/invoice/'+$scope.userId).success(function(data) {
            $scope.invoices = data;
        })
    }

    $scope.showTicketInfo = function () {
        $http.get($scope.baseUrl+'/json/ticket/'+$scope.userId).success(function(data) {
            $scope.tickets = data;
        })
    }

})

app.controller('invoiceController', function ($scope, $http) {
    $scope.baseUrl = window.location.origin;
    $scope.getPrice = function () {
        $http.get($scope.baseUrl+'/json/product/price/'+$scope.productId).success(function(data, status, headers, config) {
            $("#price").val(data.price);
        })
    }
})

app.controller('stickyCtrl', function ($scope, $http) {

    $scope.editNote = function () {

    }
})

app.controller('todoCtrl', function($scope, $http) {



    $scope.baseUrl = window.location.origin;
    $scope.todos = null;
    $scope.editedTodo = null;
    $scope.status = 'all';
    $scope.newTodo = '';

    $http.get($scope.baseUrl+'/json/todo').success(function(data, status, headers, config) {
        $scope.todos = data;
    })

    $scope.getLatestTodos = function () {
        $http.get($scope.baseUrl+'/json/todo').success(function(data) {
            $scope.todos = data;
            $scope.status = 'all';
        })
    }


    $scope.getLatestTodosActive = function () {
        $http.get($scope.baseUrl+'/json/todo/active').success(function(data) {
            $scope.todos = data;
            $scope.status = 'active';
        })
    }

    $scope.getLatestTodosCompleted = function () {
        $http.get($scope.baseUrl+'/json/todo/completed').success(function(data) {
            $scope.todos = data;
            $scope.status = 'completed';
        })
    }



    $scope.addTodo = function () {
        var newTodo = {
            title: $scope.newTodo.trim()
        };

        if (!newTodo.title) {
            return;
        }

        $scope.saving = true;

        $http.post($scope.baseUrl+'/apps/todo/create', newTodo)
            .success(function(data) {
                $scope.saving = false;
                $scope.getLatestTodos();
                $scope.newTodo = '';
            })

    };

    $scope.removeTodo = function (todo) {
        $http.get($scope.baseUrl+'/apps/todo/delete/'+todo.id).success(function(data, status, headers, config) {
            if(data == 1){
                $scope.getLatestTodos();
            }
        })
    };

    $scope.toggleCompleted = function (todo) {
        var comTodo = {
            completed: todo.completed
        };
        $http.post($scope.baseUrl+'/apps/todo/changeStatus/'+todo.id, comTodo).success(function(data, status, headers, config) {})
    };


    $scope.editTodo = function (todo) {
        $scope.editedTodo = todo;
        $scope.originalTodo = angular.extend({}, todo);
    };

    $scope.saveEdits = function (todo, event) {
        // Blur events are automatically triggered after the form submit event.
        // This does some unfortunate logic handling to prevent saving twice.
        if (event === 'blur' && $scope.saveEvent === 'submit') {
            $scope.saveEvent = null;
            return;
        }

        $scope.saveEvent = event;

        if ($scope.reverted) {
            // Todo edits were reverted-- don't save.
            $scope.reverted = null;
            return;
        }

        todo.title = todo.title.trim();

        if (todo.title === $scope.originalTodo.title) {
            $scope.editedTodo = null;
            return;
        }

        if(todo.title){

            var editTodo = {
                title: todo.title
            };

            $http.post($scope.baseUrl+'/apps/todo/update/'+todo.id, editTodo).success(function(data) {
                if(data == 1){
                    $scope.getLatestTodos();
                    $scope.editedTodo = null;
                }
            })

        }else{

        }

    };




});