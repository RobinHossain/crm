var ChartsAmcharts=function() {
    var a=function(chartData) {
        var e=AmCharts.makeChart("chartRevenue", {
                type:"serial", theme:"light", fontFamily:"Open Sans", color:"#888888",
            dataProvider: chartData,
            balloon: {
                    cornerRadius: 6
                }
                , valueAxes:[ {
                    duration:"mm", durationUnits: {
                        hh: "h ", mm: "min"
                    }
                    , axisAlpha:0
                }
                
                ], graphs:[ {
                    bullet: "square", bulletBorderAlpha: 1, bulletBorderThickness: 1, fillAlphas: .3, fillColorsField: "lineColor", legendValueText: "[[value]]", lineColorField: "lineColor", title: "price", valueField: "price"
                }
                ], chartScrollbar: {}
                , chartCursor: {
                    categoryBalloonDateFormat: "YYYY MMM DD", cursorAlpha: 0, zoomable: !1
                }
                , dataDateFormat:"YYYY-MM-DD", categoryField:"date", categoryAxis: {
                    dateFormats:[ {
                        period: "DD", format: "DD"
                    }
                        , {
                            period: "WW", format: "MMM DD"
                        }
                        , {
                            period: "MM", format: "MMM"
                        }
                        , {
                            period: "YYYY", format: "YYYY"
                        }
                    ], parseDates:!0, autoGridCount:!1, axisColor:"#555555", gridAlpha:0, gridCount:50
                }
            }
        );
        $("#chartRevenue").closest(".portlet").find(".fullscreen").click(function() {
                e.invalidateSize()
            }
        )
    };
    return {
        init:function(chartData) {
            a(chartData)
        }
    }
}

();
