tinymce.init({
    selector: '#template-body',
    height: 500,
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools filemanager"
    ],
    image_advtab: true,
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons template",

    content_css: '//www.tinymce.com/css/codepen.min.css',
    templates: [
        {title: 'Default Template 1', description: 'Custom Template 1', url: '/tinymce/template/amn.htm'},
        {title: 'Default Template 2', description: 'Custom Template 2', url: '/tinymce/template/hcm.htm'}
    ]
});