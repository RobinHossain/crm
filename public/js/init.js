

// ******************************************* Well Documented Javascript Coding *****************************/
var CRM = CRM || {};
$(function() {

var $window = $(window),
    $body = $('body'),
    $DataTable = $('#datatable');
    $selectMaterial = $('select');
    $sideNav = $('.button-collapse');
    $dropDownButtonLeft = $('.dropdown-button.lft');
    $dropDownButtonRight = $('.dropdown-button.rht');

    CRM.extra = {

        init: function() {
            CRM.extra.datatable();
            CRM.extra.selectMatF();
            CRM.extra.sideNavFun();
            CRM.extra.leftDropDownMenu();
            CRM.extra.rightDropDownMenu();
        },

        leftDropDownMenu: function () {
            if ($dropDownButtonLeft.length > 0) {
                $dropDownButtonLeft.each(function() {
                    var element = $(this);
                    element.dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            hover: true, // Activate on hover
                            belowOrigin: true, // Displays dropdown below the button
                            alignment: 'left' // Displays dropdown with edge aligned to the left of button
                        }
                    );
                });
            }
        },

        rightDropDownMenu: function () {
            if ($dropDownButtonRight.length > 0) {
                $dropDownButtonRight.each(function() {
                    var element = $(this);
                    element.dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            hover: true, // Activate on hover
                            belowOrigin: true, // Displays dropdown below the button
                            alignment: 'right' // Displays dropdown with edge aligned to the left of button
                        }
                    );
                });
            }
        },

        sideNavFun: function () {
            if ($sideNav.length > 0) {
                $sideNav.each(function() {
                    var element = $(this);
                    element.sideNav();
                });
            }
        },

        selectMatF: function () {
            if ($selectMaterial.length > 0) {
                $selectMaterial.each(function() {
                    var element = $(this);
                    element.material_select();
                });
            }
        },


        datatable: function() {
            if ($DataTable.length > 0) {
                $DataTable.each(function() {
                    var element = $(this);
                    element.dataTable({
                        "oLanguage": {
                            "sStripClasses": "",
                            "sSearch": "",
                            "sSearchPlaceholder": "Enter Keywords Here",
                            "sInfo": "_START_ -_END_ of _TOTAL_",
                            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
                            '<option value="10">10</option>' +
                            '<option value="20">20</option>' +
                            '<option value="30">30</option>' +
                            '<option value="40">40</option>' +
                            '<option value="50">50</option>' +
                            '<option value="-1">All</option>' +
                            '</select></div>'
                        },
                        bAutoWidth: false
                    });
                });
            }
        },


    };

    $(document).ready( CRM.extra.init );

});
// ******************************************* # End # Well Documented Javascript Coding *****************************/