-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 08:57 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `password` varchar(225) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `balance` decimal(10,2) DEFAULT '0.00',
  `photo` varchar(225) DEFAULT NULL,
  `notification_status` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(100) DEFAULT NULL,
  `account_type_id` int(11) DEFAULT '1',
  `role_id` int(11) DEFAULT '4',
  `group_id` int(11) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `fname`, `lname`, `password`, `company`, `remember_token`, `email`, `balance`, `photo`, `notification_status`, `phone`, `account_type_id`, `role_id`, `group_id`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `country_id`, `created_at`, `updated_at`) VALUES
(2, 'John', 'Due', '$2y$10$SmdkQ8p/xtiWr95r8WG0EOEMkIC7E.FagBYvGnHlC8Mfx8eXJP2sO', 'W3BD', 'MsWhZ4wb0lwdLiWpHCgsYjQPDmVRFNim4fkT21hRYDM4M2LDx5APpoGz7MsX', 'johndue@liniva.com', '0.00', NULL, 1, NULL, 1, 4, 1, 'California', NULL, 'California', NULL, NULL, 236, '2016-12-28 10:51:59', '2017-02-11 22:56:58'),
(3, 'Darren', 'Dunner', '$2y$10$l3nv5efV4b/pd8d/kVgmOO70c.epVmyi835K7fbOfrxkIptXqYcpq', 'W3BD', 'DcLP9haheiK2nNA9NqjKrSPwU4SkXW16ug2mXHgPFplfcvJA0Jj2eScvlbvR', 'darren@transilva.com', '0.00', NULL, 1, NULL, 1, 4, 1, 'California', NULL, 'California', NULL, NULL, 236, '2016-12-28 10:52:35', '2016-12-28 10:52:41'),
(4, 'Jenifar', 'Lopaz', '$2y$10$N5OMH0TNTBCXiq04xc4phOUyWvyY1wR/DZsB9BOP5yKMYiuQ34j6e', 'W3BD', 'Z5ln0G2C1wQ8UI5qln0cP98w914rtpxcdkUkli9tfL6revSeJ3nt01VzFjPj', 'lopaz@calivania.com', '0.00', NULL, 1, NULL, 1, 4, 1, 'California', NULL, 'California', NULL, NULL, 236, '2016-12-28 10:53:23', '2016-12-28 10:53:27'),
(5, 'Caroline', 'Harday', '$2y$10$vhuS4uWk2/NuFjT1BtR1HOnxozskNzLlznq6AQksUx8/YTkJycsJS', 'W3BD', 'XAndTD2bOm9uayBIDsLADG7HiUuU4MffpNl2NNLtIkkIqYxswwIntmx3MTPw', 'caroline@softnic.com', '0.00', NULL, 1, NULL, 1, 4, 1, 'California', NULL, 'California', NULL, NULL, 236, '2016-12-28 10:56:56', '2016-12-28 10:57:00'),
(7, 'Jonathon', 'Caddy', '$2y$10$wzwTB8ixDyvPa6Hml2rFBuE8v9jf7qt7Re3fZu35k3IczSWpgZube', 'W3BD', '5LrmbkoUWLxPRk18rPjiQf27E6Po55qVh11Dm7rE35cZoarRAx151wCKnjsk', 'client@website.com', '0.00', NULL, 1, NULL, 1, 4, 1, 'California', NULL, 'California', NULL, NULL, 236, '2016-12-28 10:58:15', '2016-12-28 10:58:17'),
(8, 'Tim', 'Due', '$2y$10$.FJufxQbk/LVDJL7fAbPiOVV6GPQGapWzQotYfi2jgEexKppna/sS', 'W3BD', 'lgeJvBk93VHrJuo5rvu7OcVxbOtzlgdlBvdnaygHX92u3dSDfnUCe1q0JY9Z', 'timdue@gmail.com', '0.00', NULL, 1, '234234234', 1, 4, 2, 'California', '', 'California', '', '', 236, '2016-12-28 10:58:50', '2016-12-28 10:59:00');

-- --------------------------------------------------------

--
-- Table structure for table `account_groups`
--

CREATE TABLE `account_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_groups`
--

INSERT INTO `account_groups` (`id`, `name`) VALUES
(1, 'Clients'),
(2, 'Wholesaler'),
(3, 'new Group'),
(4, 'Another Group');

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `name`) VALUES
(1, 'Clients'),
(2, 'Administrator'),
(3, 'Wholesaller');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `content` mediumtext NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `category_id`, `content`, `author_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'The Internet Is Broken. Here’s How I’d Fix It', 3, '<h1 id="3609" class="graf graf--h3 graf--leading graf--title">The Internet Is Broken. Here&rsquo;s How I&rsquo;d Fix&nbsp;It</h1>\r\n<section class="section section--body section--first">\r\n<div class="section-content">\r\n<div class="section-inner sectionLayout--insetColumn">\r\n<h2 id="c5e3" class="graf graf--h4 graf-after--h3 graf--subtitle">Changing the encoded anonymity of the&nbsp;web</h2>\r\n<p id="1652" class="graf graf--p graf-after--h4">My big idea is that we have to fix the internet. After 40 years, it has begun to corrode, both itself and us. It is still a marvelous and miraculous invention, but now there are bugs in the foundation, bats in the belfry, and trolls in the basement.</p>\r\n<p id="6191" class="graf graf--p graf-after--p">I do not mean this to be one of those technophobic rants dissing the Internet for rewiring our brains to give us the twitchy attention span of Donald Trump on Twitter or pontificating about how we have to log off and smell the flowers. Those qualms about new technologies have existed ever since Plato fretted that the technology of writing would threaten memorization and oratory. I love the internet and all of its digital offshoots. What I bemoan is its decline.</p>\r\n<p id="14cb" class="graf graf--p graf-after--p">There is a bug in its original design that at first seemed like a feature but has gradually, and now rapidly, been exploited by hackers and trolls and malevolent actors: Its packets are encoded with the address of their destination but not of their authentic origin. With a circuit-switched network, you can track or trace back the origins of the information, but that&rsquo;s not true with the packet-switched design of the internet.</p>\r\n<p id="6025" class="graf graf--p graf-after--figure">Compounding this was the architecture that Tim Berners-Lee and the inventors of the early browsers created for the World Wide Web. It brilliantly allowed the whole of the earth&rsquo;s computers to be webbed together and navigated through hyperlinks. But the links were one-way. You knew where the links took you. But if you had a webpage or piece of content, you didn&rsquo;t exactly know who was linking to you or coming to use your content.</p>\r\n<blockquote id="391c" class="graf graf--pullquote graf-after--p">All of that enshrined the potential for anonymity.</blockquote>\r\n<p id="e06d" class="graf graf--p graf-after--pullquote">You could make comments anonymously. Go to a webpage anonymously. Consume content anonymously. With a little effort, send email anonymously. And if you figured out a way to get into someone&rsquo;s servers or databases, you could do it anonymously.</p>\r\n<p id="4fbb" class="graf graf--p graf-after--p">For years, the benefits of anonymity on the net outweighed its drawbacks. People felt more free to express themselves, which was especially valuable if they were dissidents or hiding a personal secret. This was celebrated in the famous 1993 <em class="markup--em markup--p-em">New Yorker</em>cartoon, &ldquo;On the Internet, nobody knows you&rsquo;re a dog.&rdquo;</p>\r\n<p id="0c12" class="graf graf--p graf-after--p">Now the problem is nobody can tell if you&rsquo;re a troll. Or a hacker. Or a bot. Or a Macedonian teenager publishing a story that the pope has endorsed Trump.</p>\r\n<p id="d5d2" class="graf graf--p graf-after--p">This has poisoned civil discourse, enabled hacking, permitted cyberbullying, and made email a risk. Its inherent lack of security has allowed Russian actors to screw with our democratic process.</p>\r\n<p id="a51e" class="graf graf--p graf-after--p">The lack of secure identification and authentication inherent in the internet&rsquo;s genetic code has also prevented easy transactions, thwarted financial inclusion, destroyed the business models of content creators, unleashed deluges of spam, and forced us to use passwords and two-factor authentication schemes that would have baffled Houdini.</p>\r\n<p id="4d28" class="graf graf--p graf-after--p">The trillions being spent and the IQ points of computer science talent being allocated to tackle security issues makes it a drag, rather than a spur, to productivity in some sectors.</p>\r\n<p id="4699" class="graf graf--p graf-after--p"><span class="markup--quote markup--p-quote is-other" data-creator-ids="anon">In Plato&rsquo;s <em class="markup--em markup--p-em">Republic</em>, we learn the tale of the Ring of Gyges. Put it on, and you&rsquo;re invisible and anonymous. The question that Plato asks is whether those who put on the ring will be civil and moral. He thinks not. The internet has proven him correct.</span></p>\r\n<blockquote id="3cc6" class="graf graf--pullquote graf-after--p">The web is no longer a place of community, no longer an&nbsp;agora.</blockquote>\r\n<p id="db07" class="graf graf--p graf-after--pullquote">Every day more sites are eliminating comments sections.</p>\r\n<p id="3b8f" class="graf graf--p graf-after--p">If we could start from scratch, here&rsquo;s what I think we would do:</p>\r\n<ul class="postList">\r\n<li id="809a" class="graf graf--li graf-after--p">Create a system that enables content producers to negotiate with aggregators and search engines to get a royalty whenever their content is used, like ASCAP has negotiated for public performances and radio airings of its members&rsquo; works.</li>\r\n<li id="85f3" class="graf graf--li graf-after--li">Embed a simple digital wallet and currency for quick and easy small payments for songs, blogs, articles, and whatever other digital content is for sale.</li>\r\n<li id="5a7f" class="graf graf--li graf-after--li">Encode emails with an authenticated return or originating address.</li>\r\n<li id="e30f" class="graf graf--li graf-after--li">Enforce critical properties and security at the lowest levels of the system possible, such as in the hardware or in the programming language, instead of leaving it to programmers to incorporate security into every line of code they write.</li>\r\n<li id="e8b4" class="graf graf--li graf-after--li">Build chips and machines that update the notion of an internet packet. For those who want, their packets could be encoded or tagged with metadata that describe what they contain and give the rules for how it can be used.</li>\r\n</ul>\r\n<p id="32a2" class="graf graf--p graf-after--li">Most internet engineers think that these reforms are possible, from Vint Cerf, the original TCP/IP coauthor, to Milo Medin of Google, to Howard Shrobe, the director of cybersecurity at MIT. &ldquo;We don&rsquo;t need to live in cyber hell,&rdquo; Shrobe has argued.</p>\r\n<p id="1745" class="graf graf--p graf-after--p">Implementing them is less a matter of technology than of cost and social will. Some people, understandably, will resist any diminution of anonymity, which they sometimes label privacy.</p>\r\n<p id="de75" class="graf graf--p graf-after--p">So the best approach, I think, would be to try to create a voluntary system, for those who want to use it, to have verified identification and authentication.</p>\r\n<p id="b962" class="graf graf--p graf-after--p">People would not be forced to use such a system. If they wanted to communicate and surf anonymously, they could. But those of us who choose, at times, not to be anonymous and not to deal with people who are anonymous should have that right as well. That&rsquo;s the way it works in the real world.</p>\r\n<p id="e2cf" class="graf graf--p graf-after--p graf--last">The benefits would be many: easy and secure ways to deal with your finances and medical records. Small payment systems that could reward valued content rather than the current incentive to concentrate on clickbait for advertising. Less hacking, spamming, cyberbullying, trolling, and the spewing of anonymous hate. And the possibility of a more civil discourse.</p>\r\n</div>\r\n</div>\r\n</section>\r\n<section class="section section--body section--last">\r\n<div class="section-divider"><hr class="section-divider" /></div>\r\n<div class="section-content">\r\n<div class="section-inner sectionLayout--insetColumn">\r\n<p id="9531" class="graf graf--p graf--leading graf--last"><em class="markup--em markup--p-em">This essay is partly drawn from a talk delivered to the American Academy of Arts &amp; Sciences. It was originally published on </em><a class="markup--anchor markup--p-anchor" href="https://www.linkedin.com/pulse/internet-broken-starting-from-scratch-heres-how-id-fix-isaacson" target="_blank" rel="nofollow" data-href="https://www.linkedin.com/pulse/internet-broken-starting-from-scratch-heres-how-id-fix-isaacson"><em class="markup--em markup--p-em">LinkedIn</em></a><em class="markup--em markup--p-em">.</em></p>\r\n</div>\r\n</div>\r\n</section>', 2, 1, '2016-12-27 12:18:08', '2016-12-27 12:18:08'),
(5, '10 Things I Do Every Day To Grow My Career', 2, '<p id="3e80" class="graf graf--p graf-after--figure">As the founder of a site dedicated to helping people fix their own career problems, I&rsquo;ve always felt I should practice what I preach. The name <a class="markup--anchor markup--p-anchor" href="https://www.workitdaily.com/" target="_blank" data-href="https://www.workitdaily.com">Work It Daily</a> explains it all. Myself and my teammates truly believe small amounts of consistent activity in your career is not only easier to stick with, it yields powerful results. I&rsquo;ve been following this routine for years. It has taken me from being a one-person consulting firm to being part of a thriving company that serves millions of professionals.</p>\r\n<p id="91f3" class="graf graf--p graf-after--p"><strong class="markup--strong markup--p-strong">This is the list of 10 things I try to do every workday.</strong></p>\r\n<p id="f3e6" class="graf graf--p graf-after--p">Yes, there are days when I don&rsquo;t get them all done, but I do my best to deliver. It has proven very effective for me. They are:</p>\r\n<ol class="postList">\r\n<li id="66cb" class="graf graf--li graf-after--p">Read something related to my industry.</li>\r\n<li id="c02d" class="graf graf--li graf-after--li">Read something related to business development.</li>\r\n<li id="5f40" class="graf graf--li graf-after--li">Send two emails to touch base with old colleagues.</li>\r\n<li id="5235" class="graf graf--li graf-after--li">Empty my private client inbox by responding to all career and HR coaching questions within one business day.</li>\r\n<li id="896e" class="graf graf--li graf-after--li">Check in with each team member on their progress.</li>\r\n<li id="121a" class="graf graf--li graf-after--li">Have a short non-work related conversation with every employee.</li>\r\n<li id="7d78" class="graf graf--li graf-after--li">Review my top three goals for my company that are focused on its growth.</li>\r\n<li id="1ebd" class="graf graf--li graf-after--li">Identify and execute one task to support each of my top three goals.</li>\r\n<li id="f6dc" class="graf graf--li graf-after--li">Post five valuable pieces of content on all my major social media accounts.</li>\r\n<li id="fb09" class="graf graf--li graf-after--li">Take a full minute to appreciate what I have and how far I&rsquo;ve come.</li>\r\n</ol>\r\n<p id="49e5" class="graf graf--p graf-after--li"><strong class="markup--strong markup--p-strong">This list could be longer. BUT&hellip;</strong></p>\r\n<p id="04fe" class="graf graf--p graf-after--p">If it was longer, I wouldn&rsquo;t be as good at getting them all done. This list is manageable to me. Of course, I do more than these ten things every day. But, these are the ten I choose to do with consistency. Why? Over the years, they&rsquo;ve proven the best way for me to grow my career and my business. The collective results have made completing these tasks consistently; even when I don&rsquo;t feel like it, well worth it.</p>\r\n<p id="4712" class="graf graf--p graf-after--p"><strong class="markup--strong markup--p-strong">What do you do every day with consistency an how has it helped your career?</strong></p>', 2, 1, '2016-12-27 12:37:22', '2016-12-27 12:37:22'),
(6, '10 Pieces of Terrible Career Advice You Should Ignore', 2, '<p id="2cc3" class="graf graf--p graf-after--figure">We all have those people in our lives, who (bless their hearts) are just wrong about some things. They probably mean well (maybe), but it can be very toxic to surround yourself with bad advice.</p>\r\n<p id="0cd2" class="graf graf--p graf-after--p">Here is the worst advice I&rsquo;ve actually heard.</p>\r\n<h3 id="a742" class="graf graf--h3 graf-after--p"><strong class="markup--strong markup--h3-strong">1. &ldquo;If you have a steady paycheck, don&rsquo;t fuck it&nbsp;up!&rdquo;</strong></h3>\r\n<p id="8c91" class="graf graf--p graf-after--h3">Just the other day I heard this advice be given quite forcefully to a friend who happens to be a brilliant hardware engineer at Intel. <em class="markup--em markup--p-em">&ldquo;If you have a steady paycheck, don&rsquo;t ruin it or waste your time trying to find something else!&rdquo;</em></p>\r\n<p id="9361" class="graf graf--p graf-after--p"><strong class="markup--strong markup--p-strong">Not only is this wrong, but it&rsquo;s dangerous. </strong>This is how gifted people end up <strong class="markup--strong markup--p-strong">stuck in the same job for 20+ years</strong> and wake up one day bitterly regretting why they didn&rsquo;t see what else was out there.</p>\r\n<p id="3006" class="graf graf--p graf-after--p">Don&rsquo;t get me wrong, he has a great job, but after starting at Intel right after college, he has never experienced anything else full-time and might love doing something slightly different in a different setting with even more benefits. As I always say,<strong class="markup--strong markup--p-strong"> if you&rsquo;ve been at a job for 3 -4 years and have not been promoted, you should move on to your next opportunity. </strong>Studies show that those who switch positions or companies every 3&ndash;4 years are significantly more likely to attract the most desirable positions at top tech companies.</p>\r\n<blockquote id="4ecc" class="graf graf--blockquote graf--startsWithDoubleQuote graf-after--p">&ldquo;There had better be big, meaty challenges ahead and plenty of advancement potential in your job before you decide to give your employer another year of your life&rdquo;<em class="markup--em markup--blockquote-em"> writes Liz Ryan CEO and founder of Human Workplace in Forbes.</em></blockquote>\r\n<p id="44fe" class="graf graf--p graf-after--blockquote">In the past the longer you stayed at a job the better, <em class="markup--em markup--p-em">for the employer</em> <strong class="markup--strong markup--p-strong">-not for your potential.</strong></p>\r\n<p id="0c83" class="graf graf--p graf-after--p"><span class="markup--quote markup--p-quote is-other" data-creator-ids="anon"><strong class="markup--strong markup--p-strong">The longer you stay at a job without advancement, the less marketable you will be once you eventually try to get a new job.</strong></span> Top employers want to see advancement, not that you got comfortable and stayed in the same position for 5 years.</p>\r\n<h3 id="e746" class="graf graf--h3 graf-after--p"><strong class="markup--strong markup--h3-strong">2. &ldquo;Take the first job you can get, just until you find something better.&rdquo;</strong></h3>\r\n<p id="6a74" class="graf graf--p graf-after--h3">No! Don&rsquo;t do that. Unless you will actually starve without that job, <strong class="markup--strong markup--p-strong">don&rsquo;t settle for a job you don&rsquo;t like</strong> just because it&rsquo;s the first offer you get. Even if you say it wont happen to you, you have no idea how easy it is to stop trying and just do the bare minimum to get that paycheck every month. You could end up spending a year or more at a job you don&rsquo;t like instead of an extra month of searching. Like most things in life <strong class="markup--strong markup--p-strong">-you should refuse to settle.</strong></p>\r\n<h3 id="f0e6" class="graf graf--h3 graf-after--p"><strong class="markup--strong markup--h3-strong">3. &ldquo;Don&rsquo;t think about money -follow your passion and money will&nbsp;follow!&rdquo;</strong></h3>\r\n<p id="3856" class="graf graf--p graf-after--h3">Passion is so important -yes! But so is food and rent and feeding your future/current family. You should strive to love what you do, absolutely! However, <strong class="markup--strong markup--p-strong">those born without a massive trust fund will need to actually make money to support themselves.</strong></p>\r\n<p id="55d1" class="graf graf--p graf-after--p">Maybe your passion is painting or singing, but you&rsquo;re also pretty good at computer science. Finding a career path using your computer science skills is the more practical option. <strong class="markup--strong markup--p-strong">You should always keep your passions in your life and enjoy them as hobbies or even side jobs</strong>, <strong class="markup--strong markup--p-strong">but if you have a marketable talent in which you can find anything that you&rsquo;re pretty happy doing -do it!</strong></p>\r\n<p id="b8ff" class="graf graf--p graf-after--p">Not to be a downer, but if you think of the millions of people who want to be painters or singers in this world, less than 1% of them can actually make a decent living off of that alone. You might end up working jobs you hate even more than your practical skill just to pay your rent while you try to find something following you passion. Don&rsquo;t be a barista for the rest of your life when you have the chance to arm yourself with a degree and desirable skills for a sustainable, rewarding career.</p>', 2, 1, '2016-12-27 12:41:22', '2016-12-27 12:41:22'),
(7, 'THE BEST BURGER IN SHENZHEN', 3, '<div class="st-block markdown  text-left ">\r\n<p><strong>You&rsquo;ve probably been to a dozen joints like Frankie&rsquo;s:</strong> The varnished bar runs almost the length of the restaurant, an oaken finger indicating the cigar room in back. The brick walls are thick with beer logos and framed nostalgia.</p>\r\n<p>Twenty-somethings flit between high tables, flirting and munching on chicken wings. A guy you swear you&rsquo;ve seen in so many other watering holes sits flanked by empty barstools, suit coat on a peg under the counter, keeping counsel with a Kindle and a glass of wine. Dim overhead lights halo the chalkboard beer list: a dozen brews on tap, including Guinness and a Kansas City saison called Tank 7 Farmhouse Ale. Tank 7 is strong, and the smiling bartenders will warn you about its 8.5 percent ABV on your second or third pint. But hey, it goes well with Frankie&rsquo;s famous cheeseburger, a large, hand-formed patty topped with a healthy glob of cheese. &ldquo;Best burgers in town!&rdquo; read reviews. &ldquo;They get the fat-to-lean proportions just right.&rdquo; Other reviews focus not on the burger, but on the atmosphere, which is Frankie&rsquo;s true specialty: &ldquo;It has that Southern hospitality I miss&rdquo; and &ldquo;I walked into &shy;Frankie&rsquo;s and I was home.&rdquo; Home, in this case, refers to America. And that&rsquo;s the trick of this place. Frankie&rsquo;s&mdash;for all its chicken-&shy;fried charm&mdash;is in Shenzhen, China. It&rsquo;s at the metaphorical and physical heart of that city&rsquo;s expat community.</p>\r\n<p>It&rsquo;s a regular haunt for guys like Josh Bismanovsky, a San Francisco Bay-area native who is a couple of beers in and losing to me at Big Buck Hunter, strafing a simulated alpine stream with a tethered plastic shotgun. His forest of brown curly hair moves a beat behind his head as it snaps quickly to the right. He racks the plastic forend grip and fires at the screen; his shot connects, but with the wrong mammal. Don&rsquo;t shoot the cow! a pop-up dialog box admonishes. Game over.</p>\r\n</div>\r\n<div class="st-block image right  ">\r\n<div class="media media-element-container media-default float-right shareable"><img class="float-right shareable unveil-processed full-visible" src="http://www.popsci.com/sites/popsci.com/files/styles/medium_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=ACqGmAqB&amp;fc=42,27" alt="Josh Bismanovsky" data-smsrc="http://www.popsci.com/sites/popsci.com/files/styles/small_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=ba4O58Sg&amp;fc=42,27" data-medsrc="http://www.popsci.com/sites/popsci.com/files/styles/medium_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=ACqGmAqB&amp;fc=42,27" data-lgsrc="http://www.popsci.com/sites/popsci.com/files/styles/large_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=wrgeW5FJ&amp;fc=42,27" data-xlsrc="http://www.popsci.com/sites/popsci.com/files/styles/xl_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=RHQ8PB94&amp;fc=42,27" data-src="http://www.popsci.com/sites/popsci.com/files/styles/medium_1x_/public/images/2016/12/psc0217_wl_shenzhen_expats-burgers-d1-326-r-flat.jpg?itok=ACqGmAqB&amp;fc=42,27" />\r\n<div class="caption-wrapper indent">\r\n<div class="field-credit">\r\n<p>Christina Holmes</p>\r\n</div>\r\n<div class="field-body">\r\n<p>Josh Bismanovsky: misfit, man-about-town, injection-molding sales rep.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="st-block markdown  text-left ">\r\n<p>&ldquo;I suck at this game,&rdquo; he says, drains his beer, and walks outside to smoke. The night is warm and heavy with typhoon rain, still power-washing the city streets after two days of relentless wet. Frankie&rsquo;s has a little frosted-glass awning out front though, and you can stay pretty dry if you stand under it.</p>\r\n<p>Follow Bismanovsky&rsquo;s gaze as he pulls on his imported Japanese cigarette, and you might see <a class="linkTargets-processed" href="http://www.popsci.com/what-is-sound-no-sounds-happening">Hong Kong</a>. Not the famous jagged skyline, but the outskirts: the swampy green of the Mai Po wetlands and the bleak stretches of Lok Ma Chau, the buffer zone the Chinese government set up to separate the cultural anarchy of Hong Kong from the unsullied mainland.</p>\r\n</div>\r\n<div class="st-block markdown  text-left ">\r\n<p>Frankie&rsquo;s Bar and Grille, &shy;Guihua Road, Futian district of Shenzhen, Guangdong province, Mainland-and-don&rsquo;t-you-forget-it China, is about 50 feet from the Free Trade Zone. Small-fronted and easy to miss if not for the illuminated green-script sign, Frankie&rsquo;s sits among warehouses, at the end of a street crowded with parked tractor-trailers. Opened just five years ago, Frankie&rsquo;s is nevertheless the alpha pub in a city of dog years.</p>\r\n<p>Here time hurtles forward at an astonishing pace.</p>\r\n<p>Deng Xiaoping created the city from bucolic &shy;nothing in 1980 as the pilot of his special economic zone &shy;project. These zones were meant to create safe &shy;places for &shy;Western companies to do business&mdash;and <a class="linkTargets-processed" href="http://www.popsci.com/uber-delivers-ice-cream-drone-shenzen">they worked</a>. Even though China is not the boom-country it was two years ago, favorable trade policies and cheap skilled labor lure companies and entrepreneurs from across the globe to Shenzhen&rsquo;s nimble factories. Before the SEZ, there were some 30,000 people in the area. &shy;Today, Shenzhen&rsquo;s population is north of 10 million, and its port is one of the busiest in China. You already know this: Your <a class="linkTargets-processed" href="http://www.popsci.com/why-trumps-idea-to-move-apple-product-manufacturing-to-us-makes-no-sense">iPhone</a> is made here. Everything is made here.</p>\r\n</div>\r\n<div class="st-block markdown  text-left ">\r\n<p>But this is not a story about tech made in China; it&rsquo;s a story about lives made in China.</p>\r\n<p>As of 2013, there were 22,000 permanent foreign &shy;nationals living in the city, and nearly 8 million others visiting every year. The expats range from the manufacturing-industry vet with a house and a spouse to the fresh-off-the-plane <a class="linkTargets-processed" href="http://www.popsci.com/tags/kickstarter-campaigns">Kickstarter romantic</a> with a pocketful of pledge cash to the English teacher who can&rsquo;t tell a diode from a dinner plate. And because humans crave contact with others, a community of couldn&rsquo;t-be-more-different &shy;internationals, united by the allure of this new economic engine, is building something far more important than &shy;businesses: a new cultural reality.</p>\r\n<p>There is, of course, a context for all this: epochal change. Just as warming seas intensify typhoons like the one outside, the ineluctable tide of human evolution is washing away borders. Change is a violent storm, and Shenzhen is landfall. The leading edge of this planet-shaping shift is here: Western economics, Western people, Western culture, all thriving inside a country whose government is powerful enough to lock down a billion people&rsquo;s Internet. No force, not even the thundering anti-globalization roar of America&rsquo;s most recent presidential election, can stop this tempest.</p>\r\n<p>And though we&rsquo;re still in the throes of it, you can already start to see what will grow when the chaos subsides. The foreign community in Shenzhen has the makings of a cultural power the world hasn&rsquo;t felt since the expatriate denizens of post-World War I Europe pounded absinthe together at Harry&rsquo;s New York Bar. They too ended up far from home thanks to savage economic forces, but they harnessed the energy of change and made things: art, literature, music.</p>\r\n<p>Walking the streets of Shenzhen, feeling the energy of the blurring world, you see those same archetypal characters snapping into focus. Entrepreneurs chasing dreams, artists seeking inspiration, landed gentry following the action, lost souls in search of a definition of self. And though there&rsquo;s no Shenzhen Hemingway, the ingredients for one exist in the experimental spirit and easy access to almost any component of anything made anywhere in the world. If we haven&rsquo;t found our Shenzhen Hemingway yet, perhaps it&rsquo;s because, expecting pages full of words, we&rsquo;re missing the sonnets of solder and wire. Today, for many of us, it&rsquo;s not a painting or a poem that captures the spirit of our time; it&rsquo;s a gadget or an app. Or maybe Shenzhen&rsquo;s contribution is more subtle: precedent for a global community of creatives drawn not to the historic centers of art, but to the world&rsquo;s nascent economic hubs.</p>\r\n<p>Source:&nbsp;http://www.popsci.com/shenzhen-china-global-community</p>\r\n</div>', 2, 1, '2016-12-27 12:46:11', '2016-12-27 12:46:11'),
(8, '5 Clever Ways to Get a Job', 5, '<p class="mt10">Over the last few years, the job market has been pretty brutal. With so many job seekers vying over the same openings, competition has reached sky-high limits. In response, some candidates are going to crazy lengths to get noticed. Whether candidates submit their resume on a chocolate bar, perform a guitar solo about why they''re the perfect fit for the job, film over-the-top YouTube videos that have gone viral, employers have seen just about everything.</p>\r\n<p>As crazy as some of these situations sound, they have one common denominator: they worked. The candidates captured their target employers'' attention and sometimes even landed the job.</p>\r\n<p>While filming a video and making it go viral might be a bit impractical for you, there are some more realistic ways to get noticed. Here are five clever ways that, when implemented, are sure to make you stand out from your competition and help you land that coveted job offer.</p>\r\n<h3 class="h3">1. Impress with Your Resume &amp; Cover Letter</h3>\r\n<p>Both your resume and cover letter need to be perfect. This tip sounds like a no-brainer, but you''d be amazed at how many people&mdash;qualified, competent people&mdash;lose job offers simply because of one lazy typo. Have a friend or trusted colleague take a look at what you''ve written. An extra 5 to 10 minutes can make the difference between securing an interview and being sent a &ldquo;thanks, but no thanks&rdquo; message from HR.</p>\r\n<p>Better yet, use <a class="inlineLink" href="http://www.livecareer.com/resume-builder">LiveCareer''s Resume Builder</a> to create an error-free, professional resume. Whether you''re simply updating your resume or creating your first-ever resume, LiveCareer makes the writing process easy and fast.</p>\r\n<h3 class="h3">2. Create an Online Profile</h3>\r\n<p>Hiring managers will likely Google you, so you need an online profile that accurately represents you as a professional&mdash;like LinkedIn. Your profile should match your resume, be full of job-specific keywords, and be 100 percent professional. Remember: <a class="inlineLink" href="http://www.livecareer.com/career-tips/social-media-can-wreck-career">you''re not on Facebook</a>. It''s probably a good idea to delete that picture from your drunken weekend in Cancun&mdash;especially if it''s your profile picture.</p>\r\n<p>&nbsp;</p>\r\n<p>3. Get to Know the Right People</p>\r\n<p>Getting your foot in the door can be as simple as knowing the right person. You''d be amazed at how many of your friends and acquaintances will know someone who''s looking to hire a candidate with the same skills you have. Plus, they can also put in a good word for you with the hiring manager, which is icing on the cake.</p>\r\n<p>Send out emails to former coworkers, ask around on Facebook, set up lunch meetings with social connections&mdash;do whatever it takes to keep your name on other peoples'' minds.</p>\r\n<h3 class="h3">4. Research the Company Website Beforehand</h3>\r\n<p>Once you secure an interview, take a moment to let out a sigh of relief and pat yourself on the back. But keep in mind: the hardest part of your job search awaits you.</p>\r\n<p>Answering questions about your previous experience just isn''t enough anymore. Before your interview, spend at least one hour researching the company you''re hoping to work for. Spend some time on their website, and do a little sleuthing about the industry. Having a few really good questions to ask about the position and company is only going make you shine even brighter than the candidate who''s just relying on his or her previous experience.</p>\r\n<h3 class="h3">5. Show Your Appreciation</h3>\r\n<p>Take a few minutes to write a thank-you note. As simple as it sounds, you''d be amazed at how many people pass up this last chance to sell themselves. It''s also a great time to ask that really good question that you thought of after the interview was over.</p>\r\n<p>While these tips may seem obvious, you''d be surprised how many of them are overlooked or just simply ignored. Apply them to your routine and are you''re sure to get that awesome job you''ve been dreaming about in no time. And if you need some help, remember that <a class="inlineLink" href="http://www.livecareer.com/">LiveCareer</a> has your back. Whether you need to write a resume, cover letter, or a simple thank-you note, LiveCareer has award-winning tools that streamline the process.</p>', 2, 1, '2016-12-27 12:47:58', '2016-12-27 12:47:58'),
(9, 'Leadership Lessons: Don''t Play It Safe, But Do Play to Win', 5, '<p><em>There''s no one "right" way to lead a business. Today''s leaders have a lot of wisdom to impart about managing the modern workforce, because each one approaches leadership in his or her own unique way. Every week, Business News Daily will share a leadership lesson from a successful business owner or executive.</em></p>\r\n<ul>\r\n<li><strong>The leader: </strong>Ilir Sela, founder and CEO of <a href="http://slicelife.com/" target="_blank">Slice</a></li>\r\n<li><strong>Time in current position: </strong>5 years</li>\r\n<li><strong>Ilir''s philosophy: </strong>"Inspire your team to play to win, rather than avoid losing. In business, playing it safe is the most dangerous strategy of all."</li>\r\n</ul>\r\n<p>Before I started Slice, I was grateful to have a handful of mentors, the most important ones being my friends and family. Growing up in the pizza industry, a majority of my friends and family owned pizzerias throughout New York and this gave me a first-hand look into how pizzerias work and what is needed to be successful. From ordering ingredients to delivering orders, my family and friends showed me how much dedication it takes to run a successful business.</p>\r\n<p>A big part of my leadership philosophy has been shaped by sports and observing the successes and failures of well-known technology companies. I''m a big sports fan and believe that teams who play just to get by almost always lose.&nbsp;Teams who invest in that philosophy usually end up being mediocre and lose that quality of what made them great, simply because they''re not giving it&nbsp;their&nbsp;all.</p>\r\n<p>I believe this philosophy also applies to business. From major companies such as Kodak or Microsoft, too often people are afraid to fail and that stops them from taking the risks that are necessary for creativity and innovation to thrive.</p>\r\n<p><em>Edited for length and clarity by Nicole Taylor.</em></p>\r\n<p>- See more at: http://www.businessnewsdaily.com/9652-leadership-lessons-ilir-sela.html#sthash.b2pEzCt5.dpuf</p>', 2, 1, '2016-12-27 12:49:47', '2016-12-27 12:49:47'),
(10, 'Small Business Snapshot: Alma Mater', 6, '<p>Our Small Business Snapshot series features photos that represent, in just one image, what the small businesses we feature are all about. Amanda Sima, founder and president of Alma Mater, explains how this image represents her business. Alma Mater designs and sells fast fashion in the licensed apparel industry.&nbsp;Our approach to fan apparel is simple:&nbsp;quality fashion perspectives and modern fits. Fans have a true variety of choices to display their pride, and can feel fresh while doing it. We have more than 90 NCAA college licenses and next have our sights on delivering quality fashion to the professional leagues. I started my business because I saw a gap in an industry that garnered consumers from all walks of life.&nbsp;Most of us have love for our teams, and every season is filled with fun and pride.&nbsp; But the industry has been capitalizing on fan pride with subpar goods for too long.&nbsp;We saw an opportunity to bring fashion and fit to the mass sea of tees and fleece in every fan shop around the country.&nbsp; My first step was developing our debut line of classic sweaters while also exploring the vast, competitive licensing industry.&nbsp;After gaining licenses at three of the major Big Ten schools, we began to grow both our licensing platform and product offerings to become one of the largest providers of fast fashion in licensed apparel. The companies in licensed apparel go beyond just Nike and Under Armour. All equally huge in size producing mass volumes of goods, it is difficult to stand out with new concepts in licensed apparel.&nbsp;It is an old industry with many comforts in place for both large companies and universities.&nbsp;But ultimately, consumers have the final say, and many businesses are experiencing increasing difficulty retaining female and younger shoppers. As a female-owned, millennial-owned company, we have our finger on the pulse of what these shoppers want, and that is why we are on a fast growth track. Edited for brevity and clarity by Nicole Taylor. - See more at: http://www.businessnewsdaily.com/9650-alma-mater-snapshot.html#sthash.eYu5afvk.dpuf</p>', 2, 1, '2016-12-27 12:50:44', '2016-12-27 12:50:44'),
(11, '11 Businesses Transforming Travel', 6, '<figure class="thumbRight image300">\r\n<div class="magnify-wrapper"><img src="http://www.businessnewsdaily.com/images/i/000/013/014/original/vacay.jpg?interpolation=lanczos-none&amp;fit=around|300:200&amp;crop=300:200;*,*" alt="11 Businesses Transforming Travel" /></div>\r\n<span class="thumb-credit">Credit: sebra/Shutterstock</span></figure>\r\n<p>Traveling is a rewarding experience. Between food, drinks and sights, there are a multitude of memories and experiences not to be forgotten.</p>\r\n<p>Often, planning your trip and getting to the destination is the hardest part. These 11 businesses aim to make traveling less stressful and more enjoyable.</p>\r\n<h2 class="nolinks">1. Flytographer</h2>\r\n<p>Snapshots are the best part of vacation; it gives you the opportunity to relive your favorite moments. <a href="http://www.flytographer.com/" target="_blank">Flytographer</a>&nbsp;takes that concept and helps you plan a trip. According to the company''s website, you can "meet with one of [the company''s] local photographers around the world, have a stroll together and bring home the best souvenir possible &mdash; memories." Simply search for a photographer you like in the location you''re traveling to and book a photo shoot via the company''s website after you''ve finalized your trip details. When it''s time for your shoot, your photographer will guide you around and take candid photos (though he or she will also take posed portraits, if you prefer).</p>\r\n<h2 class="nolinks">2. GetMyBoat</h2>\r\n<p>Love the water but not sure how to properly rent a boat while away? <a href="https://www.getmyboat.com/" target="_blank">GetMyBoat</a> provides a peer-to-peer marketplace giving boat owners the power to rent their boats and vacationers the opportunity to experience a yacht, speedboat or watercraft of their dreams at an affordable price. GetMyBoat is bridging the gap of boat ownership costs. Boating enthusiasts get the benefits of worldwide inventory without the cost or headache of ownership. And as the average boat is only used 8 percent of the year, boat owners get the chance to make owning a boat more affordable by earning some income from their under-utilized asset, the site said.</p>\r\n<h2 class="nolinks">3. Peek</h2>\r\n<p>Remove the hassle and pressure of planning activities on vacation with <a href="http://www.peek.com/" target="_blank">Peek</a>. The site curates a selection of fun activities for you that you can book online or via the company''s mobile app. You can also use Peek''s Travel Guides to see an overview of the top things to see or use the "Perfect Days" feature to book a complete itinerary with "insider tips and hidden gems from tastemakers," according to the website.</p>\r\n<h2 class="nolinks">4. The Trip Tribe</h2>\r\n<p>If you''re flying solo but would prefer to travel in a group or make new friends while on vacation, <a href="https://triptribe.com/" target="_blank">The Trip Tribe</a> makes it easy. The company allows members to create a profile and search for trips &mdash; they''ll use your profile information to match you with other travelers who share your interests so you can book a trip with like-minded people. When you view trips on the site, you can see which other members are going, as well as the dates, price per person and other important details. Some of The Trip Tribe''s featured trips include an eco-retreat in Puerto Rico, a food and beer tour in Ireland, and a cycling and boating adventure in the Czech Republic. <strong>[See Related Story:&nbsp;</strong><a href="http://www.businessnewsdaily.com/2389-jobs-travel-lovers.html" target="_blank"><strong>22 Great Jobs for People Who Love to Travel</strong></a><strong> ]</strong></p>\r\n<h2 class="nolinks">5. Welcome Beyond</h2>\r\n<p><a href="http://www.welcomebeyond.com/" target="_blank">Welcome Beyond</a> is dedicated to those travelers who prefer to forgo staying in a big-name hotel and would rather have a unique vacation experience. Welcome Beyond&nbsp;strives to curate hotels and vacation rentals that are "truly original." These exclusive rentals range from quirky to posh and are easy to search for and book. Choose a country and region for your stay, and search by interests like architecture and design, nature and ecotourism, and secluded retreats. You can also refine your search by accommodation type, with selections ranging from boutique hotels to private islands available in more than 30 different countries, including the United States.</p>\r\n<h2 class="nolinks">6. BlackJet</h2>\r\n<p>If you thought UberBlack made you feel like you were riding in style, try traveling on a jet. Book a seat on a private jet with <a href="https://www.blackjet.com/" target="_blank">BlackJet</a>. According to the company''s website, "BlackJet members enjoy the convenience, reliability and private jet experience at prices you would expect from American, Delta or United first class." As with all flights, costs vary depending on your origin and destination locations and the date of travel, but prices start around $1,000. BlackJet memberships are limited but free until you book your first trip &mdash; after that, there''s an annual fee.</p>\r\n<h2 class="nolinks">7. Triposo</h2>\r\n<p>Travelers with a spontaneous streak will love <a href="http://www.triposo.com/" target="_blank">Triposo</a>, an app that gives you personalized suggestions for things to do on your trip while you''re on the go. Users can book tours and activities via the app, as well as learn important information about their destination''s history, culture, currency and language. Before you go on your trip, download Triposo''s destination guide to your device, and while you''re there, get real-time updates about what''s going on nearby. Bonus: The app works offline too, so you don''t need to search for Wi-Fi to find your next activity.</p>\r\n<h2 class="nolinks">8. Wanderable</h2>\r\n<p>Weddings are expensive, and honeymoons can quickly add up, eliminate stress and lower costs with <a href="https://www.wanderable.com/" target="_blank">Wanderable</a>. Rather than have your friends and relatives buy items off a registry, register with Wanderable where they can contribute to your honeymoon fund. But the company does more than just collect cash for your dream trip &mdash; Wanderable allows you to search destinations and curate your trip based on your interests. Wedding guests can also contribute to specific costs, like transportation, lodging and even special tours or events offered at your destination.</p>\r\n<h2 class="nolinks">9. AirHelp</h2>\r\n<p>Most times, airlines will help you rebook a canceled flight and accommodate you. When&nbsp;they don''t, <a href="https://www.getairhelp.com/" target="_blank">AirHelp</a>&nbsp;steps in. You can either use the company''s online form or download its free mobile app to file a claim with your flight details, and AirHelp will take care of communicating with the airline for you so you can avoid feeling stressed and confused. The company will even file a lawsuit, if possible or necessary. If your claim is successful, the company will take a 25 percent service fee out of the money you''re awarded as payment, but if you lose your claim and you don''t get paid, you won''t owe AirHelp anything.</p>\r\n<h2 class="nolinks">10. Options Away</h2>\r\n<p>Have you ever found the perfect flight, only to come back to it a day or two later when you''re ready to purchase your tickets and find that it''s sold out? <a href="http://www.optionsaway.com/" target="_blank">Options Away</a>&nbsp;lets you&nbsp;search for flights and save them while you wait for your next paycheck or to finalize the rest of your plans. You can save flights for two, three, seven or 14 days, and in&nbsp;doing so, will also lock in the price of your tickets &mdash; if they go up, you can still purchase them for the price they were when you found them, but if the price of your airfare drops during the wait, you''ll pay the lower price when you decide to purchase.</p>\r\n<h2 class="nolinks">11. Stray Boots</h2>\r\n<p>Exploring a new city doesn''t have to be about meandering around looking at tourist attractions. Turn your next trip into a real-life scavenger hunt with <a href="https://www.strayboots.com/" target="_blank">Stray Boots</a>. Download the app, select and purchase the tour you want to go on, then activate it while you''re connected to the Internet (you only need it to load beforehand, and it can be offline during play). The app will guide you through your chosen city while you and your friends or family members complete challenges, answer trivia questions, take fun photos and earn points. Along with many popular U.S. cities like New York and San Diego, you can also participate in scavenger hunts in London, Paris, Copenhagen and Tel Aviv.&nbsp;</p>\r\n<p>- See more at: http://www.businessnewsdaily.com/7992-travel-vacation-businesses.html#sthash.0eR9GqeS.dpuf</p>', 2, 1, '2016-12-27 12:51:28', '2016-12-27 12:51:28');
INSERT INTO `articles` (`id`, `title`, `category_id`, `content`, `author_id`, `status`, `created_at`, `updated_at`) VALUES
(12, '5 Job Skills You''ll Need in the Future', 6, '<p>Any career expert will tell you that keeping your professional skills sharp is the key to landing a job. What''s in demand today may be old news in a year or two, so it''s important to stay on top of your industry''s changing needs. Whether you''re actively searching for a new job or want to "future-proof" your resume for the coming years, here are five skills hiring managers will be looking for in tomorrow''s workforce. 1. SMAC (social, mobile, analytics, cloud) The "digital revolution" of recent years has brought with it a slew of new technology jobs that were unheard of just a decade ago. In the IT and gaming industries in particular, candidates skilled in social, mobile, analytics and cloud &mdash; collectively known as SMAC &mdash; have been hard to come by, said Santiago Pinedo, chief human resources officer of Etermax, the mobile games company behind Trivia Crack. "New technologies are evolving so fast that it&rsquo;s becoming almost impossible to keep pace from a talent supply standpoint," Pinedo said. "There will be a shortage of talent in positions such as data science, game analysis, full stack developers or SEM analysts, among others." Aside from training themselves in these highly demanded skills, Pinedo advises candidates to update their resumes to make them as simple, direct and visually attractive as possible. "The ability to be clear and assertive is now becoming more important than it ever was," he added. [See Related Story: The Best Tech Skills to List on Your Resume] 2. Health care IT Between the Affordable Care Act and new health management technologies, the health care industry is changing faster than ever. The problem, says Bryan Haardt, CEO of&nbsp;Decisio&nbsp;Health, is that companies in this field tend to focus more on research and development, rather than on improving their basic IT and process improvement. "It''s a difficult investment to make because very few people have the clinical experience, systems knowledge, and analytical prowess to make the necessary improvements and changes," Haardt told Business News Daily. "If you can combine clinical understanding with the skills of a process improvement analyst or data scientist, you''ll have a job for a lifetime." 3. Content creation and promotion In today''s social media-centric "selfie culture," everyone is a content creator. Eric Shashoua, CEO of Kiwi for Gmail, said knowing how to market yourself across multiple social networks and digital platforms is becoming an essential job skill, regardless of your industry. This is especially true of image-based promotion, as Shashoua says more and more people are now able to take and edit excellent photos very quickly and efficiently. "You really need to be good at taking photos of things and being able to edit them on your own," he said. &nbsp; Right now, Shashoua advised professionals to focus on learning social media managing and marketing, and staying abreast of the new emerging platforms. "Trying to learn how to market yourself on a social, digital medium &hellip; is what an online marketer has to be able to do," he added. "Once they master this, they have a major leg up on everything." 4. Customer support Shashoua said that social media isn''t just about content promotion: Many companies are now using social platforms for customer service, and support reps who can successfully navigate these digital channels will be in high demand. "Companies can live or die based on their presence on social media," Shashoua said. "You have to be really savvy on social media [with] customer support, because reviews and ratings and positive thoughts [stem from] how you treat your customers." 5. Communication and collaboration While these skills will always be valuable in any industry, entry-level job seekers pursuing fields like sales and product management should highlight their willingness to communicate and collaborate with others to learn the tricks of the trade, said David Pachter, co-founder of JumpCrew. "Marketing automation and &hellip; CRM [customer relationship management] software have leveled the playing field in our industry across people with a variety of experiences," said Pachter, whose company provides sales and marketing solution. "If companies believe your core collaboration, communication and leadership skills will be an asset ... you can qualify for roles that you may not have experience in." Pachter predicts that roles like sales and marketing associates, support operations and product/project management will all continue to see demand rise.&nbsp; "The 100,000 developers and admins who attend DreamForce are creating systems and customizing software for millions of new roles in an interconnected and collaborative workforce," he said. - See more at: http://www.businessnewsdaily.com/9643-future-job-skills.html#sthash.5gLG2au2.dpuf</p>', 2, 1, '2016-12-27 12:52:07', '2016-12-27 12:52:07'),
(13, 'An E-Cigarette Started a Small Fire on a Flight After It ‘Malfunctioned’', 6, '<p>&nbsp;</p>\r\n<h2 class="article-excerpt">No passengers were injured</h2>\r\n<div class="article-content">\r\n<p>(LITTLE ROCK, Ark.) &mdash; Officials say an <a href="http://fortune.com/fortune500/american-airlines-group-67/" target="_blank">American Airlines</a> flight from Dallas to Indianapolis was forced to make an emergency landing in Arkansas after a passenger&rsquo;s electronic cigarette &ldquo;malfunctioned&rdquo; and started a small fire.</p>\r\n<p>American Airlines spokesman Ross Feinstein says none of the 137 passengers or five crew members was injured, adding that crew members quickly extinguished the fire. The flight made the emergency landing Thursday afternoon at Little Rock&rsquo;s Clinton National Airport.</p>\r\n<p>Feinstein says a replacement plane was sent to Little Rock and all passengers arrived in Indianapolis later Thursday. He says the original plane was inspected in Dallas and no damage was discovered.</p>\r\n<p>Under Federal Aviation Administration rules , e-cigarettes are prohibited in checked luggage but allowed in carry-on bags. They may not be used on board the plane.</p>\r\n</div>', 2, 1, '2016-12-27 12:53:26', '2016-12-27 12:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `article_categories`
--

CREATE TABLE `article_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article_categories`
--

INSERT INTO `article_categories` (`id`, `name`, `slug`) VALUES
(2, 'Career', 'career'),
(3, 'Computer Science', 'computer-science'),
(5, 'Jobs', 'jobs'),
(6, 'Business', 'business');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(1, 'Afghanistan'),
(2, 'Aland Islands'),
(3, 'Albania'),
(4, 'Algeria'),
(5, 'American Samoa'),
(6, 'Andorra'),
(7, 'Angola'),
(8, 'Anguilla'),
(9, 'Antarctica'),
(10, 'Antigua and Barbuda'),
(11, 'Argentina'),
(12, 'Armenia'),
(13, 'Aruba'),
(14, 'Australia'),
(15, 'Austria'),
(16, 'Azerbaijan'),
(17, 'Bahamas'),
(18, 'Bahrain'),
(19, 'Bangladesh'),
(20, 'Barbados'),
(21, 'Belarus'),
(22, 'Belgium'),
(23, 'Belize'),
(24, 'Benin'),
(25, 'Bermuda'),
(26, 'Bhutan'),
(27, 'Bolivia Plurinational State of'),
(28, 'Bonaire Sint Eustatius and Saba'),
(29, 'Bosnia and Herzegovina'),
(30, 'Botswana'),
(31, 'Bouvet Island'),
(32, 'Brazil'),
(33, 'British Indian Ocean Territory'),
(34, 'Brunei Darussalam'),
(35, 'Bulgaria'),
(36, 'Burkina Faso'),
(37, 'Burundi'),
(38, 'Cambodia'),
(39, 'Cameroon'),
(40, 'Canada'),
(41, 'Cape Verde'),
(42, 'Cayman Islands'),
(43, 'Central African Republic'),
(44, 'Chad'),
(45, 'Chile'),
(46, 'China'),
(47, 'Christmas Island'),
(48, 'Cocos [Keeling, ] Islands'),
(49, 'Colombia'),
(50, 'Comoros'),
(51, 'Congo'),
(52, 'Congo the Democratic Republic of the'),
(53, 'Cook Islands'),
(54, 'Costa Rica'),
(55, 'Cote d''Ivoire'),
(56, 'Croatia'),
(57, 'Cuba'),
(58, 'Curacao'),
(59, 'Cyprus'),
(60, 'Czech Republic'),
(61, 'Denmark'),
(62, 'Djibouti'),
(63, 'Dominica'),
(64, 'Dominican Republic'),
(65, 'Ecuador'),
(66, 'Egypt'),
(67, 'El Salvador'),
(68, 'Equatorial Guinea'),
(69, 'Eritrea'),
(70, 'Estonia'),
(71, 'Ethiopia'),
(72, 'Falkland Islands [Malvinas, ]'),
(73, 'Faroe Islands'),
(74, 'Fiji'),
(75, 'Finland'),
(76, 'France'),
(77, 'French Guiana'),
(78, 'French Polynesia'),
(79, 'French Southern Territories'),
(80, 'Gabon'),
(81, 'Gambia'),
(82, 'Georgia'),
(83, 'Germany'),
(84, 'Ghana'),
(85, 'Gibraltar'),
(86, 'Greece'),
(87, 'Greenland'),
(88, 'Grenada'),
(89, 'Guadeloupe'),
(90, 'Guam'),
(91, 'Guatemala'),
(92, 'Guernsey'),
(93, 'Guinea'),
(94, 'Guinea-Bissau'),
(95, 'Guyana'),
(96, 'Haiti'),
(97, 'Heard Island and McDonald Islands'),
(98, 'Holy See, Vatican City State'),
(99, 'Honduras'),
(100, 'Hong Kong'),
(101, 'Hungary'),
(102, 'Iceland'),
(103, 'India'),
(104, 'Indonesia'),
(105, 'Iran Islamic Republic of'),
(106, 'Iraq'),
(107, 'Ireland'),
(108, 'Isle of Man'),
(109, 'Israel'),
(110, 'Italy'),
(111, 'Jamaica'),
(112, 'Japan'),
(113, 'Jersey'),
(114, 'Jordan'),
(115, 'Kazakhstan'),
(116, 'Kenya'),
(117, 'Kiribati'),
(118, 'Korea, Democratic People''s Republic of'),
(119, 'Korea, Republic of'),
(120, 'Kuwait'),
(121, 'Kyrgyzstan'),
(122, 'Lao People''s Democratic Republic'),
(123, 'Latvia'),
(124, 'Lebanon'),
(125, 'Lesotho'),
(126, 'Liberia'),
(127, 'Libyan Arab Jamahiriya'),
(128, 'Liechtenstein'),
(129, 'Lithuania'),
(130, 'Luxembourg'),
(131, 'Macao'),
(132, 'Macedonia'),
(133, 'Madagascar'),
(134, 'Malawi'),
(135, 'Malaysia'),
(136, 'Maldives'),
(137, 'Mali'),
(138, 'Malta'),
(139, 'Marshall Islands'),
(140, 'Martinique'),
(141, 'Mauritania'),
(142, 'Mauritius'),
(143, 'Mayotte'),
(144, 'Mexico'),
(145, 'Micronesia Federated States of'),
(146, 'Moldova'),
(147, 'Monaco'),
(148, 'Mongolia'),
(149, 'Montenegro'),
(150, 'Montserrat'),
(151, 'Morocco'),
(152, 'Mozambique'),
(153, 'Myanmar'),
(154, 'Namibia'),
(155, 'Nauru'),
(156, 'Nepal'),
(157, 'Netherlands'),
(158, 'New Caledonia'),
(159, 'New Zealand'),
(160, 'Nicaragua'),
(161, 'Niger'),
(162, 'Nigeria'),
(163, 'Niue'),
(164, 'Norfolk Island'),
(165, 'Northern Mariana Islands'),
(166, 'Norway'),
(167, 'Oman'),
(168, 'Pakistan'),
(169, 'Palau'),
(170, 'Palestinian Territory Occupied'),
(171, 'Panama'),
(172, 'Papua New Guinea'),
(173, 'Paraguay'),
(174, 'Peru'),
(175, 'Philippines'),
(176, 'Pitcairn'),
(177, 'Poland'),
(178, 'Portugal'),
(179, 'Puerto Rico'),
(180, 'Qatar'),
(181, 'Reunion'),
(182, 'Romania'),
(183, 'Russian Federation'),
(184, 'Rwanda'),
(185, 'Saint Barthelemy'),
(186, 'Saint Helena Ascension and Tristan da Cunha'),
(187, 'Saint Kitts and Nevis'),
(188, 'Saint Lucia'),
(189, 'Saint Martin [French part]'),
(190, 'Saint Pierre and Miquelon'),
(191, 'Saint Vincent and the Grenadines'),
(192, 'Samoa'),
(193, 'San Marino'),
(194, 'Sao Tome and Principe'),
(195, 'Saudi Arabia'),
(196, 'Senegal'),
(197, 'Serbia'),
(198, 'Seychelles'),
(199, 'Sierra Leone'),
(200, 'Singapore'),
(201, 'Sint Maarten [Dutch part]'),
(202, 'Slovakia'),
(203, 'Slovenia'),
(204, 'Solomon Islands'),
(205, 'Somalia'),
(206, 'South Africa'),
(207, 'South Georgia and the South Sandwich Islands'),
(208, 'South Sudan'),
(209, 'Spain'),
(210, 'Sri Lanka'),
(211, 'Sudan'),
(212, 'Suriname'),
(213, 'Svalbard and Jan Mayen'),
(214, 'Swaziland'),
(215, 'Sweden'),
(216, 'Switzerland'),
(217, 'Syrian Arab Republic'),
(218, 'Taiwan'),
(219, 'Tajikistan'),
(220, 'Tanzania United Republic of'),
(221, 'Thailand'),
(222, 'Timor-Leste'),
(223, 'Togo'),
(224, 'Tokelau'),
(225, 'Tonga'),
(226, 'Trinidad and Tobago'),
(227, 'Tunisia'),
(228, 'Turkey'),
(229, 'Turkmenistan'),
(230, 'Turks and Caicos Islands'),
(231, 'Tuvalu'),
(232, 'Uganda'),
(233, 'Ukraine'),
(234, 'United Arab Emirates'),
(235, 'United Kingdom'),
(236, 'United States'),
(237, 'United States Minor Outlying Islands'),
(238, 'Uruguay'),
(239, 'Uzbekistan'),
(240, 'Vanuatu'),
(241, 'Venezuela, Bolivarian Republic of'),
(242, 'Viet Nam'),
(243, 'Virgin Islands British'),
(244, 'Virgin Islands U.S.'),
(245, 'Wallis and Futuna'),
(246, 'Western Sahara'),
(247, 'Yemen'),
(248, 'Zambia'),
(249, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `etemplates`
--

CREATE TABLE `etemplates` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject` mediumtext NOT NULL,
  `body` mediumtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `etemplates`
--

INSERT INTO `etemplates` (`id`, `name`, `subject`, `body`, `status`) VALUES
(1, 'Order:Activation Email', '{business_name} Order Activation', '<p>{logo}</p>\r\n<div style="line-height: 1.6; color: #222; text-align: left; width: 550px; font-size: 10pt; margin: 0px 10px; font-family: verdana,''droid sans'',''lucida sans'',sans-serif; padding: 14px; border: 3px solid #d8d8d8; border-top: 3px solid #8bc039;">\r\n<div style="padding: 5px;">{activation_message}</div>\r\n</div>', 1),
(2, 'Tickets:Update', 'Update to Ticket [TID-{ticket_number}]', '<p class="logo">{logo}</p>\r\n<div><strong>Subject:</strong>{ticket_subject}\r\n<div style="padding: 5px;">{ticket_contents}</div>\r\n</div>', 1),
(5, 'Order:Order Confirmation', '{business_name} Order Confirmation - {order_num}', '<p class="logo">{logo}</p>\r\n<div>\r\n<div style="padding: 5px; font-size: 11pt; font-weight: bold;">Greetings,</div>\r\n<div style="padding: 5px;">Thank you for choosing <strong>{business_name}. </strong></div>\r\n<div style="padding: 5px;">Here is your order details-</div>\r\n<div style="padding: 10px 5px;">Date: {order_date} <br /> Order Number: {order_num} <br /> Invoice ID: {invoice_id}</div>\r\n<div style="padding: 5px;">Should you have any questions in regards to this order, please feel free to contact our support department by creating a new ticket from your Client Portal</div>\r\n<div style="padding: 0px 5px;">\r\n<div>Best Regards,<br />{business_name} Team</div>\r\n</div>\r\n</div>', 1),
(6, 'Invoice:Invoice Payment Confirmation', '{business_name} Invoice - Payment Confirmation', '<p>{logo}</p>\r\n<div style="line-height: 1.6; color: #222; text-align: left; width: 550px; font-size: 10pt; margin: 0px 10px; font-family: verdana,''droid sans'',''lucida sans'',sans-serif; padding: 14px; border: 3px solid #d8d8d8; border-top: 3px solid #8bc039;">\r\n<div style="padding: 5px; font-size: 11pt; font-weight: bold;">Greetings,</div>\r\n<div style="padding: 5px;">This email serves as your official invoice from <strong>{business_name}. </strong></div>\r\n<div style="padding: 5px;">Login to your client Portal to view this invoice.</div>\r\n<div style="padding: 10px 5px;">Log in URL: <a style="color: #1da9c0; font-weight: bold; padding: 3px; text-decoration: none;" href="{{sys-url}}/cp/login.php" target="_blank">{sys_url}/cp/login.php</a><br />Invoice ID: {invoice_id}<br />Invoice Amount: {invoice_amount}</div>\r\n<div style="padding: 5px;">Should you have any questions in regards to this invoice or any other billing related issue, please feel free to contact the Billing department by creating a new ticket from your Client Portal</div>\r\n<div style="padding: 0px 5px;">\r\n<div>Best Regards,<br />{business_name} Team</div>\r\n</div>\r\n</div>', 1),
(7, 'Invoice:Invoice Created', '{business_name} Invoice', '<p>{logo}</p>\r\n<div style="line-height: 1.6; color: #222; text-align: left; width: 550px; font-size: 10pt; margin: 0px 10px; font-family: verdana,''droid sans'',''lucida sans'',sans-serif; padding: 14px; border: 3px solid #d8d8d8; border-top: 3px solid #8bc039;">\r\n<div style="padding: 5px; font-size: 11pt; font-weight: bold;">Greetings,</div>\r\n<div style="padding: 5px;">This email serves as your official invoice from <strong>{business_name}. </strong></div>\r\n<div style="padding: 5px;">Login to your client Portal to view this invoice.</div>\r\n<div style="padding: 10px 5px;">Log in URL: <a style="color: #1da9c0; font-weight: bold; padding: 3px; text-decoration: none;" href="{{sys-url}}/cp/login.php" target="_blank">{sys_url}/cp/login.php</a><br />Invoice ID: {invoice_id}<br />Invoice Amount: {invoice_amount}</div>\r\n<div style="padding: 5px;">Should you have any questions in regards to this invoice or any other billing related issue, please feel free to contact the Billing department by creating a new ticket from your Client Portal</div>\r\n<div style="padding: 0px 5px;">\r\n<div>Best Regards,<br />{business_name} Team</div>\r\n</div>\r\n</div>', 1),
(8, 'Tickets:New Ticket', '[TID-{ticket_number}] A new ticket has been created for you', '<p class="logo">{logo}</p>\r\n<div>\r\n<div><strong>Subject:</strong>{ticket_subject}</div>\r\n<div style="padding: 5px;">{ticket_contents}</div>\r\n</div>', 1),
(9, 'Details:Signup', 'Welcome to {business_name}', '<p class="logo">{logo}</p>\r\n<div>\r\n<div style="padding: 5px; font-weight: bold;">Hi {name},</div>\r\n<div style="padding: 5px;">Thanks for signing up for <strong>{business_name}! </strong>, the most powerful Business Management Application Around the globe!</div>\r\n<div style="padding: 5px;">Start by logging in: Use following details to login.</div>\r\n<div style="padding: 10px 5px;">Log in URL: <a style="color: #1da9c0; font-weight: bold; padding: 3px; text-decoration: none;">{sys_url}/cp/login.php</a><br />Username: {username}<br />Password: {password}</div>\r\n<div style="padding: 5px;">If you have any questions or need assistance, please don''t hesitate to contact us.</div>\r\n<div style="padding: 0px 5px;">\r\n<div>Best Regards,<br />{business_name} Team</div>\r\n</div>\r\n</div>', 1),
(10, 'Send Inovice PDF', '{business_name} Invoice PDF - {invoice_id}', '<p class="logo">{logo}</p>\r\n<div>\r\n<div style="padding: 5px; font-size: 11pt; font-weight: bold;">Greetings,</div>\r\n<div style="padding: 5px;">Thank you for choosing <strong>{business_name}. </strong></div>\r\n<div style="padding: 5px;">Here is your order details-</div>\r\n<div style="padding: 10px 5px;">Date: {order_date} <br /> Invoice ID: {invoice_id}</div>\r\n<div style="padding: 5px;">Please find the attachment below, the attachement included your invoice.</div>\r\n<div style="padding: 0px 5px;">\r\n<div>Best Regards,<br />{business_name} Team</div>\r\n</div>\r\n</div>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `faq_category_id` int(11) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `faq_category_id`, `content`, `title`, `created_at`, `updated_at`) VALUES
(1, NULL, 'You can easily create a ticket navigating `Support` menu and click `Create New Ticket', 'How can I create a ticket ?', '2016-12-28 11:21:13', '2016-12-28 11:21:13'),
(2, NULL, 'By Navigating Billing Menu you will get a submenu name `My Transactions` and clicking this menu you will get your transactions.', 'How can I see my all transaction ?', '2016-12-28 11:23:07', '2016-12-28 11:23:07'),
(3, NULL, 'To order a product you have to login, if you are a new user you have to register first, then after login you can easily order a product/service item.', 'How to order a product/service ?', '2016-12-28 11:24:32', '2016-12-28 11:24:32'),
(4, NULL, 'Hover person logo from the top menu you will get two item login and register, if you click on register button you can easily register to crm.', 'How to get Register.', '2016-12-28 12:27:18', '2016-12-28 12:27:18'),
(5, NULL, 'Yes, it''s required to login to buy a product/service.', 'Is it require to login for buy a product/service.', '2016-12-28 12:28:10', '2016-12-28 12:28:10'),
(6, NULL, 'You will get the ticket reply as soon as possible. ', 'When I will get the ticket reply ?', '2016-12-28 12:28:46', '2016-12-28 12:28:46'),
(7, NULL, 'You will get your active service/product by navigate ''Products/Services'' Menu', 'How can I view my active product/services.', '2016-12-28 12:30:12', '2016-12-28 12:30:12'),
(8, NULL, 'Yes, you can by navigating billing menu.', 'Can I get my invoice list ?', '2016-12-28 12:31:21', '2016-12-28 12:31:21');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `note` mediumtext,
  `price` int(11) DEFAULT NULL,
  `invoice_status_id` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `recurring_status` int(11) DEFAULT NULL,
  `recurring_frequency` varchar(100) DEFAULT NULL,
  `recurring_times` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` float(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_statuses`
--

CREATE TABLE `invoice_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_statuses`
--

INSERT INTO `invoice_statuses` (`id`, `name`) VALUES
(1, 'Unpaid'),
(2, 'Paid'),
(3, 'Canceled');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_02_04_052334_entrust_setup_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `notice` mediumtext NOT NULL,
  `status` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `client_id`, `title`, `notice`, `status`, `created_at`, `updated_at`) VALUES
(6, NULL, 'Title One', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '2016-12-02 09:04:38', '2016-12-02 09:04:38'),
(10, NULL, 'jQuery live search', 'nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor inLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod reprehenderit in voluptate velit esses', 0, '2016-12-03 19:44:54', '2016-12-02 09:24:10'),
(11, NULL, 'r sit amet, consec', 'has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp', 0, '2016-12-02 09:48:01', '2016-12-02 09:48:01'),
(12, NULL, ' a long established fact that a re', 't look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, som', 0, '2016-12-02 09:48:40', '2016-12-02 09:48:40'),
(13, NULL, 'Where can I get some?', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the ', 0, '2016-12-02 09:49:00', '2016-12-02 09:49:00'),
(14, NULL, 'The standard Lorem Ipsum passage, used since the 1500s', ' ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore ', 0, '2016-12-02 09:49:26', '2016-12-02 09:49:26'),
(17, NULL, 'The Notice', 'For All Client', 0, '2017-01-30 12:45:18', '2017-01-30 12:45:18'),
(18, 2, 'Notice', 'For John Due', 0, '2017-01-30 12:45:44', '2017-01-30 12:45:44'),
(19, 5, 'For Another Client', 'Notice here', 0, '2017-01-30 12:58:08', '2017-01-30 12:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` varchar(225) NOT NULL,
  `note` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('robinsabbir@gmail.com', 'cb21beeca386d1dee557c80f104fd476945abef8084341b64acdee257f4f8170', '2016-11-07 10:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `setting` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `name`, `setting`, `value`, `status`) VALUES
(1, 'Paypal', 'Email Address', 'crm@website.com', 1),
(2, 'Bank Payment', 'Account Info', 'Premium Bank Limited\r\nJohn Due\r\nA/C no 803947539485743\r\nMaster Branch', 1),
(3, 'Stripe', 'Email Account', 'crm@w3bd.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` mediumtext NOT NULL,
  `variation` mediumtext,
  `product_category_id` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `show_client` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `details`, `variation`, `product_category_id`, `price`, `status`, `quantity`, `show_client`, `created_at`, `updated_at`) VALUES
(1, 'VPS Hosting Server', '<p>=&gt; VPS Hosting</p>\r\n<p>=&gt; Dedicated Server</p>\r\n<p>=&gt; Yearly Charge</p>', NULL, NULL, 234, 0, 100, 0, '2017-01-15 07:41:41', '2016-12-28 12:05:13'),
(2, 'Computer Service', '<p>Service your computer from home !</p>', NULL, 1, 200, 1, 1000, 1, '2017-01-15 07:38:37', '2016-12-28 12:13:17'),
(3, 'Computer Service', '<p>Hey Guys, I am going to sell a service.</p>\r\n<p>Thank You</p>', NULL, 1, 23, 0, 20, 0, '2017-01-15 07:35:11', '2017-01-15 01:28:39'),
(5, 'PRduct names one', '<p>The Product Details here</p>', '{"1":{"name":"Var one","price":"23"},"2":{"name":"Var two","price":"23"}}', 2, 59, 0, 23, 0, '2017-01-15 10:49:27', '2017-01-15 04:14:03'),
(6, 'Product Name', '<p>Product Details</p>', '{"1":{"name":"Variation One","price":"25"},"2":{"name":"Variation two","price":"23"}}', 3, 2323, 0, 23, 0, '2017-01-15 11:01:46', '2017-01-15 04:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`) VALUES
(1, 'Medias', 'Medias'),
(2, 'Technology', 'Technology'),
(3, 'Information Technology', 'Information-Technology');

-- --------------------------------------------------------

--
-- Table structure for table `recurrings`
--

CREATE TABLE `recurrings` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `next_due` date NOT NULL,
  `recurring_type` int(11) DEFAULT NULL,
  `how_many` int(11) DEFAULT NULL,
  `note` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recurring_types`
--

CREATE TABLE `recurring_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recurring_types`
--

INSERT INTO `recurring_types` (`id`, `name`) VALUES
(1, 'Monthly'),
(2, 'Every 3 Months'),
(3, 'Every 6 Months'),
(4, 'Yearly'),
(5, 'Quarterly'),
(6, 'Every Two Years');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` text,
  `payload` text NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `default_email` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `pay_address` mediumtext,
  `default_country_id` varchar(11) DEFAULT '1',
  `default_currency` varchar(100) DEFAULT NULL,
  `default_currency_symbol` varchar(100) DEFAULT NULL,
  `theme` int(11) DEFAULT '1',
  `website_title` varchar(255) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `copyright_text` mediumtext,
  `client_panel` varchar(255) DEFAULT NULL,
  `tax_id` int(11) DEFAULT '1',
  `cron_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `company_name`, `default_email`, `logo`, `pay_address`, `default_country_id`, `default_currency`, `default_currency_symbol`, `theme`, `website_title`, `brand_name`, `copyright_text`, `client_panel`, `tax_id`, `cron_url`, `created_at`, `updated_at`) VALUES
(1, 'System', 'robinbdseod1@gmail.com', 'logo.png', 'W3BD,\r\nWorld Width Web Development Bangladesh,\r\nX Plaza, House 3, Road 4, Section 2, Mirpur\r\nDhaka, Bangladesh', NULL, NULL, '$', 1, 'Our System', 'System', 'All rights reserved.', 'http://website.com/client', 1, NULL, '2016-12-13 14:40:16', '2016-10-26 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting_sliders`
--

CREATE TABLE `setting_sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `show_client` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `name`, `email`, `show_client`) VALUES
(4, 'Sales CRM', 'sales@crm.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `taxs`
--

CREATE TABLE `taxs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxs`
--

INSERT INTO `taxs` (`id`, `name`, `value`, `type`) VALUES
(1, 'Text Per Service', '10', 'Income Text');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `answered` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_categories`
--

CREATE TABLE `ticket_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_categories`
--

INSERT INTO `ticket_categories` (`id`, `name`) VALUES
(1, 'Domain Support'),
(2, 'Hosting Support'),
(3, 'Business Support'),
(4, 'Sales Support'),
(5, 'Billing Support');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_messages`
--

CREATE TABLE `ticket_messages` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `message_from` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `id` int(11) NOT NULL,
  `title` mediumtext NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`id`, `title`, `active`, `created_at`, `updated_at`) VALUES
(36, 'Need to list of pending work', 1, '2016-12-30 19:45:39', '2016-12-27 12:12:41'),
(37, 'Need to make a call to customer', 0, '2017-01-12 18:37:38', '2016-12-27 12:13:07'),
(38, 'Meeting time 12 PM', 0, '2017-01-26 18:21:35', '2016-12-27 12:13:32'),
(39, 'Call to Merilla Grande', 0, '2017-01-26 18:21:34', '2016-12-27 12:14:28'),
(40, 'Making list of existing client', 0, '2017-01-26 18:21:33', '2016-12-27 12:15:27'),
(41, 'Meeting With Foreign Client', 0, '2017-01-26 18:21:33', '2016-12-28 10:44:51'),
(42, 'how', 1, '2016-12-29 11:44:19', '2016-12-29 11:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `transaction_type_id` int(11) DEFAULT NULL,
  `invoice_id` int(10) DEFAULT NULL,
  `for` int(11) DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `memo` mediumtext,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

CREATE TABLE `transaction_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_types`
--

INSERT INTO `transaction_types` (`id`, `name`) VALUES
(1, 'Income'),
(2, 'Expense'),
(3, 'Transfer');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `photo` varchar(225) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `username`, `photo`, `email`, `user_role_id`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Robin', 'Hossains', 'robinsabbir@gmail.com', '147871694439_logo.jpg', 'robinsabbir@gmail.com', 1, '$2y$10$eHTBLiWMl2kz08DOIzyqrO//FoHfqhsyDbjBZl8oYeo5vmKEFjS6C', 'rkBeeA2AOIcRcms7BfX7QMfE6wo33c2ms3uNi5WnZvJcaMbrKkAQXW7meapg', '2016-10-26 18:00:00', '2017-02-11 22:14:54'),
(10, 'John', 'Due', 'john_due', NULL, 'john_due@cygo.com', 3, 'john_due@cygo.com', NULL, '2016-12-27 12:09:23', '2016-12-27 12:09:23'),
(11, 'Admin', 'Demo', 'admin-demo@w3bd.com', NULL, 'admin-demo@w3bd.com', 1, '$2y$10$Rf4msjIwjk4NCwbobwAXAuYxw9DAH/oZ3gfeAhJTNjxaeWQNw0pQq', 'R13OabUsW8YmrVR6FqtODMGJqpk0velui4l5Swwd3oNdRI0ObwAcLK46OO5Y', '2016-12-30 11:48:31', '2016-12-30 11:50:53'),
(12, 'hello', 'world', NULL, NULL, 'worldhello500@gmail.com', NULL, '$2y$10$4Bn31rs/pLK8CtclbhV/U.4hEHzFopGraOdwQBAv78aBM6cWnELeS', 'UDMRlmbpGjtY7z51u0G6ctJQCQJd720LT4neZxhicpYWOjuMxFzO7gVurxpD', '2017-01-10 01:10:37', '2017-01-10 01:11:25'),
(13, 'John', 'Due', 'john_due@yahoo.com', '147871694439_logo.jpg', 'john_due@yahoo.com', 1, '$2y$10$cUCNrcraT7iCBu/YQ7Uzde.ddgmVOdiqFrvqKVnb9zJIh3aQ7APhO', 'YC5JaKQcdJKm8N5xi2i3UrAdDpUkfcaXyd66Q2uUPc3CU1TTPDNT6P9ZTvuW', '2017-01-12 00:50:59', '2017-01-12 00:53:30'),
(14, 'Testf', 'TestL', NULL, NULL, 'test@ltest.com', NULL, '$2y$10$2w5ybQDt7W9JvItcixw9gOJkLV5PYfG5DakUMxix5PTj/ruJKbmOm', 'xSj7hb1X0p04MTB6dmBvlq4Y3u9GuVpRyWAJ6V2PvXnwKGJPQL5DYEsegIe9', '2017-02-02 12:13:51', '2017-02-02 12:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Editor'),
(3, 'Manager'),
(4, 'Client'),
(5, 'Author');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_groups`
--
ALTER TABLE `account_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_categories`
--
ALTER TABLE `article_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `etemplates`
--
ALTER TABLE `etemplates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_statuses`
--
ALTER TABLE `invoice_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recurrings`
--
ALTER TABLE `recurrings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recurring_types`
--
ALTER TABLE `recurring_types`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_sliders`
--
ALTER TABLE `setting_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxs`
--
ALTER TABLE `taxs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_categories`
--
ALTER TABLE `ticket_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_messages`
--
ALTER TABLE `ticket_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_types`
--
ALTER TABLE `transaction_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `account_groups`
--
ALTER TABLE `account_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `article_categories`
--
ALTER TABLE `article_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;
--
-- AUTO_INCREMENT for table `etemplates`
--
ALTER TABLE `etemplates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `invoice_statuses`
--
ALTER TABLE `invoice_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `recurrings`
--
ALTER TABLE `recurrings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting_sliders`
--
ALTER TABLE `setting_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `taxs`
--
ALTER TABLE `taxs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ticket_categories`
--
ALTER TABLE `ticket_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ticket_messages`
--
ALTER TABLE `ticket_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `transaction_types`
--
ALTER TABLE `transaction_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
