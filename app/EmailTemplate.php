<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = 'etemplates';
    protected $fillable = ['id','name','subject','body','status'];
    public $timestamps = false;
}
