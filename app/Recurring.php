<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurring extends Model
{
    protected $table = 'recurrings';
    protected $fillable = ['invoice_id','account_id','next_due','recurring_type','how_many','note'];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice', 'invoice_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

}
