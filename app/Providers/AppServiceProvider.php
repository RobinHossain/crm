<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
	    view()->composer('layouts.app', function($view)
	    {
		    $settings = DB::table('settings')->where('id',1)->first();
		    $authID = Auth::user()->user_role_id;
		    $getRoleInfo = DB::table('user_roles')->where('id', $authID)->first();
		    $settings = (object) array_merge((array) $settings, (array) $getRoleInfo);
		    $view->with('configurations', $settings);
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('mailgun.client', function() {
            $client = new \GuzzleHttp\Client([
                // your configuration
            ]);

            return new \Http\Adapter\Guzzle6\Client($client);
        });
        //
    }
}
