<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
    protected $table = 'invoice_statuses';
    protected $fillable = ['name'];
    public $timestamps = false;

}
