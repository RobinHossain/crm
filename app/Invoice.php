<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $fillable = ['reference', 'account_id', 'invoice_item_id', 'price', 'tax_id', 'note', 'invoice_status_id', 'status', 'due_date', 'recurring_status', 'recurring_frequency', 'recurring_times'];

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

    public function item()
    {
        return $this->belongsTo('App\InvoiceItem', 'invoice_item_id');
    }

    public function status()
    {
        return $this->belongsTo('App\InvoiceStatus', 'invoice_status_id');
    }

    public function tax()
    {
        return $this->belongsTo('App\Tax', 'tax_id');
    }


}
