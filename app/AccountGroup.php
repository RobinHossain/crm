<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountGroup extends Model
{
    protected $table = 'account_groups';
    protected $fillable = ['name'];
    public $timestamps = false;
}
