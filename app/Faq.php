<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';
    protected $fillable = ['faq_category_id','title','content'];

    public function category()
    {
        return $this->belongsTo('App\FaqCategory', 'faq_category_id');
    }
}
