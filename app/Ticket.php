<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = ['client_id','subject','ticket_category_id','status','answered'];

    public function account()
    {
        return $this->belongsTo('App\Account', 'client_id');
    }

    public function category()
    {
        return $this->belongsTo('App\TicketCategory', 'ticket_category_id');
    }
}
