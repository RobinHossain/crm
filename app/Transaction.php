<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = ['transaction_type_id', 'for', 'from', 'amount', 'memo', 'date'];

    public function type()
    {
        return $this->belongsTo('App\TransactionType', 'transaction_type_id');
    }

    public function tfor()
    {
        return $this->belongsTo('App\Account', 'for');
    }

    public function tfrom()
    {
        return $this->belongsTo('App\Account', 'from');
    }
}
