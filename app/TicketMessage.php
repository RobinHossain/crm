<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $table = 'ticket_messages';
    protected $fillable = ['ticket_id','message','message_from','user_id','date','file'];
}
