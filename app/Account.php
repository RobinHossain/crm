<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //

    protected $table = 'accounts';
    protected $dates = [ 'created_at', 'updated_at' ];
    protected $fillable = ['fname', 'lname', 'company', 'email', 'password', 'balance', 'account_type_id', 'notification_status', 'phone', 'group_id', 'address_1', 'address_2',
        'city', 'state', 'zipcode', 'country_id'];

    public function type()
    {
        return $this->belongsTo('App\AccountType', 'account_type_id');
    }

    public function group(){
        return $this->belongsTo('App\AccountGroup', 'group_id');
    }

    public function country(){
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function role(){
        return $this->belongsTo('App\UserRole', 'role_id');
    }


}
