<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingSliders extends Model
{
    //
    protected $table = 'setting_sliders';
    protected $fillable = ['title', 'image'];
}
