<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = [ 'created_at', 'updated_at' ];

    protected $fillable = [
        'fname', 'lname', 'username', 'email', 'user_role_id', 'password','photo'
    ];

	public function role()
	{
		return $this->belongsTo('App\UserRole', 'user_role_id');
	}

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
