<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = ['company_name','default_email','logo','pay_address','default_country_id','default_currency','default_currency_symbol','theme','website_title','brand_name','copyright_text','client_panel','tax_id'];
}
