<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\EmailTemplate;
use App\Invoice;
use App\Mail\OrderShipped;
use App\Setting;
use App\Todo;
use App\Transaction;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Bogardo\Mailgun\Facades\Mailgun;
use Bogardo\Mailgun\Mail\Message;
use Illuminate\Support\Facades\URL;
use Validator;
//use PDF;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class TestController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function where(){
        return view('errors.404');
    }


    public function pdfGet(){
        $data = "Title Here";
        $cars = array("Volvo", "BMW", "Toyota");
        $pdf = \PDF::loadView('pdf.invoice', $cars);
        return $pdf->download('invoice.pdf');

    }

    public function some(Request $request)
    {

        $getTemplate = EmailTemplate::where('id',9)->first();
        $getSettings = Setting::where('id',1)->first();

        $templateBody = $getTemplate->body;
        $subject_pre = $getTemplate->subject;

        // Generate Variable arrays
        $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
        $sysUrl = URL::to('/');

        $arrToExtract = array();

        if($request->input('fname') &&  $request->input('lname')){
            $arrToExtract['name'] = $request->input('fname').' '.$request->input('lname');
        }

        if($request->input('username') && $request->input('password')){
            $arrToExtract['username'] = $request->input('username');
            $arrToExtract['password'] = $request->input('lname');
        }

        $arrToExtract['business_name'] = $getSettings->company_name;
        $arrToExtract['logo'] = $getLogoUrl;
        $arrToExtract['sys_url'] = $sysUrl;
        // Generate Variable arrays


//        $subject = replaceWithEtemplateVar($subject_pre,$arrToExtract);
        $body = replaceWithEtemplateVar($templateBody,$arrToExtract);

        return view('emails.signup_manual', compact('body'));



    }

    public function sendMail(){
        $data = [
            'customer' => 'John Smith',
            'url' => 'http://laravel.com'
        ];

        Mailgun::send('emails.welcome', $data, function ($message) {
            $message->to('robinsabbir@gmail.com', 'John Smith')->subject('Welcome!');
        });
    }

}
