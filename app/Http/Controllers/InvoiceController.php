<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\EmailTemplate;
use App\Invoice;
use App\InvoiceItem;
use App\Setting;
use App\Tax;
use App\User;
use App\UserRole;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\URL;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Bogardo\Mailgun\Facades\Mailgun;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;
use App\Transaction;


class InvoiceController extends Controller
{
    use FormAccessible;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $title = "All Invoice";
        $invoices = Invoice::with('account', 'status')->orderBy('invoices.created_at', 'desc')->get();
        return view('invoice.index', compact('title', 'invoices'));
    }

    public function addGet(){
        $title = "Add Invoice";
        $get_account_list = Account::all('id', 'fname', 'lname');
        $get_product_list = Product::all('id', 'name', 'price');
        $accounts = array();
        $products = array();
        foreach($get_account_list as $acl){
            $accounts[$acl->id] = $acl->fname." ".$acl->lname;
        }
        foreach($get_product_list as $prdct){
            $products[$prdct->id] = $prdct->name;
        }
        $tax_list = Tax::all('id','name','value');
        $taxs = array();
        foreach($tax_list as $tax){
            $taxs[$tax->id] = $tax->name." ($".$tax->value.")";
        }
        return view('invoice.add', compact('title', 'accounts', 'products', 'taxs'));
    }

    public function manageGet($id = null){
        $title = "Manage Invoices";
        $invoice = Invoice::with('account', 'status')->where('id', $id)->first();
        $setting = Setting::where('id', 1)->first();
        $invoiceItems = InvoiceItem::with('product')->where('invoice_id', $invoice->id)->get();



        return view('invoice.manage', compact('title', 'invoice', 'setting', 'invoiceItems', 'total_price'));
    }

    public function markUnpaidGet($id){
        $changeStatus = DB::table('invoices')->where('id', $id)->update([
            'invoice_status_id' => 1
        ]);
        if($changeStatus) {
            return redirect('invoice/manage/' . $id)->send();
        }
    }

    public function markPaidGet($id){
        $changeStatus = DB::table('invoices')->where('id', $id)->update([
            'invoice_status_id' => 2
        ]);
        if($changeStatus) {
            return redirect('invoice/manage/' . $id)->send();
        }
    }

    public function markCancelGet($id){
        $changeStatus = DB::table('invoices')->where('id', $id)->update([
            'invoice_status_id' => 3
        ]);
        if($changeStatus){
            return redirect('invoice/manage/'.$id)->send();
        }

    }

    public function removeCancelGet($id){
        $changeStatus = DB::table('invoices')->where('id', $id)->update([
            'invoice_status_id' => 1
        ]);
        if($changeStatus) {
            return redirect('invoice/manage/' . $id)->send();
        }
    }

    public function sendEmailGet($id){
        if($id){
            $getInvoice = Invoice::with('account', 'status')->where('id', $id)->first();
            $invoice = $getInvoice->toArray();

            $getInvoiceItems = InvoiceItem::with('product')->where('invoice_id', $getInvoice->id)->get();
            $invoice['invoiceItem'] = $getInvoiceItems->toArray();

            $setting = Setting::where('id', 1)->first();
            $invoice['setting'] = $setting->toArray();


            $pdf = PDF::loadView('invoice.pdf', $invoice)->setPaper('a4')->save( 'pdf/invoice.pdf' );

            if($pdf){

                $customerId = $getInvoice->account_id;

                $customerInfo = Account::where('id', $customerId)->select('fname','lname','email')->first();

                $getTemplate = EmailTemplate::where('id',5)->first();
                $getSettings = Setting::where('id',1)->first();

                $templateBody = $getTemplate->body;
                $subject_pre = $getTemplate->subject;

                // Generate Variable arrays
                $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
                $sysUrl = URL::to('/');

                $arrToExtract = array();
                $arrToExtract['name'] = $customerInfo->fname.' '.$customerInfo->lname;
                $arrToExtract['email'] = $customerInfo->email;

                $arrToExtract['business_name'] = $getSettings->company_name;
                $arrToExtract['logo'] = $getLogoUrl;
                $arrToExtract['sys_url'] = $sysUrl;

                $arrToExtract['order_date'] = $getInvoice->created_at;
                $arrToExtract['invoice_id'] = $getInvoice->id;
                // Generate Variable arrays


                $body = replaceWithEtemplateVar($templateBody,$arrToExtract);
                $subject = replaceWithEtemplateVar($subject_pre, $arrToExtract);

                $mailUse = array('to' => $arrToExtract['email'], 'name' => $arrToExtract['name'], 'subject' => $subject);
                $data = array('body' => $body);

                Mailgun::send(['html' => 'emails.sendinvoicepdf'], $data, function ($message) use($mailUse) {
                    $message
                        ->to($mailUse['to'], $mailUse['name'])
                        ->subject($mailUse['subject'])
                        ->attach('pdf/invoice.pdf');
                });
            }
            Session::flash('message', 'Email Send to the Client !');
        }else{
            Session::flash('error', 'Can not send the email!');
        }
        return redirect('invoice/manage/' . $id)->send();
    }

    public function downloadInvoice($id){
        $getInvoice = Invoice::with('account', 'status')->where('id', $id)->first();
        $invoice = $getInvoice->toArray();

        $getInvoiceItems = InvoiceItem::with('product')->where('invoice_id', $getInvoice->id)->get();
        $invoice['invoiceItem'] = $getInvoiceItems->toArray();

        $setting = Setting::where('id', 1)->first();
        $invoice['setting'] = $setting->toArray();
//        return view('invoice.pdfc', $invoice);
//        pr($invoice);
        $pdf = PDF::loadView('invoice.pdf', $invoice)->setPaper('a4');
        return $pdf->download('invoice.pdf');
    }

    public function viewInvoicePdf($id){
        $getInvoice = Invoice::with('account', 'status')->where('id', $id)->first();
        $invoice = $getInvoice->toArray();

        $getInvoiceItems = InvoiceItem::with('product')->where('invoice_id', $getInvoice->id)->get();
        $invoice['invoiceItem'] = $getInvoiceItems->toArray();

        $setting = Setting::where('id', 1)->first();
        $invoice['setting'] = $setting->toArray();
//        return view('invoice.pdfc', $invoice);
//        pr($invoice);
        $pdf = PDF::loadView('invoice.pdf', $invoice)->setPaper('a4');
        return $pdf->stream();
    }

    public function editInvoiceGet($id){
        $title = "Manage Invoices";
        $invoice = Invoice::with('account', 'status')->where('id', $id)->first();
        $setting = Setting::where('id', 1)->first();
        $invoiceItems = InvoiceItem::with('product')->where('invoice_id', $invoice->id)->get();
        return view('invoice.edit', compact('title', 'invoice', 'setting', 'invoiceItems', 'total_price'));
    }

    public function editInvoicePost($id, Request $request){
        $regRequest = $request->input();


        $delete = InvoiceItem::where('invoice_id', $id)->delete();

        if($delete){
            foreach ($regRequest['product'] as $prdct) {
                InvoiceItem::create([
                    'invoice_id' => $id,
                    'product_id' => $prdct['id'],
                    'price' => $prdct['price'],
                    'quantity' => $prdct['quantity']
                ]);
            }
        }

        $invoice_update = DB::table('invoices')->where('id', $id)->update([
            'note' => $regRequest['note']
        ]);

        if($invoice_update){
            Session::flash('message', 'The invoice has been updated !');
        }else{
            Session::flash('error', 'Invoice Can Not Update !');
        }

        return redirect('invoices')->send();

    }

    public function deleteInvoice(Request $request){
        $delete = Invoice::where('id', $request->id)->delete();
        if ($delete) {
            Session::flash('message', 'The invoice has been deleted !');
        } else {
            Session::flash('error', 'Can not delete the invoice !');
        }
        return redirect('invoices')->send();
    }

    public function addPost(Request $request){
        $this->validate($request, [
            'account_id' => 'required|numeric',
            'product' => 'required|numeric',
            'price' => 'required|numeric',
            'tax_id' => 'numeric'
        ]);

        $invoice_status_id = 2;

        if($request->tax_id){
            $textId = $request->tax_id;
        }else{
            $textId = null;
        }

        $create_invoice = Invoice::create([
            'account_id' => $request->input('account_id'),
            'tax_id' => $textId,
            'note' => $request->input('note'),
            'price' => $request->input('price'),
            'invoice_status_id' => $invoice_status_id,
            'due_date' => $request->input('due_date')
        ]);
        $invoice_id = $create_invoice->id;
        $reference = rand(10482910,89768989).'-'.$invoice_id;
        DB::table('invoices')->where('id', $invoice_id)->update([
            'reference' => $reference
        ]);

        if($invoice_id){
            $invoice_items = InvoiceItem::create([
                'product_id' => $request->input('product'),
                'price' => $request->input('price'),
                'invoice_id' => $invoice_id
            ]);
            if($invoice_items->id){
                Session::flash('message', 'A Invoice has been created !');
            }else{
                Session::flash('error', 'Invoice can not create!');
            }
        }

        return redirect('invoices/')->send();

    }

}
