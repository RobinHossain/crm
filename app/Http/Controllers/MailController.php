<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Invoice;
use App\Mail\OrderShipped;
use App\Todo;
use App\Transaction;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Bogardo\Mailgun\Facades\Mailgun;
use Bogardo\Mailgun\Mail\Message;
use Validator;
//use PDF;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class MailController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clientsBulkMailPost(Request $request){
        $this->validate($request, [
            'subject' => 'required',
            'message' => 'required'
        ]);
        $getAllInput = $request->input();
        $account_groups_list = DB::table('accounts')->select('id', 'fname', 'lname', 'email')->whereIn('group_id',$request->groups)->get();

        $data = array('message' => $request->message);
        $message_body = $request->message;
        $subject = $request->subject;

        $groups = array();
        $group_inc = 0;
        foreach ($account_groups_list as $groupClient){
            $groups[$group_inc]['id'] = $groupClient->id;
            $groups[$group_inc]['name'] = $groupClient->fname.' '.$groupClient->lname;
            $groups[$group_inc]['email'] = $groupClient->email;
            $group_inc++;
        }


        $mailSent = Mailgun::send(['html' => 'emails.bulk'], $data, function(Message $message) use($groups, $subject, $message_body){
            foreach ($groups as $group){
                $message->to($group['email'], $group['name'], [
                    'message' => $message_body
                ])->subject($subject);
            }
        });

        if($mailSent){
            Session::flash('message', 'The mail has been sent for all client !');
        }else{
            Session::flash('error', 'Mail can not sent !');
        }


        return redirect('clients/bulk-email')->send();

    }

    public function sendMail(){
        $data = [
            'customer' => 'John Smith',
            'url' => 'http://laravel.com'
        ];

        $sendMail = Mailgun::send('emails.welcome', $data, function ($message) {
            $message->to('robinsabbir@gmail.com', 'John Smith')->subject('Welcome!');
        });
        if($sendMail){
            
        }
    }

}
