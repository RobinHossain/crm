<?php

namespace App\Http\Controllers;

// For validation
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class AdministratorController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function logout( Request $request ){
	    $this->guard()->logout();
	    $request->session()->flush();
	    $request->session()->regenerate();
	    return redirect('/');
    }

    public function index()
    {
        $title = "Manage Administration";
        $user_list = DB::table('users')->select('id', 'fname', 'lname', 'username', 'email', 'user_role_id')->get();
        $users = array();
        $user_inc = 0;
        foreach($user_list as $user){
            $role = DB::table('user_roles')->select('name')->where('id', $user->user_role_id)->first();
            $users[$user_inc]['id'] = $user->id;
            $users[$user_inc]['fname'] = $user->fname;
            $users[$user_inc]['lname'] = $user->lname;
            $users[$user_inc]['username'] = $user->username;
            $users[$user_inc]['email'] = $user->email;
            if($role){
                $users[$user_inc]['role'] = $role->name;
            }else{
                $users[$user_inc]['role'] = "";
            }
            $user_inc++;
        }

        return view('admin.administrator', compact('title', 'users'));
    }

    public function getRoles()
    {
        $title = "All User Roles with Access Features";
        $roles = DB::table('user_roles')->select('id', 'name', 'slug', 'manage_client', 'manage_order_invoice', 'manage_support', 'manage_finance', 'manage_product', 'manage_admin_option')->get();
        return view('admin.roles', compact('title', 'roles'));
    }

    public function roleAdd()
    {
        $title = "Add a role";
        return view('admin.addrole', compact('title'));
    }

    public function editRole($id = null)
    {
        $title = "Edit an Existing User Role";
        $role = DB::table('user_roles')->select('id', 'name', 'slug', 'manage_client', 'manage_order_invoice', 'manage_support', 'manage_finance', 'manage_product', 'manage_admin_option')->where('id', $id)->first();
        return view('admin.addrole', compact('title', 'role'));
    }

    public function editRolePost($id = null, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $role_update = DB::table('user_roles')->where('id', $id)->update([
            'name' => $request->input('name'),
        ]);

        if($role_update){
            Session::flash('message', 'User Role updated successfully !');
        }else{
            Session::flash('error', 'Role can not update!');
        }

        return redirect('administrator/roles')->send();
    }

    public function deleteAdministrator(Request $request){
        $id  = $request->input('id');
        $delete = User::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'The user has been deleted !');
        }else{
            Session::flash('error', 'The User Can Not Delete !');
        }
        return redirect('administrator')->send();
    }

    public function deleteRoleGet($id)
    {
        $delete = UserRole::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'The user role has been deleted !');
        }else{
            Session::flash('error', 'The User Role Can Not Delete !');
        }
        return redirect('administrator/roles')->send();
    }

    public function roleAddPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $role_create = UserRole::create([
            'name' => $request->input('name'),
        ]);

        if($role_create->id){
            Session::flash('message', 'User Role created successfully !');
        }else{
            Session::flash('error', 'Role can not create!');
        }

        return redirect('administrator/roles')->send();
    }

    public function add(){
        $title = "Add A New User";
        $user_roles = DB::table('user_roles')->pluck('name', 'id');
        return view('admin.add', compact('title', 'user_roles'));
    }

    public function addUser(Request $request){
        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users',
            'group_id' => 'required|numeric',
            'password' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $user_create = User::create([
            'fname' => $request->input('fname'),
            'lname' => $request->input('lname'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'user_role_id' => $request->input('group_id'),
            'password' => Hash::make($request->password),
        ]);

        if($request->photo){
            $randomName = time().rand(33,39);
            $logoName = $randomName.'_'.$request->photo->getClientOriginalName();
            $request->photo->move(public_path('imgs/account'), $logoName);
            $user = DB::table('users')->where('id', $user_create->id)->update([
                'photo' => $logoName
            ]);
        }

        if($user_create->id){
            Session::flash('message', 'User created successfully !');
        }else{
            Session::flash('error', 'Account can not create!');
        }

        return redirect('administrator/')->send();
    }

    public function editUser($id = null){
        $title = "Edit Existing User Info";
        $user = DB::table('users')->select('id', 'fname', 'lname', 'username', 'email', 'user_role_id','photo')->where('id', $id)->first();
        $user_role = DB::table('user_roles')->select('name')->where('id', $user->user_role_id)->first();
        $user_roles = DB::table('user_roles')->pluck('name', 'id');
        return view('admin.edit', compact('title', 'user', 'user_role', 'user_roles'));
    }

    public function editUserPost($id = null, Request $request){

        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'username' => 'required',
            'email' => 'email',
            'password' => 'min:6',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

//        pr($request->input());

        if($request->password ){
            DB::table('users')->where('id', $id)->update([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'username' => $request->username,
                'email' => $request->email,
                'user_role_id' => $request->user_role_id,
                'password' => Hash::make($request->password)
            ]);
        }else{
            DB::table('users')->where('id', $id)->update([
	            'fname' => $request->fname,
	            'lname' => $request->lname,
	            'username' => $request->username,
	            'email' => $request->email,
	            'user_role_id' => $request->user_role_id
            ]);
        }

        if($request->photo){
            $randomName = time().rand(33,39);
            $logoName = $randomName.'_'.$request->photo->getClientOriginalName();
            $request->photo->move(public_path('imgs/account'), $logoName);
            DB::table('users')->where('id', $id)->update([
                'photo' => $logoName
            ]);
        }


	    Session::flash('message', 'Your Changes Updated successfully !');

        return redirect('administrator/')->send();
    }

	protected function guard()
	{
		return Auth::guard();
	}

}
