<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Invoice;
use App\Ticket;
use App\TicketMessage;
use App\Todo;
use App\Transaction;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class JsonController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */




    public function getProductJson(){
        $get_product_list = Product::all('id', 'name', 'price');
        $products = array();
        $product_inc = 0;
        foreach($get_product_list as $prdct){
            $products[$product_inc]['id'] = $prdct->id;
            $products[$product_inc]['name'] = $prdct->name;
        }
        return response()->json($products);
    }

    public function getProductPriceJson($id = null){
        if($id){
            $price = DB::table('products')->where('id', $id)->select('price')->first();
            return response()->json($price);
        }
        return "";
    }

    public function activityGet($id = null){
        $activities = Account::with('type','group','country','role')->where('id', $id)->get();

            $currency = DB::table('settings')->select('default_currency_symbol')->where('id', 1)->first();

            $activity['id'] = $activities[0]->id;
            $activity['name'] = $activities[0]->fname." ".$activities[0]->lname;
            $activity['address'] = $activities[0]->address_1;
            $activity['company'] = $activities[0]->company;
            $activity['email'] = $activities[0]->email;
            $activity['balance'] = $currency->default_currency_symbol . " " . $activities[0]->balance;
            $activity['notification_status'] = $activities[0]->notification_status == 1 ? "On" : "Off";
            $activity['phone'] = $activities[0]->phone;
            $activity['type'] = $activities[0]->type->name;
            $activity['role'] = $activities[0]->role->name;
            $activity['group'] = $activities[0]->group->name;
            $activity['city'] = $activities[0]->city;
            $activity['state'] = $activities[0]->state;
            $activity['zipcode'] = $activities[0]->zipcode;
            $activity['country'] = $activities[0]->country->name;
        return response()->json($activity);
    }

    public function transactionGet($id = null){
        $transaction_list = Transaction::with('type', 'tfor', 'tfrom')->get();
        $transactions = array();
        $trans_inc = 0;
        foreach($transaction_list as $transaction){
            $transactions[$trans_inc]['id'] = $transaction->id;
            $transactions[$trans_inc]['type'] = $transaction->type->name;
            $transactions[$trans_inc]['for'] = $transaction->tfor->fname." ".$transaction->tfor->lname;
            $transactions[$trans_inc]['from'] = $transaction->tfrom->fname." ".$transaction->tfrom->lname;
            $transactions[$trans_inc]['amount'] = $transaction->amount;
            $transactions[$trans_inc]['memo'] = $transaction->memo;
            $transactions[$trans_inc]['date'] = $transaction->date;
            $trans_inc++;
        }
        return response()->json($transactions);
    }

    public function orderGet($id = null){

        $order_list = DB::table('orders')->select('id', 'client_id', 'amount', 'status', 'created_at')->where('client_id', $id)->get();

        $orders = array();
        $order_inc = 0;
        foreach($order_list as $oitem){
            $order_item = DB::table('order_items')->where('order_id', $oitem->id)->count();
            $orders[$order_inc]['id'] = $oitem->id;
            $orders[$order_inc]['amount'] = $oitem->amount;
            $orders[$order_inc]['status'] = $oitem->status;
            $orders[$order_inc]['total_item'] = $order_item;
            $orders[$order_inc]['created'] = $oitem->created_at;
            $order_inc++;
        }

//        pr($orders);
        return response()->json($orders);

    }
    
    public function getTransactionById($id = null){
        $transaction_list = Transaction::where('id', $id)->select('memo', 'date')->first();
        $transaction = $transaction_list->toArray();
        return response()->json($transaction);
    }

    public function invoiceGet($id = null){
        $invoice_list = Invoice::with('account', 'status')->where('account_id', $id)->get();
        $invoices = array();
        $inv_inc = 0;
        foreach($invoice_list as $inv){
            $invoices[$inv_inc]['id'] = $inv->id;
            $invoices[$inv_inc]['amount'] = $inv->price;
            $invoices[$inv_inc]['status'] = $inv->status->name;
            $invoices[$inv_inc]['due_date'] = $inv->due_date;
            $invoices[$inv_inc]['created'] = $inv->created_at->format('M d Y');
        }

        return response()->json($invoices);
    }

    public function TicketGet($id = null){
        $ticketList = Ticket::where('client_id', $id)->get();
        $tickets = array();
        $ticket_inc = 0;
        foreach($ticketList as $ticket){
            $tickets[$ticket_inc]['id'] = $ticket->id;
            $tickets[$ticket_inc]['subject'] = $ticket->subject;
            $getTicketMessage = TicketMessage::where('ticket_id', $ticket->id)->orderBy('updated_at', 'desc')->first();

            $tickets[$ticket_inc]['date'] = $getTicketMessage->date;
            $tickets[$ticket_inc]['message'] = $getTicketMessage->message;

        }

        return response()->json($tickets);
    }


    public function todoGet(){
        $todoList = Todo::all();
        $todos = array();
        $todo_inc = 0;
        foreach($todoList as $todo){
            $todos[$todo_inc]['id'] = $todo->id;
            $todos[$todo_inc]['title'] = $todo->title;
            $todos[$todo_inc]['completed'] = $todo->active == 1? false: true;
            $todo_inc++;
        }
        return response()->json($todos);
    }

    public function todogetName($name = null){
        if($name == 'active'){
            $status = 1;
        }elseif($name == 'completed'){
            $status = 0;
        }
        $todoList = Todo::where('active', $status)->get();
        $todos = array();
        $todo_inc = 0;
        foreach($todoList as $todo){
            $todos[$todo_inc]['id'] = $todo->id;
            $todos[$todo_inc]['title'] = $todo->title;
            $todos[$todo_inc]['completed'] = $todo->active == 1? false: true;
            $todo_inc++;
        }
        return response()->json($todos);
    }

}
