<?php

namespace App\Http\Controllers;

use App\Account;
use App\Invoice;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function where(){
        return view('errors.404');
    }
    public function index()
    {
        $title = "Dashboard";
        $invoices = Invoice::where('invoice_status_id', '!=', 3)->count();
        $accounts = Account::where('account_type_id', 1)->count();
        $totalInvoice = Invoice::select('price')->where('invoice_status_id', '!=', 3)->get();
        $total_sales = 0;
        foreach ($totalInvoice as $tInvoice){
            $total_sales = $total_sales + $tInvoice['price'];
        }

        $getPaidInvoice = Invoice::select('price','created_at')->where('invoice_status_id', 2)->whereMonth('created_at', '>=',date('last month'))->get();
        $revenue_pre = array();
        $rev_inc = 0;
        foreach ($getPaidInvoice as $gPaidIn){
            $revenue_pre[$rev_inc]['date'] = $gPaidIn->created_at->format('Y-m-d');
            $revenue_pre[$rev_inc]['value'] = $gPaidIn->price;
            $rev_inc++;
        }


        $orders = Order::whereMonth('created_at', '>=',date('last month'))->count();

        $revenue = json_encode(generateLastMonthDates($revenue_pre));

//        pr($revenue_pre);

        // Get All last month activity
        // For Orders Only

        $orders_new = DB::select(DB::raw('SELECT DATE(created_at) as date, COUNT( id ) as value FROM orders WHERE created_at BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, \'%Y-%m-%d\') AND DATE_FORMAT(NOW(), \'%Y-%m-%d\') GROUP BY DATE(created_at)'));
        $orders_preArr = array();
        $order_inc = 0;
        foreach ($orders_new as $gOrder){
            $orders_preArr[$order_inc]['date'] = $gOrder->date;
            $orders_preArr[$order_inc]['value'] = $gOrder->value;
            $orders_preArr[$order_inc]['type'] = 'order';
            $order_inc++;
        }

        // For Invoice Only
        $invoice_new = DB::select(DB::raw('SELECT DATE(created_at) as date, COUNT( id ) as value FROM invoices WHERE created_at BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, \'%Y-%m-%d\') AND DATE_FORMAT(NOW(), \'%Y-%m-%d\') GROUP BY DATE(created_at)'));
        $invoice_preArr = array();
        $invoice_inc = 0;
        foreach ($invoice_new as $ginvoice){
            $invoice_preArr[$invoice_inc]['date'] = $ginvoice->date;
            $invoice_preArr[$invoice_inc]['value'] = $ginvoice->value;
            $invoice_preArr[$invoice_inc]['type'] = 'invoice';
            $invoice_inc++;
        }

        // For Clients Only
        $clients_new = DB::select(DB::raw('SELECT DATE(created_at) as date, COUNT( id ) as value FROM accounts WHERE created_at BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, \'%Y-%m-%d\') AND DATE_FORMAT(NOW(), \'%Y-%m-%d\') GROUP BY DATE(created_at)'));
        $clients_preArr = array();
        $client_inc = 0;
        foreach ($clients_new as $gClient){
            $clients_preArr[$client_inc]['date'] = $gClient->date;
            $clients_preArr[$client_inc]['value'] = $gClient->value;
            $clients_preArr[$client_inc]['type'] = 'client';
            $client_inc++;
        }

        // For Sales Only
        $sales_new = DB::select(DB::raw('SELECT DATE(created_at) as date, SUM( price ) as value FROM invoices WHERE created_at BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, \'%Y-%m-%d\') AND DATE_FORMAT(NOW(), \'%Y-%m-%d\') GROUP BY DATE(created_at)'));
        $sales_newArr = array();
        $client_inc = 0;
        foreach ($sales_new as $gSales){
            $sales_newArr[$client_inc]['date'] = $gSales->date;
            $sales_newArr[$client_inc]['value'] = $gSales->value;
            $sales_newArr[$client_inc]['type'] = 'sale';
            $client_inc++;
        }

        $ordersInvoiceArr = array_merge($orders_preArr,$invoice_preArr);
        $ordersInvoiceClientArr = array_merge($ordersInvoiceArr,$clients_preArr);
        $ordersInvoiceClientSalesArr = array_merge($ordersInvoiceClientArr,$sales_newArr);

        $lastMonthAllUpdate = json_encode(generateLastMonthDateDatas($ordersInvoiceClientSalesArr));

//        pr(generateLastMonthDateDatas($ordersInvoiceClientSalesArr));
        // Get All last month activity


        return view('home.index', compact('invoices','title','total_sales','accounts','revenue','orders', 'lastMonthAllUpdate'));
    }
}
