<?php

namespace App\Http\Controllers;

// For validation
use App\ProductCategory;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class ProductController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $title = "Product/Service List";
        $products = Product::with('category')->select('id', 'name', 'details', 'product_category_id', 'price', 'status', 'quantity')->get();
        return view('product.index', compact('title', 'products'));
    }

    public function getProductCategories(){
        $title = "Product/Service Category";
        $categories = ProductCategory::get();
        return view('product.categories', compact('title', 'categories'));
    }

    public function add()
    {
        $title = "Add A New Product/Service";
        $categories = ProductCategory::pluck('name', 'id');//DB::table('countries')->pluck('name', 'id');
        return view('product.add', compact('title', 'categories'));
    }

    public function getAddCategory(){
        $title = "Add Product/Service Category";
        return view('product/addCategory', compact('title'));
    }

    public function getEditCategory($id){
        $title = "Edit Category";
        $category = ProductCategory::where('id', $id)->first();
        return view('product/addCategory', compact('title', 'category'));
    }

    public function postEditCategory( $id, Request $request ){
        $this->validate($request, [
            'name' => 'required',
        ]);

        $slug = preg_replace('/[ ,]+/', '-', trim($request->name));

        $category = DB::table('product_categories')->where('id', $id)->update([
            'name' => $request->name,
            'slug' => $request-> $slug
        ]);

        if($category){
            Session::flash('message', 'The category has been udpated !');
        } else {
            Session::flash('error', 'Can not update the category !');
        }

        return redirect('product/categories')->send();

    }

    public function postAddCategory(Request $request){
        $this->validate($request, [
            'name' => 'required',
        ]);

        $slug = preg_replace('/[ ,]+/', '-', trim($request->name));

        $category = ProductCategory::create([
            'name' => $request->name,
            'slug' => $slug
            ]);

        if ($category) {
            Session::flash('message', 'The category has been added !');
        } else {
            Session::flash('error', 'Can not add the category !');
        }

        return redirect('product/categories')->send();
    }

    public function postDeleteCategory(Request $request){
        $id = $request->id;
        if($id){
            $delete_category = ProductCategory::where('id', $id)->delete();
            if($delete_category){
                Session::flash('message', 'The Product Category has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete the product category!');
            }
        }
        return redirect('product/categories/')->send();
    }

    public function addProduct(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'product_category_id' => 'required|numeric',
            'details' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric'
        ]);

//        pr($request->all());


        $productVariation = json_encode($request->product_variation);
        if( $request->status && $request->status == 'on' ){ $status = 1; }else{ $status = 0; }
        if( $request->show_client && $request->show_client == 'on' ){ $show_client = 1; }else{ $show_client = 0; }

//        pr($productVariation);

        $product = Product::create([
            'name' => $request->name,
            'details' => $request->details,
            'variation' => $productVariation,
            'price' => $request->price,
            'product_category_id' => $request->product_category_id,
            'status' => $status,
            'quantity' => $request->quantity,
            'show_client' => $show_client,
        ]);

        if ($product) {
            Session::flash('message', 'The product has been added !');
        } else {
            Session::flash('error', 'Can not add the product !');
        }

        return redirect('products/')->send();

    }

    public function deleteProduct(Request $request)
    {
        $delete = Product::where('id', $request->id)->delete();
            if ($delete) {
                Session::flash('message', 'The product has been deleted !');
            } else {
                Session::flash('error', 'Can not delete the product !');
            }

        return redirect('products/')->send();
    }

    public function editProduct($id = null)
    {
        $title = "Product List";
        $product = DB::table('products')->select('id', 'name', 'details', 'variation', 'price', 'status', 'show_client', 'quantity', 'product_category_id')->where('id', $id)->first();

        $decodedVariation = json_decode($product->variation);


        if(count($decodedVariation)>0){
            $productVariation = array();
            $pvCount = 1;
            foreach ($decodedVariation as $variation){
                $productVariation[$pvCount]['name'] = $variation->name;
                if( isset($variation->price) && !empty($variation->price) ){
                    $productVariation[$pvCount]['price'] = $variation->price;
                }else{
                    $productVariation[$pvCount]['price'] = 0;
                }

                $pvCount++;
            }
            $product->variation  = $productVariation;
        }



//        pr($product);


        $categories = ProductCategory::pluck('name', 'id');
        return view('product.add', compact('title', 'product', 'categories'));
    }

    public function editProductPost($id = null, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'product_category_id' => 'required|numeric'
        ]);

        $productVariation = json_encode($request->product_variation);


        $status = 0;
        $show_client = 0;
        if ($request->input('status') == 'on') {
            $status = 1;
        }

        if ($request->input('show_client') == 'on') {
            $show_client = 1;
        }


        $clients = DB::table('products')->where('id', $id)->update([
            'name' => $request->input('name'),
            'details' => $request->input('details'),
            'variation' => $productVariation,
            'price' => $request->input('price'),
            'product_category_id' => $request->product_category_id,
            'status' => $status,
            'quantity' => $request->input('quantity'),
            'show_client' => $show_client,
        ]);

        if ($clients) {
            Session::flash('message', 'The product has been added !');
        } else {
            Session::flash('error', 'Can add the product !');
        }

        return redirect('products/')->send();

    }
}
