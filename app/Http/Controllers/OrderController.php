<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\EmailTemplate;
use App\Invoice;
use App\InvoiceItem;
use App\Setting;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Nexmo\Laravel\Facade\Nexmo;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use Bogardo\Mailgun\Facades\Mailgun;

use App\Order;
use App\OrderItem;
use App\Coun;


class OrderController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function add()
    {
        $title = "Add New Order";
        $customer_list = DB::table('accounts')->select('id', 'fname', 'lname')->where('account_type_id', '1')->get();
        $product_list = DB::table('products')->select('id', 'name', 'quantity')->where('status', 1)->where('show_client', 1)->where('quantity', '>', 0)->get();

        $customers = array();
        foreach ($customer_list as $customer) {
            $customers[$customer->id] = $customer->fname . " " . $customer->lname;
        }

        $productss = array();
        foreach ($product_list as $product) {
            $productss[$product->id] = $product->name . " (" . $product->quantity . ")";
        }

        $pselect = array('value="" disabled selected' => 'Please Select Products');
        $products = $pselect + $productss;


        return view('order.add', compact('title', 'customers', 'products'));
    }

    public function addConfirm(Request $request)
    {
        $getRequest = $request->input();
        $forCustomer = $getRequest['customer'];
        $getProducts = $getRequest['products'];
        $customer_info = DB::table('accounts')->select('id', 'fname', 'lname', 'phone', 'city', 'address_1')->where('id', $forCustomer)->first();
        $title = "Confirm Order For " . $customer_info->fname . " " . $customer_info->lname;
        $product_list = DB::table('products')->select('id', 'name', 'price')->whereIn('id', $getProducts)->get();

        return view('order.add_confirm', compact('title', 'customer_info', 'product_list'));
    }

    public function addConfirmed(Request $request)
    {
//        pr($request->input());
        $regRequest = $request->input();

        $this->validate($request, [
            'product_id' => 'required|numeric',
            'customer_id' => 'required|numeric',
        ]);

        $created_order = Order::create([
            'client_id' => $regRequest['customer_id'],
            'amount' => $regRequest['add_order_total_price'],
            'status' => 'pending',
        ]);



        $dueDate = $created_order->created_at;
        $createdOrderId = $created_order->id;

        if ($created_order->id) {

            $create_invoice = Invoice::create([
                'account_id' => $regRequest['customer_id'],
                'price' => $regRequest['add_order_total_price'],
                'invoice_status_id' => 1,
                'due_date' => $dueDate
            ]);


            $invoicId = $create_invoice->id;

            if ($invoicId) {

                foreach ($regRequest['product'] as $prdct) {
                    OrderItem::create([
                        'order_id' => $createdOrderId,
                        'product_id' => $prdct['id'],
                        'quantity' => $prdct['quantity'],
                    ]);

                    InvoiceItem::create([
                        'invoice_id' => $invoicId,
                        'product_id' => $prdct['id'],
                        'price' => $prdct['price'],
                        'quantity' => $prdct['quantity']
                    ]);

                }

            }

            if($invoicId && $request->send_invoice ==  'on'){

                // Generate Invoice - Start



                $getInvoice = Invoice::with('account', 'status')->where('id', $invoicId)->first();

                $invoice = $getInvoice->toArray();

                $getInvoiceItems = InvoiceItem::with('product')->where('invoice_id', $getInvoice->id)->get();
                $invoice['invoiceItem'] = $getInvoiceItems->toArray();

                $setting = Setting::where('id', 1)->first();
                $invoice['setting'] = $setting->toArray();


                $pdf = PDF::loadView('invoice.pdf', $invoice)->setPaper('a4')->save( 'pdf/invoice.pdf' );

                if($pdf){

                    $customerId = $request->customer_id;

                    $customerInfo = Account::where('id', $customerId)->select('fname','lname','email', 'phone')->first();

                    $getTemplate = EmailTemplate::where('id',5)->first();
                    $getSettings = Setting::where('id',1)->first();

                    $templateBody = $getTemplate->body;
                    $subject_pre = $getTemplate->subject;

                    // Generate Variable arrays
                    $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
                    $sysUrl = URL::to('/');

                    $arrToExtract = array();
                    $arrToExtract['name'] = $customerInfo->fname.' '.$customerInfo->lname;
                    $arrToExtract['email'] = $customerInfo->email;
                    $arrToExtract['phone'] = $customerInfo->phone;

                    $arrToExtract['business_name'] = $getSettings->company_name;
                    $arrToExtract['logo'] = $getLogoUrl;
                    $arrToExtract['sys_url'] = $sysUrl;

                    $arrToExtract['order_date'] = $dueDate;
                    $arrToExtract['order_num'] = $createdOrderId;
                    $arrToExtract['invoice_id'] = $invoicId;
                    // Generate Variable arrays


                    $body = replaceWithEtemplateVar($templateBody,$arrToExtract);
                    $subject = replaceWithEtemplateVar($subject_pre, $arrToExtract);

                    $mailUse = array('to' => $arrToExtract['email'], 'name' => $arrToExtract['name'], 'subject' => $subject, 'phone' => $arrToExtract['phone']);
                    $data = array('body' => $body);

                    Mailgun::send(['html' => 'emails.orderconfirm'], $data, function ($message) use($mailUse) {
                        $message
                            ->to($mailUse['to'], $mailUse['name'])
                            ->subject($mailUse['subject'])
                            ->attach('pdf/invoice.pdf');
                    });

//                    Nexmo::message()->send([
//                        'to' => $arrToExtract['phone'],
//                        'from' => '16105552344',
//                        'text' => 'An Invoice Has been created for you!'
//                    ]);


                }

            }



                Session::flash('message', 'A order has been created !');
                return redirect('invoice/manage/' . $invoicId)->send();


//            Session::flash('message', 'A order has been created !');
        } else {
            Session::flash('error', 'Data can not save!');
        }

        return redirect('order/')->send();


    }

    public function manageOrder($id = null)
    {
        $order = DB::table('orders')->select('id', 'client_id', 'amount', 'note', 'status', 'created_at')->where('id', $id)->first();
        $customer = DB::table('accounts')->select('fname', 'lname', 'email', 'phone', 'address_1')->where('id', $order->client_id)->first();
        $order_items = DB::table('order_items')->select('product_id', 'quantity')->where('order_id', $id)->get();

        $products = array();
        $product_inc = 0;
//        pr($order_items);
        foreach ($order_items as $oitems) {
            $product_info = DB::table('products')->select('name', 'price')->where('id', $oitems->product_id)->first();
            $products[$product_inc]['id'] = $oitems->product_id;
            if ($product_info && count($product_info) > 0) {
                $products[$product_inc]['name'] = $product_info->name;
                $products[$product_inc]['price'] = $product_info->price;
            }
            $products[$product_inc]['quantity'] = $oitems->quantity;
            $product_inc++;
        }
        $title = "Manage Order #" . $id;

        return view('order.ordermanage', compact('title', 'order', 'customer', 'products'));
    }

    public function manageOrderStatus(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|numeric'
        ]);
        $order_id = $request->input('order_id');
        $status = $request->input('status');
        if ($status !== 'delete') {
            $clients = DB::table('orders')->where('id', $order_id)->update([
                'status' => $status
            ]);
        } elseif ($status == 'delete') {
            $clients = Order::where('id', $order_id)->delete();
        }

        if ($status == 'active') {
            $status_var = "Assigned as Accepted";
        } elseif ($status == 'cancel') {
            $status_var = "Assigned as Canceled";
        } elseif ($status == 'pending') {
            $status_var = "Assigned as Pending";
        } elseif ($status == 'fraud') {
            $status_var = "Assigned as Fraud";
        } elseif ($status == 'delete') {
            $status_var = "has been Deleted";
        }
        if ($clients) {
            Session::flash('message', 'Set Order #' . $order_id . ' ' . $status_var);
        } else {
            Session::flash('error', 'Can not change the order!');
            return redirect('order')->send();
        }
        if ($status !== 'delete') {
            return redirect('order/manage/' . $order_id)->send();
        } else {
            return redirect('order')->send();
        }
    }

    public function manageOrderNote(Request $request)
    {
        $order_id = $request->input('order_id');
        $note = $request->input('note');
        $order_update = DB::table('orders')->where('id', $order_id)->update([
            'note' => $note
        ]);
        if ($order_update) {
            Session::flash('message', 'Order Note Save Successfully for #' . $order_id);
        } else {
            Session::flash('error', 'Can not change the Current Order!');
        }
        return redirect('order/manage/' . $order_id)->send();
    }

    public function manage($status = null)
    {
        $title = "Manage Orders";
        if ($status) {
            if ($status == 'pending' || $status == 'active' || $status == 'fraud' || $status == 'cancel') {
                $order_items = DB::table('orders')->select('id', 'client_id', 'amount', 'status', 'created_at')->where('status', $status)->get();
            } else {
                Session::flash('message', 'You are not looking order in right way');
                return redirect('order/')->send();
            }
        } else {
            $order_items = DB::table('orders')->select('id', 'client_id', 'amount', 'status', 'created_at')->get();
        }


        $orders = array();
        $order_inc = 0;
        foreach ($order_items as $oitem) {
            $customer_info = DB::table('accounts')->select('fname', 'lname', 'email')->where('id', $oitem->client_id)->first();
            $orders[$order_inc]['id'] = $oitem->id;
            if(isset($customer_info->fname) && isset($customer_info->lname)){
                $orders[$order_inc]['customer_name'] = $customer_info->fname." ".$customer_info->lname;
            }else{
                $orders[$order_inc]['customer_name'] = "";
            }
            $orders[$order_inc]['amount'] = $oitem->amount;
            $orders[$order_inc]['status'] = $oitem->status;
            $orders[$order_inc]['created'] = $oitem->created_at;
            $order_inc++;
        }

	    $user = Auth::user('id', 'user_role_id');
	    $userRole = UserRole::where('id', $user->user_role_id)->first();

        return view('order.manage', compact('title', 'orders', 'userRole'));
    }
}
