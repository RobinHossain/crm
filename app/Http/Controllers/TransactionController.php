<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\User;
use App\UserRole;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;
use App\Transaction;


class TransactionController extends Controller
{
    use FormAccessible;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $title = "All transactions";
        $transactions = Transaction::with('type', 'tfor', 'tfrom')->get();
//        pr($transactions);
//        $transactions = DB::table('transactions')->select('id','type_id','for','from','amount','memo','date')->get();
        return view('transaction.index', compact('title', 'transactions'));
    }

    public function income(){
        $title = "All Income Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 1)->get();
        return view('transaction.income', compact('title', 'transactions'));
    }

    public function incomeAdd(){
        $title = "Add a new Income Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 1)->get();
        $get_account_list = Account::all('id', 'fname', 'lname');

        $accounts = array();
        foreach($get_account_list as $acl){
            $accounts[$acl->id] = $acl->fname." ".$acl->lname;
        }

        return view('transaction.incomeAdd', compact('title', 'transactions', 'accounts'));
    }

    public function incomeAddPost(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'amount' => 'required|numeric',
            'for' => 'required|numeric',
            'from' => 'required|numeric',
        ]);

        $forAccount =  $request->input('for');
        $fromAccount =  $request->input('from');

        if($forAccount == $fromAccount){
            return Redirect::back()->with('message','Transaction can not for same account !');
        }else{
//            $tAmount = $request->input('amount');
//            $getFromBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $fromAccount)->first();
//            $getForBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $forAccount)->first();
//            if($tAmount >= $getFromBalance->balance){
//                return Redirect::back()->with('message','Insufficient Balance to transfer from '.$getFromBalance->fname.' '.$getFromBalance->lname.'\'s Account!');
//            }else{
//                $finalFromBalance = ($getFromBalance->balance - $tAmount);
//                $edit_transaction = DB::table('accounts')->where('id', $fromAccount)->update([
//                    'balance' => $finalFromBalance
//                ]);
//                $finalForBalance = $getForBalance->balance + $tAmount ;
//                $edit_transaction = DB::table('accounts')->where('id', $forAccount)->update([
//                    'balance' => $finalForBalance
//                ]);
//                if($finalFromBalance && $finalForBalance){
//                    Session::flash('message', 'Balance Transferred to '.$getForBalance->fname.' '.$getForBalance->lname.'\'s Account!');
//                }
//            }

        }

        $t_type = 1;

        $transaction = Transaction::create([
            'date' => $request->input('date'),
            'amount' => $request->input('amount'),
            'for' => $request->input('for'),
            'from' => $request->input('from'),
            'memo' => $request->input('memo'),
            'transaction_type_id' => $t_type,
        ]);

        if($transaction->id){
            Session::flash('message', 'A transaction has been added !');
        }else{
            Session::flash('error', 'Transaction can not add!');
        }

        return redirect('transactions/')->send();
    }

    public function editPost(Request $request ){
        $id = $request->id;
        $date = $request->input('date');
        $memo = $request->input('memo');
        if($date || $memo){
            $edit_transaction = DB::table('transactions')->where('id', $id)->update([
                'date' => $date,
                'memo' => $memo
            ]);
            if($edit_transaction){
                Session::flash('message', 'The Transaction Updated !');
            }else{
                Session::flash('error', 'Can not update the transaction!');
            }
            return redirect('transactions/')->send();
        }
    }

    public function deletePost(Request $request, $id = null){



        if($id == null){
            $id = $request->id;
        }
            $delete_transaction = Transaction::where('id', $id)->delete();
            if($delete_transaction){
                Session::flash('message', 'The Transaction has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete the transaction!');
            }
        return redirect('transactions/')->send();
    }

    public function expense(){
        $title = "All Expense Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 2)->get();
        return view('transaction.expense', compact('title', 'transactions'));
    }

    public function expenseAdd(){
        $title = "Add a new expense Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 2)->get();
        $get_account_list = Account::all('id', 'fname', 'lname');
        $accounts = array();
        foreach($get_account_list as $acl){
            $accounts[$acl->id] = $acl->fname." ".$acl->lname;
        }
        return view('transaction.expenseAdd', compact('title', 'transactions', 'accounts'));
    }

    public function expenseAddPost(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'amount' => 'required|numeric',
            'for' => 'required|numeric',
            'from' => 'required|numeric',
        ]);

        $forAccount =  $request->input('for');
        $fromAccount =  $request->input('from');

        if($forAccount == $fromAccount){
            return Redirect::back()->with('message','Transaction can not for same account !');
        }else{
//            $tAmount = $request->input('amount');
//            $getFromBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $fromAccount)->first();
//            $getForBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $forAccount)->first();
//            if($tAmount >= $getFromBalance->balance){
//                return Redirect::back()->with('error','Insufficient Balance to transfer from '.$getFromBalance->fname.' '.$getFromBalance->lname.'\'s Account!');
//            }else{
//                $finalFromBalance = ($getFromBalance->balance - $tAmount);
//                $edit_transaction = DB::table('accounts')->where('id', $fromAccount)->update([
//                    'balance' => $finalFromBalance
//                ]);
//                $finalForBalance = $getForBalance->balance + $tAmount ;
//                $edit_transaction = DB::table('accounts')->where('id', $forAccount)->update([
//                    'balance' => $finalForBalance
//                ]);
//                if($finalFromBalance && $finalForBalance){
//                    Session::flash('message', 'Balance Transferred to '.$getForBalance->fname.' '.$getForBalance->lname.'\'s Account!');
//                }
//            }

        }

        $t_type = 2;

        $transaction = Transaction::create([
            'date' => $request->input('date'),
            'amount' => $request->input('amount'),
            'for' => $request->input('for'),
            'from' => $request->input('from'),
            'memo' => $request->input('memo'),
            'transaction_type_id' => $t_type,
        ]);

        if($transaction->id){
            Session::flash('message', 'A transaction has been added !');
        }else{
            Session::flash('error', 'Transaction can not add!');
        }

        return redirect('transactions/')->send();
    }

    public function transfer(){
        $title = "All Transfer Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 3)->get();
        return view('transaction.transfer', compact('title', 'transactions'));
    }

    public function transferAdd(){
        $title = "Add a new transfer Entry";
        $transactions = Transaction::with('tfor', 'tfrom')->where('transaction_type_id', 3)->get();
        $get_account_list = Account::all('id', 'fname', 'lname');
        $accounts = array();
        foreach($get_account_list as $acl){
            $accounts[$acl->id] = $acl->fname." ".$acl->lname;
        }
        return view('transaction.transferAdd', compact('title', 'transactions', 'accounts'));
    }

    public function transferAddPost(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'amount' => 'required|numeric',
            'for' => 'required|numeric',
            'from' => 'required|numeric',
        ]);

        $forAccount =  $request->input('for');
        $fromAccount =  $request->input('from');

        if($forAccount == $fromAccount){
            return Redirect::back()->with('message','Transaction can not for same account !');
        }else{
//            $tAmount = $request->input('amount');
//            $getFromBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $fromAccount)->first();
//            $getForBalance = DB::table('accounts')->select('balance','fname','lname')->where('id', $forAccount)->first();
//            if($tAmount >= $getFromBalance->balance){
//                return Redirect::back()->with('error','Insufficient Balance to transfer from '.$getFromBalance->fname.' '.$getFromBalance->lname.'\'s Account!');
//            }else{
//                $finalFromBalance = ($getFromBalance->balance - $tAmount);
//                $edit_transaction = DB::table('accounts')->where('id', $fromAccount)->update([
//                    'balance' => $finalFromBalance
//                ]);
//                $finalForBalance = $getForBalance->balance + $tAmount ;
//                $edit_transaction = DB::table('accounts')->where('id', $forAccount)->update([
//                    'balance' => $finalForBalance
//                ]);
//                if($finalFromBalance && $finalForBalance){
//                    Session::flash('message', 'Balance Transferred to '.$getForBalance->fname.' '.$getForBalance->lname.'\'s Account!');
//                }
//            }

        }

        $t_type = 3;

        $transaction = Transaction::create([
            'date' => $request->input('date'),
            'amount' => $request->input('amount'),
            'for' => $request->input('for'),
            'from' => $request->input('from'),
            'memo' => $request->input('memo'),
            'transaction_type_id' => $t_type,
        ]);

        if($transaction->id){
            Session::flash('message', 'A transaction has been added !');
        }else{
            Session::flash('error', 'Transaction can not add!');
        }

        return redirect('transactions/')->send();
    }




}
