<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Invoice;
use App\Notice;
use App\Todo;
use App\Transaction;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class ApplicationController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    

    public function todoGet()
    {
        $title = "Todo Application";
        $todos = DB::table('todo')->select('id', 'title')->get();
        return view('apps.todo', compact('title', 'todos'));
    }

    public function stickyNoteGet()
    {
        $title = "Sticky Note Application";
        $todos = DB::table('todo')->select('id', 'title')->get();
        return view('apps.sticky', compact('title', 'todos'));
    }

    public function noticeAddPost(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'notice' => 'required'
        ]);

        if($request->input('client_id') && is_numeric($request->input('client_id'))){
            $client_id = $request->input('client_id');
        }else{
            $client_id = null;
        }

        $status = 0;
        if ($request->input('show_client')) {
            if($request->input('show_client') == 'on'){
                $status = 1;
            }elseif($request->input('show_client') == 'off'){
                $status = 0;
            }else{
                $status = $request->input('show_client');
            }
        }
        $create = Notice::create([
            'notice' => $request->input('notice'),
            'title' => $request->input('title'),
            'client_id' => $client_id,
            'status' => $status
        ]);

        if ($create) {
            Session::flash('message', 'The Notice has been added !');
        } else {
            Session::flash('error', 'Can not add notice!');
        }

        return redirect('apps/noticeboard')->send();
    }

    public function noticeBoardGet(){
        $title = "Notice Board Application";
        $notices = Notice::select('id','notice','title','status','updated_at')->get();
        $clients = Account::select(DB::raw("CONCAT(fname,' ',lname) AS name"),'id')->pluck('name', 'id');
//        pr($clients);
        return view('apps.noticeboard', compact('title','notices','clients'));
    }

    public function noticeGetById($id){
        $notice = Notice::select('id','notice','title','status','updated_at')->where('id', $id)->first();
        return response()->json($notice);
    }

    public function noticeDeleteById($id){
        $delete = Notice::where('id', $id)->delete();
        if ($delete) {
            return response()->json('deleted');
        } else {
            return response()->json(0);
        }
    }
    

    public function todoDeleteGet($id = null)
    {
        $delete_todo = Todo::where('id', $id)->delete();
        if ($delete_todo) {
            return response()->json(1);
        } else {
            return response()->json(0);
        }
    }

    public function editNoticePost(Request $request){
        $id = $request->input('id');
        $status = 1;
        if ($request->input('status') && $request->input('status') == true) {
            $status = 0;
        } else {
            $status = 1;
        }
        $updateTodo = DB::table('notices')->where('id', $id)->update([
            'title' => $request->input('title'),
            'notice' => $request->input('notice'),
            'status' => $status
        ]);
        if ($updateTodo) {
            return redirect('apps/noticeboard')->send();
        }
    }

    public function todoStatusUpdatePost($id = null, Request $request)
    {
        if ($request->input('completed') && $request->input('completed') == true) {
            $status = 0;
        } else {
            $status = 1;
        }
        $updateTodo = DB::table('todo')->where('id', $id)->update([
            'active' => $status
        ]);
        if ($updateTodo) {
            return response()->json(1);
        }
    }

    public function todoUpdatePost($id = null, Request $request)
    {
        $updateTodo = DB::table('todo')->where('id', $id)->update([
            'title' => $request->input('title')
        ]);
        if ($updateTodo) {
            return response()->json(1);
        }
    }

    public function todoCreatePost(Request $request)
    {
        $status = 1;
        $article = Todo::create([
            'title' => $request->input('title'),
            'active' => $status,
        ]);
        if ($article) {
            return response()->json($article->id);
        }
    }

}
