<?php

namespace App\Http\Controllers;

// For validation
use App\Invoice;
use App\User;
use App\UserRole;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class AccountController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */

    public function activityAll($id = null)
    {
        $title = "Accounts Activity";
        $id = $id;
        $activity = DB::table('accounts')->where('id', $id)->select('id', 'fname', 'lname', 'company', 'email', 'notification_status', 'phone', 'account_type_id', 'role_id', 'group_id', 'address_1', 'address_2', 'city', 'state', 'zipcode', 'country_id')->first();
//        pr($activity);
        return view('account.activity', compact('title', 'id'));
    }

    public function activity($id = null)
    {
        $title = "Accounts Activity";
        $activity = DB::table('accounts')->where('id', $id)->select('id', 'fname', 'lname', 'company', 'email', 'notification_status', 'phone', 'account_type_id', 'role_id', 'group_id', 'address_1', 'address_2', 'city', 'state', 'zipcode', 'country_id')->first();
        return response()->json($activity);
    }

    public function balanceSheet()
    {
        $title = "Account Balance Sheet";
        $accounts = DB::table('accounts')->select('id', 'fname', 'lname', 'balance')->where('balance', '>', 0)->get();
        return view('account.balance', compact('accounts', 'title'));
    }

    public function revenue()
    {

        $title = "Total Income From Last Month to Current Month";

        $getPaidInvoice = Invoice::select('price', 'created_at')->where('invoice_status_id', 2)->whereMonth('created_at', '>=', date('last month'))->get();
        $revenue_pre = array();
        $rev_inc = 0;
        foreach ($getPaidInvoice as $gPaidIn) {
            $revenue_pre[$rev_inc]['date'] = $gPaidIn->created_at->format('Y-m-d');
            $revenue_pre[$rev_inc]['value'] = $gPaidIn->price;
            $rev_inc++;
        }


//        $orders = Order::whereMonth('created_at', '>=',date('last month'))->count();

        $revenue = json_encode(generateLastMonthDates($revenue_pre));

//        pr($revenue);


//        $revenues = json_encode($revenue);
//        pr($revenue);
        return view('account.revenue', compact('revenue', 'title'));
    }

}
