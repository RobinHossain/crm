<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\User;
use App\UserRole;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;
use App\Payment;


class PaymentController extends Controller
{
    use FormAccessible;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $title = "All Payment Settings";
        $payments = Payment::all();
        return view('payment.index', compact('title', 'payments'));
    }

    public function addGet()
    {
        $title = "Add a new payment method";
        return view('payment.add', compact('title'));
    }


    public function manageGet($id = null)
    {
        $title = "Edit Existing Payment Method";
        $payment = DB::table('payments')->select('id', 'name', 'setting', 'value', 'status')->where('id', $id)->first();
        return view('payment.manage', compact('title','payment'));
    }

    public function managePost($id = null, Request $request){
        $this->validate($request, [
            'name' => 'required',
            'setting' => 'required',
            'value' => 'required',
        ]);

        $status = 0;
        if ($request->input('status')) {
            if($request->input('status') == 'on'){
                $status = 1;
            }elseif($request->input('status') == 'off'){
                $status = 0;
            }else{
                $status = $request->input('status');
            }
        }

        $payment = DB::table('payments')->where('id', $id)->update([
            'name' => $request->input('name'),
            'setting' => $request->input('setting'),
            'value' => $request->input('value'),
            'status' => $status
        ]);

        if ($payment) {
            Session::flash('message', 'The payment method has been updated !');
        } else {
            Session::flash('error', 'Can not update the payment method !');
        }

        return redirect('payments/')->send();
    }

    public function deleteGet($id = null)
    {
        if ($id) {
            $delete = Payment::where('id', $id)->delete();
            if ($delete) {
                Session::flash('message', 'The payment method has been deleted !');
            } else {
                Session::flash('error', 'Can not delete the payment method !');
            }
        }

        return redirect('payments/')->send();
    }

    public function addPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'setting' => 'required',
            'value' => 'required',
            'status' => 'required|numeric'
        ]);

        $status = 0;
        if ($request->input('status')) {
            $status = $request->input('status');
        }

        $payment = Payment::create([
            'name' => $request->input('name'),
            'setting' => $request->input('setting'),
            'value' => $request->input('value'),
            'status' => $status,
        ]);

        if ($payment) {
            Session::flash('message', 'The payment method has been added !');
        } else {
            Session::flash('error', 'Can not add a new payment !');
        }

        return redirect('payments/')->send();
    }


}
