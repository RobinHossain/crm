<?php

namespace App\Http\Controllers;

// For validation
use App\EmailTemplate;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class EmailTemplateController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */




    public function index(){
        $title = "Email Templates";
        $templates = EmailTemplate::all();
        return view('email_template.index', compact('title', 'templates'));
    }

    public function manageGet($id = null){
        $title = "Edit Existing Template";
        $template = DB::table('etemplates')->select('id', 'name', 'subject', 'body', 'status')->where('id', $id)->first();
        return view('email_template.manage', compact('title', 'template'));
    }

    public function managePost($id = null, Request $request){
        $this->validate($request, [
            'name' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ]);

        $status = 0;
        if ($request->input('status')) {
            if($request->input('status') == 'on'){
                $status = 1;
            }elseif($request->input('status') == 'off'){
                $status = 0;
            }else{
                $status = $request->input('status');
            }
        }

        $payment = DB::table('etemplates')->where('id', $id)->update([
            'name' => $request->input('name'),
            'subject' => $request->input('subject'),
            'body' => $request->input('body'),
            'status' => $status
        ]);

        if ($payment) {
            Session::flash('message', 'The Email Template has been updated !');
        } else {
            Session::flash('error', 'Can not update the email template !');
        }

        return redirect('email-templates/')->send();
    }

}
