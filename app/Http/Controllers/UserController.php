<?php

namespace App\Http\Controllers;
// For validation
use App\EmailTemplate;
use App\Invoice;
use App\InvoiceItem;
use App\Order;
use App\OrderItem;
use App\Recurring;
use App\Setting;
use App\Ticket;
use App\TicketMessage;
use App\Transaction;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Validator;
use Bogardo\Mailgun\Facades\Mailgun;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Account;
use App\AccountGroup;
use App\Coun;


class UserController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(){
        pr('yes');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
    	$user = Auth::user('id', 'user_role_id');
    	$userRole = UserRole::where('id', $user->user_role_id)->first();
    	if( $userRole->manage_client !=1 ){
		    return redirect('/')->send();
	    }

        $title = "Manage Clients";
        $clients = DB::table('accounts')->select('fname', 'lname', 'created_at', 'balance', 'id')->where('account_type_id', 1)->get();
        return view('client.manage', compact('title', 'clients'));
    }

    public function allAccounts()
    {
        $title = "Manage All Account";
        $account_list = DB::table('accounts')->select('fname', 'lname', 'balance', 'created_at', 'account_type_id', 'id')->get();
        $accounts = array();
        $acc_inc = 0;
        foreach($account_list as $acclist){
            $account_type = DB::table('account_types')->select('id', 'name')->where('id', $acclist->account_type_id)->first();
            $accounts[$acc_inc]['id'] = $acclist->id;
            $accounts[$acc_inc]['name'] = $acclist->fname ." ". $acclist->lname;
            $accounts[$acc_inc]['balance'] = $acclist->balance;
            $accounts[$acc_inc]['created_at'] = $acclist->created_at;
            $accounts[$acc_inc]['account_type'] = $account_type->name;
            $acc_inc++;
        }
        return view('client.allaccount', compact('title', 'accounts'));
    }

    public function addAccountGet()
    {
	    $user = Auth::user('id', 'user_role_id');
	    $userRole = UserRole::where('id', $user->user_role_id)->first();
	    if( $userRole->manage_client !=1 ){
		    return redirect('/')->send();
	    }

        $mclass = 'clients';
        $countries = DB::table('countries')->pluck('name', 'id');
        $account_groups = DB::table('account_groups')->pluck('name', 'id');
        $title = "Add a new Client";
        $clients = DB::table('accounts')->select('fname', 'lname', 'created_at', 'group_id', 'id')->get();
        return view('client.add', compact('title', 'clients', 'countries', 'account_groups', 'mclass'));
    }

    public function addAccountGetAcc()
    {
        $mclass = 'clients';
        $countries = DB::table('countries')->pluck('name', 'id');
        $account_groups = DB::table('account_groups')->pluck('name', 'id');
        $account_types = DB::table('account_types')->pluck('name', 'id');
        $title = "Add a new account";
        $clients = DB::table('accounts')->select('fname', 'lname', 'created_at', 'group_id', 'id')->get();
        return view('client.addaccount', compact('title', 'clients', 'countries', 'account_groups', 'account_types', 'mclass'));
    }

    public function addAccountPost(Request $request)
    {
//        pr($request->input());

        $this->validate($request, [
            'email' => 'required|email|max:255|unique:accounts',
            'phone' => 'required|min:9|max:12',
            'fname' => 'required',
            'lname' => 'required',
            'zipcode' => 'numeric',
            'country_id' => 'required',
        ]);
        $getAllInput = $request->input();
        if(isset($getAllInput['notification_status']) && $getAllInput['notification_status'] == 'on'){$notification_status = 1; } else{$notification_status = 0;}
        $genPassword = $getAllInput['email'].'-'.rand(199,3984);
        $clients = Account::create([
            'fname' => $getAllInput['fname'],
            'lname' => $getAllInput['lname'],
            'company' => $getAllInput['company'],
            'email' => $getAllInput['email'],
            'password' => bcrypt($genPassword),
            'notification_status' => $notification_status,
            'phone' => $getAllInput['phone'],
            'group_id' => $getAllInput['group_id'],
            'address_1' => $getAllInput['address_1'],
            'address_2' => $getAllInput['address_2'],
            'city' => $getAllInput['city'],
            'state' => $getAllInput['state'],
            'zipcode' => $getAllInput['zipcode'],
            'country_id' => $getAllInput['country_id'],
            'remember_token' => $getAllInput['_token']
        ]);

        if (isset($clients) && $clients !== false) {
            $getTemplate = EmailTemplate::where('id',9)->first();
            $getSettings = Setting::where('id',1)->first();

            $templateBody = $getTemplate->body;
            $subject_pre = $getTemplate->subject;



            // Generate Variable arrays
            $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
            $sysUrl = URL::to('/');

            $arrToExtract = array();

            if($request->input('fname') &&  $request->input('lname')){
                $arrToExtract['name'] = $request->input('fname').' '.$request->input('lname');
            }

            if($request->input('email')){
                $arrToExtract['username'] = $request->input('email');
                $arrToExtract['password'] = $genPassword;
            }

            $arrToExtract['business_name'] = $getSettings->company_name;
            $arrToExtract['logo'] = $getLogoUrl;
            $arrToExtract['sys_url'] = $sysUrl;
            // Generate Variable arrays
            

            $body = replaceWithEtemplateVar($templateBody,$arrToExtract);
            $subject = replaceWithEtemplateVar($subject_pre, $arrToExtract);

            $mailUse = array('to' => $getAllInput['email'], 'name' => $arrToExtract['name'], 'subject' => $subject);
            $data = array('body' => $body);

            Mailgun::send(['html' => 'emails.signup_manual'], $data, function ($message) use($mailUse) {
                $message->to($mailUse['to'], $mailUse['name'])->subject($mailUse['subject']);
            });

            Session::flash('message', 'A client has been added !');
        }else{
            Session::flash('error', 'Data can not save!');
        }
        return redirect('clients/')->send();

    }

    public function addAccountPostAcc(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email|max:255|unique:accounts',
            'phone' => 'required|min:9|max:12',
            'fname' => 'required',
            'lname' => 'required',
            'zipcode' => 'numeric',
            'country_id' => 'required',
        ]);
        $getAllInput = $request->input();
        if(isset($getAllInput['notification_status']) && $getAllInput['notification_status'] == 'on'){$notification_status = 1; } else{$notification_status = 0;}
        $clients = Account::create([
            'fname' => $getAllInput['fname'],
            'lname' => $getAllInput['lname'],
            'company' => $getAllInput['company'],
            'email' => $getAllInput['email'],
            'notification_status' => $notification_status,
            'phone' => $getAllInput['phone'],
            'group_id' => $getAllInput['group_id'],
            'account_type_id' => $getAllInput['account_type_id'],
            'address_1' => $getAllInput['address_1'],
            'address_2' => $getAllInput['address_2'],
            'city' => $getAllInput['city'],
            'state' => $getAllInput['state'],
            'zipcode' => $getAllInput['zipcode'],
            'country_id' => $getAllInput['country_id'],
        ]);

        if (isset($clients) && $clients !== false) {
            Session::flash('message', 'A client has been added !');
        }else{
            Session::flash('error', 'Data can not save!');
        }
        return redirect('accounts/')->send();

    }

    public function editAccountGet($id){
        $mclass = 'clients';
        $countries = DB::table('countries')->pluck('name', 'id');
        $account_groups = DB::table('account_groups')->pluck('name', 'id');
        $title = "Add a new Client";
        $clients = DB::table('accounts')->where('id', '=', $id)->first();
        return view('client.edit', compact('title', 'clients', 'countries', 'account_groups', 'mclass'));
    }

    public function editAccountGetAcc($id){
        $mclass = 'clients';
        $countries = DB::table('countries')->pluck('name', 'id');
        $account_groups = DB::table('account_groups')->pluck('name', 'id');
        $title = "Add a new account";
        $accounts = DB::table('accounts')->where('id', '=', $id)->first();
        $account_types = DB::table('account_types')->pluck('name', 'id');
        return view('client.editaccount', compact('title', 'accounts', 'countries', 'account_groups', 'account_types', 'mclass'));
    }

    public function editAccountPost($id = null, Request $request){
        $getAllInput = $request->input();
//        pr($getAllInput);
        if(isset($getAllInput['notification_status']) && $getAllInput['notification_status'] == 'on'){$notification_status = 1; } else{$notification_status = 0;}
        $clients = DB::table('accounts')->where('id', $id)->update([
            'fname' => $getAllInput['fname'],
            'lname' => $getAllInput['lname'],
            'company' => $getAllInput['company'],
            'email' => $getAllInput['email'],
            'notification_status' => $notification_status,
            'phone' => $getAllInput['phone'],
            'group_id' => $getAllInput['group_id'],
            'address_1' => $getAllInput['address_1'],
            'address_2' => $getAllInput['address_2'],
            'city' => $getAllInput['city'],
            'state' => $getAllInput['state'],
            'zipcode' => $getAllInput['zipcode'],
            'country_id' => $getAllInput['country_id']
        ]);

        if (isset($clients) && $clients !== false) {
            Session::flash('message', 'The client\'s Information Updated !');
        }else{
            Session::flash('error', 'Data can not save!');
        }
        return redirect('clients/')->send();
    }

    public function editAccountPostAcc($id = null, Request $request){
        $getAllInput = $request->input();
        if(isset($getAllInput['notification_status']) && $getAllInput['notification_status'] == 'on'){$notification_status = 1; } else{$notification_status = 0;}
        $update_account = DB::table('accounts')->where('id', $id)->update([
            'fname' => $getAllInput['fname'],
            'lname' => $getAllInput['lname'],
            'company' => $getAllInput['company'],
            'email' => $getAllInput['email'],
            'notification_status' => $notification_status,
            'phone' => $getAllInput['phone'],
            'account_type_id' => $getAllInput['account_type_id'],
            'group_id' => $getAllInput['group_id'],
            'address_1' => $getAllInput['address_1'],
            'address_2' => $getAllInput['address_2'],
            'city' => $getAllInput['city'],
            'state' => $getAllInput['state'],
            'zipcode' => $getAllInput['zipcode'],
            'country_id' => $getAllInput['country_id']
        ]);

        if (isset($update_account) && $update_account !== false) {
            Session::flash('message', 'The account\'s Information Updated !');
        }else{
            Session::flash('error', 'The account can not update!');
        }
        return redirect('accounts/')->send();
    }

    public function deleteAccountGet(Request $request){
       $id = $request->input('id');
        if($id){

            $userInvoice = DB::table('invoices')->where('account_id', $id)->pluck('id');
            if(count($userInvoice)>0){
                Invoice::whereIn('id', $userInvoice->toArray())->delete();
                InvoiceItem::whereIn('invoice_id', $userInvoice->toArray())->delete();
            }

            $userOrder = DB::table('orders')->where('client_id', $id)->pluck('id');
            if(count($userOrder)>0) {
                Order::whereIn('id', $userOrder->toArray())->delete();
                OrderItem::whereIn('order_id', $userOrder->toArray())->delete();
            }


            $userRecurrings = DB::table('recurrings')->where('account_id', $id)->pluck('id');
            if(count($userRecurrings)>0) {
                Recurring::whereIn('id', $userRecurrings->toArray())->delete();
            }


            $userTickets = DB::table('tickets')->where('client_id', $id)->pluck('id');
            if(count($userTickets)>0) {
                Ticket::whereIn('id', $userTickets->toArray())->delete();
                TicketMessage::whereIn('ticket_id', $userTickets->toArray())->delete();
            }


            $userTransactions = DB::table('transactions')->where('for', $id)->orWhere('from', $id)->pluck('id');
            if(count($userTransactions)>0) {
                Transaction::whereIn('id', $userTransactions->toArray())->delete();
            }



            if($userInvoice && $userOrder && $userRecurrings && $userTickets && $userTransactions) {
                $deleteAccount = Account::where('id', $id)->delete();
            }
            if($deleteAccount){
                Session::flash('message', 'The client has been Removed !');
            }else{
                Session::flash('error', 'Can not delete the client!');
            }
        }
        return redirect('clients/')->send();
    }

    public function clientsGroupDeletePost(Request $request){
       $id = $request->input('id');
        if($id){
            $delete_group = AccountGroup::where('id', $id)->delete();
            if($delete_group){
                Session::flash('message', 'The client Group has been Removed !');
            }else{
                Session::flash('error', 'Can not delete the client group!');
            }
        }
        return redirect('clients/groups/')->send();
    }

    public function deleteAccountGetAcc($id){
        pr($id);
    }

    public function clientsGroupList(){
	    $user = Auth::user('id', 'user_role_id');
	    $userRole = UserRole::where('id', $user->user_role_id)->first();
	    if( $userRole->manage_client !=1 ){
		    return redirect('/')->send();
	    }

        $title = "Manage Client Groups";

        $groups = DB::table('account_groups')
            ->select('account_groups.name', 'account_groups.id', DB::raw('COUNT(accounts.group_id) as total'))
            ->leftJoin('accounts','account_groups.id','=','accounts.group_id')
            ->groupBy('account_groups.name')
            ->groupBy('account_groups.id')
            ->get();

//        pr($groups);

        return view('client.grouplist', compact('title', 'groups'));
    }

    public function clientsGroupAddGet(){
        $title = "Add a new Client Groups";
        $groups = DB::table('account_groups')->select('id', 'name')->get();
        return view('client.addgroup', compact('title', 'groups'));
    }

    public function clientsGroupEditGet($id){
        $title = "Edit an Existing Client Groups";
        $groups = DB::table('account_groups')->select('id', 'name')->where('id',$id)->first();
        return view('client.editgroup', compact('title', 'groups'));
    }

    public function clientsGroupEditPost($id = null, Request $request){

        $this->validate($request, [
            'name' => 'required'
        ]);
        $getAllInput = $request->input();
        $clients = DB::table('account_groups')->where('id', $id)->update([
            'name' => $getAllInput['name'],
        ]);

        if (isset($clients) && $clients !== false) {
            Session::flash('message', 'The client group has been updated !');
        }else{
            Session::flash('error', 'Data can not save!');
        }
        return redirect('clients/groups')->send();
    }


    public function clientsGroupAddPost(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);
        $getAllInput = $request->input();
        $acGroup = AccountGroup::create([
            'name' => $getAllInput['name'],
        ]);
        if (isset($acGroup) && $acGroup !== false) {
            Session::flash('message', 'A client group has been added !');
        }else{
            Session::flash('error', 'Data can not save!');
        }
        return redirect('clients/groups')->send();
    }

    public function clientsBulkMail(){
        $title = "Send bulk email.";
        $getAccountGroups = DB::table('account_groups')
            ->select('account_groups.name', 'account_groups.id', DB::raw('COUNT(accounts.group_id) as total'))
            ->rightJoin('accounts','account_groups.id','=','accounts.group_id')
            ->groupBy('account_groups.name')
            ->groupBy('account_groups.id')
            ->get();

//        pr($getAccountGroups);
        $account_groups_list = array();
        foreach ($getAccountGroups as $group){
            $account_groups_list[$group->id] = $group->name.'('.$group->total.')';
        }

//        pr($account_groups_list);
        $pselect = array('value="" disabled selected' => 'Please Select Contact Group');
        $account_groups =  $pselect + $account_groups_list;


        return view('client/bulkemail', compact('title', 'account_groups'));
    }
    
}
