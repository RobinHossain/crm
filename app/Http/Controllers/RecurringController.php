<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Invoice;
use App\InvoiceItem;
use App\Recurring;
use App\Tax;
use App\User;
use App\UserRole;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;
use App\Transaction;


class RecurringController extends Controller
{
    use FormAccessible;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $title = "Recurring List";
        $recurrings = Recurring::with('invoice','account')->get();
        return view('recurring.index', compact('title', 'recurrings'));
    }

    public function addGet(){
        $title = "Add Recurring";
        $get_account_list = Account::all('id', 'fname', 'lname');
        $get_product_list = Product::all('id', 'name', 'price');
        $accounts = array();
        $products = array();
        foreach($get_account_list as $acl){
            $accounts[$acl->id] = $acl->fname." ".$acl->lname;
        }
        foreach($get_product_list as $prdct){
            $products[$prdct->id] = $prdct->name;
        }

        $tax_list = Tax::all('id','name','value');
        $taxs = array();
        foreach($tax_list as $tax){
            $taxs[$tax->id] = $tax->name." (".$tax->value.")";
        }
        $recurring_types = DB::table('recurring_types')->pluck('name', 'id');

        return view('recurring.add', compact('title', 'accounts', 'products', 'recurring_types', 'taxs'));

    }

    public function addPost(Request $request){
        $this->validate($request, [
            'account_id' => 'required|numeric',
            'product' => 'required|numeric',
            'price' => 'required|numeric',
            'recurring_type' => 'required|numeric',
            'tax_id' => 'numeric',
            'how_many' => 'required|numeric'
        ]);

        if($request->tax_id){
            $textId = $request->tax_id;
        }else{
            $textId = null;
        }

        $invoice_status_id = 2;
        $create_invoice = Invoice::create([
            'account_id' => $request->input('account_id'),
            'tax_id' => $textId,
            'note' => $request->input('note'),
            'invoice_status_id' => $invoice_status_id,
            'due_date' => $request->input('due_date')
        ]);
        $invoice_id = $create_invoice->id;


        if($invoice_id){
            $invoice_items = InvoiceItem::create([
                'product_id' => $request->input('product'),
                'price' => $request->input('price')
            ]);
            $invoice_item_id = $invoice_items->id;

            if($invoice_item_id){
                $create_recurring = Recurring::create([
                    'invoice_id' => $invoice_id,
                    'account_id' => $request->input('account_id'),
                    'note' => $request->input('note'),
                    'how_many' => $request->input('how_many'),
                    'recurring_type' => $request->input('recurring_type'),
                    'next_due' => $request->input('next_due')
                ]);

                if($create_recurring){
                    Session::flash('message', 'A Recurring Invoice has been created !');
                }else{
                    Session::flash('error', 'Recurring can not create!');
                }
            }else{
                Session::flash('error', 'Invoice can not create, recurring stopped for that!');
            }
        }else{
            Session::flash('error', 'Invoice Item can not create, Invoice and recurring stopped for that!');
        }

        return redirect('recurring/')->send();
    }


}
