<?php

namespace App\Http\Controllers;

// For validation
use App\EmailTemplate;
use App\Payment;
use App\Support;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class SupportController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */




    public function index($id = null){
        $title = "Support Department List";
        $supports = Support::all();
        if($id){
            $support_single = DB::table('supports')->where('id', $id)->first();
        }else{
            $support_single = null;
        }
        return view('support.index', compact('title', 'supports', 'support_single'));
    }

    public function addPost(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $status = 0;
        if ($request->input('show_client')) {
            if($request->input('show_client') == 'on'){
                $status = 1;
            }elseif($request->input('show_client') == 'off'){
                $status = 0;
            }else{
                $status = $request->input('show_client');
            }
        }

        $create = Support::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'show_client' => $status
        ]);

        if ($create) {
            Session::flash('message', 'The Support Info has been Created !');
        } else {
            Session::flash('error', 'Can not create the Support option !');
        }

        return redirect('support/')->send();
    }

    public function editPost($id = null, Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $status = 0;
        if ($request->input('show_client')) {
            if($request->input('show_client') == 'on'){
                $status = 1;
            }elseif($request->input('show_client') == 'off'){
                $status = 0;
            }else{
                $status = $request->input('show_client');
            }
        }

        $support = DB::table('supports')->where('id', $id)->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'show_client' => $status
        ]);

        if ($support) {
            Session::flash('message', 'The Support info has been updated !');
        } else {
            Session::flash('error', 'Can not update the Support info !');
        }

        return redirect('support/')->send();
    }

    public function delete($id = null){
        if ($id) {
            $delete = Support::where('id', $id)->delete();
            if ($delete) {
                Session::flash('message', 'The support dept has been deleted !');
            } else {
                Session::flash('error', 'Can not delete the product dept !');
            }
        }

        return redirect('support/')->send();
    }

}
