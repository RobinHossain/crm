<?php

namespace App\Http\Controllers;

// For validation
use App\EmailTemplate;
use App\Payment;
use App\Setting;
use App\SettingSliders;
use App\Support;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class SettingController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $title = "Settings";
        $countries = DB::table('countries')->pluck('name', 'id');
        $settings = DB::table('settings')->where('id',1)->first();
        return view('setting.index', compact('title', 'countries', 'settings'));
    }

    public function devDocs(){
        $title = "Documentation";
        return view('setting.documentation', compact('title'));
    }

    public function getSliders(){
        $title = "Client Homepage Slider";
        $sliders = SettingSliders::select('id', 'title', 'image', 'created_at')->get();
        return view('setting.slider.index', compact('title', 'sliders'));
    }

    public function getAddSlider(){
        $title = 'Add a new slider';
        return view('setting.slider.add', compact('title'));
    }

    public function postAddSlider(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

        ]);

        $randomFileName = str_random(12);

        if($request->image){
            $logoName = 'slider_'.$randomFileName.'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('imgs/slider'), $logoName);

        }
    }

    public function updatePost(Request $request){
        $id = 1;
        $this->validate($request, [
            'company_name' => 'required',
            'website_title' => 'required',
            'default_email' => 'required|email',
            'pay_address' => 'required',
            'client_panel' => 'required',
            'copyright_text' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $setting_restriction = env('SETTING_RESTRICTION', '0');

        if( $setting_restriction == 0 ){
            $settings = DB::table('settings')->where('id', $id)->update([
                'company_name' => $request->input('company_name'),
                'default_email' => $request->input('default_email'),
                'pay_address' => $request->input('pay_address'),
                'default_country_id' => $request->input('default_country_id'),
                'default_currency' => $request->input('default_currency'),
                'default_currency_symbol' => $request->input('default_currency_symbol'),
                'website_title' => $request->input('website_title'),
                'brand_name' => $request->input('brand_name'),
                'copyright_text' => $request->input('copyright_text'),
                'client_panel' => $request->input('client_panel'),
                'cron_url' => $request->input('cron_url'),
            ]);

            if($request->logo){
                $logoName = 'logo'.'.'.$request->logo->getClientOriginalExtension();
                $request->logo->move(public_path('imgs'), $logoName);
                $settings = DB::table('settings')->where('id', $id)->update([
                    'logo' => $logoName
                ]);
            }
	        Session::flash('message', 'The setting has been updated !');
        } else{
            Session::flash('error', 'Changing system setting restricted !');
        }

        return redirect('settings/')->send();

    }

}
