<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Article;
use App\ArticleCategory;
use App\Faq;
use App\Invoice;
use App\Ticket;
use App\TicketMessage;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Mews\Purifier\Facades\Purifier;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class ArticleController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /*
     * Article Main Page
     */
    public function index()
    {
        $title = "All Article List";
        $articles = Article::with('category')->get();
//        pr($articles);
        return view('article.index', compact('title', 'articles'));
    }

    /*
     * Get FAQ List
     */
    public function faqGet(){
        $title = "All Faq List";
        $faqs = Faq::with('category')->get();
        return view('faq.index', compact('title', 'faqs'));
    }

    public function faqAddGet(){
        $title = "Add a new faq item";
        $categories = DB::table('faq_categories')->pluck('name', 'id');
        return view('faq.add', compact('title', 'categories'));
    }

    public function faqAddPost(Request $request){

//        pr($request->input());
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $faq = Faq::create([
            'title' => $request->input('title'),
            'content' => $request->input('content')
        ]);

        if($faq->id){
            Session::flash('message', 'An faq item has been added !');
        }else{
            Session::flash('error', 'Faq item can not add!');
        }

        return redirect('faq/')->send();

    }

    public function faqEditGet($id = null){

        $title = "Edit Faq Item";
        $faq = Faq::where('id', $id)->first();
        return view('faq.edit', compact('title','faq'));
    }

    public function faqEditPost($id = null, Request $request){
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $faq = DB::table('faqs')->where('id', $id)->update([
            'title' => $request->input('title'),
            'content' => $request->input('content')
        ]);

        if($faq){
            Session::flash('message', 'The faq item has been updated !');
        }else{
            Session::flash('error', 'Faq item can not update right now!');
        }

        return redirect('faq/')->send();
    }

    public function add(){
        $title = "Add a new article";
        $categories = DB::table('article_categories')->pluck('name', 'id');
        $get_account_list = User::all('id', 'fname', 'lname');
        $authors = array();
        foreach($get_account_list as $acl){
            $authors[$acl->id] = $acl->fname." ".$acl->lname;
        }

        return view('article.add', compact('title', 'categories', 'authors'));

    }

    public function addPost(Request $request){

//        pr($request->input());
        $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required|numeric',
            'author_id' => 'required|numeric',
            'content' => 'required',
        ]);

        $status = 0;
        if($request->input('status') && $request->input('status') == 'on'){
            $status = 1;
        }


        $article = Article::create([
            'title' => $request->input('title'),
            'category_id' => $request->input('category_id'),
            'author_id' => $request->input('author_id'),
            'content' => $request->input('content'),
            'status' => $status
        ]);

        if($article->id){
            Session::flash('message', 'An article has been added !');
        }else{
            Session::flash('error', 'Article can not add!');
        }

        return redirect('article/')->send();

    }

    public function faqDeleteGet($id = null){
        if($id){
            $delete_faq = Faq::where('id', $id)->delete();
            if($delete_faq){
                Session::flash('message', 'The Faq item has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete this faq item!');
            }
        }
        return redirect('faq/')->send();
    }

    public function edit($id = null){
        $title = "Add a new article";
        $categories = DB::table('article_categories')->pluck('name', 'id');
        $get_account_list = User::all('id', 'fname', 'lname');
        $authors = array();
        foreach($get_account_list as $acl){
            $authors[$acl->id] = $acl->fname." ".$acl->lname;
        }

        $article = DB::table('articles')->select('id', 'title', 'category_id', 'content', 'author_id', 'status')->where('id', $id)->first();

        return view('article.edit', compact('title', 'categories', 'authors', 'article'));
    }

    public function editPost($id = null, Request $request){
        $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required|numeric',
            'author_id' => 'required|numeric',
            'content' => 'required',
        ]);

        $status = 0;
        if($request->input('status') && $request->input('status') == 'on'){
            $status = 1;
        }

        $article = DB::table('articles')->where('id', $id)->update([
            'title' => $request->input('title'),
            'category_id' => $request->input('category_id'),
            'author_id' => $request->input('author_id'),
            'content' => $request->input('content'),
            'status' => $status
        ]);

        if($article){
            Session::flash('message', 'The article has been updated !');
        }else{
            Session::flash('error', 'Article can not update right now!');
        }

        return redirect('article/')->send();
    }

    public function delete($id = null){
        if($id){
            $delete_article = Article::where('id', $id)->delete();
            if($delete_article){
                Session::flash('message', 'The Article has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete the article!');
            }
        }
        return redirect('article/')->send();
    }

    public function categories(){
        $title = "Article Categories";
        $categories = ArticleCategory::all();
        return view('article.categories', compact('title', 'categories'));
    }

    public function categoryAdd(){
        $title = "Add an Article Category";
        return view('article.addCategory', compact('title'));
    }

    public function categoryAddPost(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $article_category = ArticleCategory::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug')
        ]);

        if($article_category->id){
            Session::flash('message', 'An article category has been added !');
        }else{
            Session::flash('error', 'Article category can not add!');
        }

        return redirect('article/categories/')->send();

    }

    public function categoryEdit($id = null){
        $title = "Edit Existing Category";
        $category = DB::table('article_categories')->select('id', 'name', 'slug')->where('id', $id)->first();
        return view('article.editCategory', compact('title', 'category'));
    }

    public function categoryEditPost($id = null, Request $request){
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $category = DB::table('article_categories')->where('id', $id)->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug')
        ]);

        if($category){
            Session::flash('message', 'The article category has been updated !');
        }else{
            Session::flash('error', 'Article category can not update right now!');
        }

        return redirect('article/categories/')->send();
    }

    public function categoryDeletePost($id = null, Request $request){
    	if( !$id ){ $id = $request->id; }
        if($id){
            $delete_category = ArticleCategory::where('id', $id)->delete();
            if($delete_category){
                Session::flash('message', 'The Article Category has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete the article category!');
            }
        }
        return redirect('article/categories/')->send();
    }


}
