<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\EmailTemplate;
use App\Invoice;
use App\Setting;
use App\Ticket;
use App\TicketCategory;
use App\TicketMessage;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Validator;
use Bogardo\Mailgun\Facades\Mailgun;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;


class TicketController extends Controller
{
    use FormAccessible;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $title = "All Ticket List";
        $tickets = Ticket::with('account', 'category')->orderBy('tickets.created_at', 'desc')->get();
//        pr($tickets->toArray());
        return view('ticket.index', compact('title', 'tickets'));
    }


    public function answerGet()
    {
        $title = "Answered Ticket";
        $tickets = Ticket::with('account', 'category')->get();
        return view('ticket.answer', compact('title', 'tickets'));
    }

    public function historyGet($id){
        $title = "Ticket History";
        $ticket = Ticket::with('account', 'category')->where('id', $id)->first();
        $histories = TicketMessage::where('ticket_id',$id)->orderBy('ticket_messages.created_at', 'desc')->get();
	    $getSettings = Setting::select('client_panel', 'id')->where('id',1)->first();
	    $client_panel = $getSettings->client_panel;
        $history_id = $id;
//        pr($histories->toarray());
        return view('ticket.history', compact('title', 'ticket', 'histories', 'history_id', 'client_panel'));
    }

    public function replyGet($id){
        $title = "Reply Ticket";
        $lastMessage = TicketMessage::where('id', $id)->first();
        return view('ticket.reply', compact('title', 'lastMessage'));
    }

    public function replyPost($id, Request $request){

        $ticket_id = $id;

        $userId = Auth::user()->id;
        $date = Carbon::now()->format('Y-m-d');
        $message_from = 'Admin';


        $clients = DB::table('tickets')->where('id', $ticket_id)->update([
            'answered' => 1
        ]);

        if($request->input('reply')){
            $message = TicketMessage::create([
                'ticket_id' => $ticket_id,
                'message' => $request->input('reply'),
                'message_from' => $message_from,
                'user_id' => $userId,
                'date' => $date
            ]);
            if ($message->id) {

                $getTicketInfo = Ticket::where('id', $ticket_id)->first();
                $getClientId = $getTicketInfo->client_id;
                $ticket_subject = $getTicketInfo->subject;

                $ticket_contents = $request->reply;


                $customerInfo = Account::where('id', $getClientId)->select('fname','lname','email')->first();

//                pr($customerInfo);

                $getTemplate = EmailTemplate::where('id',2)->first();
                $getSettings = Setting::where('id',1)->first();

                $templateBody = $getTemplate->body;
                $subject_pre = $getTemplate->subject;

                // Generate Variable arrays
                $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
                $sysUrl = URL::to('/');

                $arrToExtract = array();
                $arrToExtract['name'] = $customerInfo->fname.' '.$customerInfo->lname;
                $arrToExtract['email'] = $customerInfo->email;

                $arrToExtract['business_name'] = $getSettings->company_name;
                $arrToExtract['logo'] = $getLogoUrl;
                $arrToExtract['sys_url'] = $sysUrl;

                $arrToExtract['ticket_number'] = $getTicketInfo->id;
                $arrToExtract['ticket_subject'] = $ticket_subject;
                $arrToExtract['ticket_contents'] = $ticket_contents;

                $body = replaceWithEtemplateVar($templateBody,$arrToExtract);
                $subject = replaceWithEtemplateVar($subject_pre, $arrToExtract);

                $mailUse = array('to' => $customerInfo->email, 'name' => $customerInfo->fname.' '.$customerInfo->lname, 'subject' => $subject);
                $data = array('body' => $body);

                Mailgun::send(['html' => 'emails.ticket_new'], $data, function ($message) use($mailUse) {
                    $message->to($mailUse['to'], $mailUse['name'])->subject($mailUse['subject']);
                });

                Session::flash('message', 'The Message replied !');
            } else {
                Session::flash('error', 'Can not reply the message!');
            }
        }else{
            return Redirect::back()->with('message','Please check your reply messages !');
        }

        return Redirect::back();

    }

    public function categoryGet($id = null)
    {
        $title = "All Ticket List";
        if ($id) {
            $tickets = Ticket::with('account', 'category')->where('ticket_category_id', $id)->get();
        } else {
            $tickets = Ticket::with('account', 'category')->get();
        }

        return view('ticket.index', compact('title', 'tickets'));
    }

    public function deleteTicket( Request $request ){
        $this->validate($request, [
            'id' => 'required'
        ]);
        $id = $request->id;
        if($id){
            $delete_ticket = Ticket::where('id', $id)->delete();
            $delete_ticket_message = TicketMessage::where('ticket_id', $id)->delete();
            if( $delete_ticket && $delete_ticket_message ){
                Session::flash('message', 'The Ticket has been Deleted !');
            }else{
                Session::flash('error', 'Can not delete the Ticket!');
            }
        }
        return redirect('ticket')->send();
    }

    public function answeredGet(){
        $title = "All Answered Ticket List";
        $tickets = Ticket::where('answered', 1)->get();
        return view('ticket.answered', compact('title', 'tickets'));
    }

    public function clientRepliedGet(){
        $title = "Customer Replied Ticket List";
        $tickets = Ticket::where('answered','!=',1)->get();
        return view('ticket.answered', compact('title', 'tickets'));
    }

    public function categoryActiveGet()
    {
        $title = "All Ticket List";
        $tickets = Ticket::with('account', 'category')->where('status', 1)->get();
        return view('ticket.index', compact('title', 'tickets'));
    }

    public function categoryCloseGet()
    {
        $title = "All Ticket List";
        $tickets = Ticket::with('account', 'category')->where('status', 0)->get();
        return view('ticket.index', compact('title', 'tickets'));
    }

	public function categories(){
		$title = "Ticket Categories";
		$categories = TicketCategory::all();
		return view('ticket.categories', compact('title', 'categories'));
	}

	public function categoryAdd(){
		$title = "Add an Ticket Category";
		return view('ticket.addCategory', compact('title'));
	}

	public function categoryAddPost(Request $request){

		$this->validate($request, [
			'name' => 'required'
		]);

		if(!empty($request->slug)){
			$slug = $request->slug;
		}else{
			$slug = str_replace(' ','-',$request->name);
			$slug = strtolower($slug);
		}


		$ticket_category = TicketCategory::create([
			'name' => $request->name,
			'slug' => $slug
		]);

		if($ticket_category->id){
			Session::flash('message', 'An Ticket category has been added !');
		}else{
			Session::flash('error', 'Ticket category can not add!');
		}

		return redirect('ticket/categories/')->send();

	}

	public function categoryEdit($id = null){
		$title = "Edit Existing Category";
		$category = DB::table('ticket_categories')->select('id', 'name', 'slug')->where('id', $id)->first();
		return view('ticket.editCategory', compact('title', 'category'));
	}

	public function categoryEditPost($id = null, Request $request){
		$this->validate($request, [
			'name' => 'required'
		]);

		if(!empty($request->slug)){
			$slug = $request->slug;
		}else{
			$slug = str_replace(' ','-',$request->name);
			$slug = strtolower($slug);
		}

		$category = DB::table('ticket_categories')->where('id', $id)->update([
			'name' => $request->input('name'),
			'slug' => $slug
		]);

		if($category){
			Session::flash('message', 'The ticket category has been updated !');
		}else{
			Session::flash('error', 'Ticket category can not update right now!');
		}

		return redirect('ticket/categories/')->send();
	}

	public function categoryDeletePost($id = null, Request $request){
		if( !$id ){ $id = $request->id; }
		if($id){
			$delete_category = TicketCategory::where('id', $id)->delete();
			if($delete_category){
				Session::flash('message', 'The Ticket Category has been Deleted !');
			}else{
				Session::flash('error', 'Can not delete the ticket category!');
			}
		}
		return redirect('ticket/categories/')->send();
	}


    public function add()
    {
        $title = "Create a new ticket";
        $get_account_list = Account::all('id', 'fname', 'lname');
        $accounts = array();
        foreach ($get_account_list as $acl) {
            $accounts[$acl->id] = $acl->fname . " " . $acl->lname;
        }
        $ticket_categories = DB::table('ticket_categories')->pluck('name', 'id');


        return view('ticket.add', compact('title', 'accounts', 'ticket_categories'));

    }

    public function addPost(Request $request)
    {
        $userId = Auth::user()->id;

        $this->validate($request, [
            'ticket_for' => 'required|numeric',
            'category' => 'required|numeric',
            'message' => 'required',
            'subject' => 'required',
        ]);

        $status = 0;
        if ($request->input('status') && $request->input('status') == 'on') {
            $status = 1;
        }

        $message_from = "Admin";





        $ticket = Ticket::create([
            'client_id' => $request->input('ticket_for'),
            'ticket_category_id' => $request->input('category'),
            'subject' => $request->input('subject'),
            'status' => $status,
        ]);

        $ticketId = $ticket->id;
        $date = Carbon::now()->format('Y-m-d');

        if ($ticketId) {

            if($request->file){
                $randomName = time().rand(33,39);
                $fileName = $randomName.'_'.$request->file->getClientOriginalName();
                $request->file->move(public_path('imgs/tickets'), $fileName);
            }else{
                $fileName = null;
            }

            $message = TicketMessage::create([
                'ticket_id' => $ticketId,
                'message' => $request->input('message'),
                'message_from' => $message_from,
                'user_id' => $userId,
                'date' => $date,
                'file' => $fileName
            ]);



            if ($message->id) {

                $ticket_contents = $request->message;
                $ticket_subject = $request->subject;
                $clientId = $request->ticket_for;

                $customerInfo = Account::where('id', $clientId)->select('fname','lname','email')->first();

                $getTemplate = EmailTemplate::where('id',8)->first();
                $getSettings = Setting::where('id',1)->first();

                $templateBody = $getTemplate->body;
                $subject_pre = $getTemplate->subject;

                // Generate Variable arrays
                $getLogoUrl = '<img src="'.URL::to('imgs/'.$getSettings->logo).'" alt="Logo" />';
                $sysUrl = URL::to('/');

                $arrToExtract = array();
                $arrToExtract['name'] = $customerInfo->fname.' '.$customerInfo->lname;
                $arrToExtract['email'] = $customerInfo->email;

                $arrToExtract['business_name'] = $getSettings->company_name;
                $arrToExtract['logo'] = $getLogoUrl;
                $arrToExtract['sys_url'] = $sysUrl;

                $arrToExtract['ticket_number'] = $ticketId;
                $arrToExtract['ticket_subject'] = $ticket_subject;
                $arrToExtract['ticket_contents'] = $ticket_contents;

                $body = replaceWithEtemplateVar($templateBody,$arrToExtract);
                $subject = replaceWithEtemplateVar($subject_pre, $arrToExtract);

                $mailUse = array('to' => $customerInfo->email, 'name' => $customerInfo->fname.' '.$customerInfo->lname, 'subject' => $subject);
                $data = array('body' => $body);

                Mailgun::send(['html' => 'emails.ticket_new'], $data, function ($message) use($mailUse) {
                    $message->to($mailUse['to'], $mailUse['name'])->subject($mailUse['subject']);
                });



                Session::flash('message', 'A ticket has been created !');
            } else {
                Session::flash('error', 'Ticket can not add!');
            }
        }

        return redirect('ticket')->send();


    }

}
