<?php

namespace App\Http\Controllers;

// For validation
use App\Account;
use App\Invoice;
use App\InvoiceItem;
use App\Tax;
use App\User;
use App\UserRole;
use Dompdf\Dompdf;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

// For DB Selection
use App\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Order;
use App\OrderItem;
use App\Coun;
use App\Product;
use App\Transaction;


class ExportController extends Controller
{
    use FormAccessible;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function invoice(){
        $title = "All Invoice";
        $invoices = DB::table('invoices')->select('account_id','price','tax_id','note')->get();

        $invoices = $invoices->toArray();

        $data = array(
            array('data1', 'data2'),
            array('data3', 'data4')
        );

//        $invoices = Invoice::all();

        Excel::create('Filename', function($excel) use($invoices) {

            $excel->sheet('Sheetname', function($sheet) use($invoices) {

                $sheet->fromModel($invoices);

            });

        })->export('csv');

//        Excel::create('Filename', function($excel) {
//
//            // Set the title
//            $excel->setTitle('Our new awesome title');
//
//            // Chain the setters
//            $excel->setCreator('Maatwebsite')
//                ->setCompany('Maatwebsite');
//
//            // Call them separately
//            $excel->setDescription('A demonstration to change the file properties');
//
//        });


    }


}
