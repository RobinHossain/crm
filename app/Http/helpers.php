<?php


function generateLastMonthDates($monthData){
    $newArr  = array();
    $newInc = 0;
    $begin_pre = date("Y-m-d", strtotime("first day of last month"));
    $end_pre = date("Y-m-d", strtotime("today UTC"));
    $begin = new \DateTime( $begin_pre );
    $end = new \DateTime( $end_pre );
    $end = $end->modify( '+1 day' );

    $interval = new \DateInterval('P1D');
    $daterange = new \DatePeriod($begin, $interval ,$end);

    foreach($daterange as $date){
        $newArr[$newInc]['date'] = $date->format("Y-m-d");
        $newArr[$newInc]['value'] = 0;
        $newInc++;
    }

    for ($final_inc = 0; $final_inc < count($newArr); $final_inc++){
        foreach ($monthData as $r_val){
            if($newArr[$final_inc]['date'] == $r_val['date']){
                $newArr[$final_inc]['value'] = $r_val['value'];
            }
        }
    }
    return $newArr;
}


function generateLastMonthDateDatas($monthDatas){
    $newArr  = array();
    $newInc = 0;
    $begin_pre = date("Y-m-d", strtotime("first day of last month"));
    $end_pre = date("Y-m-d", strtotime("today UTC"));
    $begin = new \DateTime( $begin_pre );
    $end = new \DateTime( $end_pre );
    $end = $end->modify( '+1 day' );

    $interval = new \DateInterval('P1D');
    $daterange = new \DatePeriod($begin, $interval ,$end);

    foreach($daterange as $date){
        $newArr[$newInc]['date'] = $date->format("Y-m-d");
        $newArr[$newInc]['order'] = 0;
        $newArr[$newInc]['invoice'] = 0;
        $newArr[$newInc]['client'] = 0;
        $newArr[$newInc]['sale'] = 0;
        $newInc++;
    }

    for ($final_inc = 0; $final_inc < count($newArr); $final_inc++){
        foreach ($monthDatas as $r_val){
            if($r_val['type'] == 'order' && $newArr[$final_inc]['date'] == $r_val['date']){
                $newArr[$final_inc]['order'] = $r_val['value'];
            }

            if($r_val['type'] == 'invoice' && $newArr[$final_inc]['date'] == $r_val['date']){
                $newArr[$final_inc]['invoice'] = $r_val['value'];
            }

            if($r_val['type'] == 'client' && $newArr[$final_inc]['date'] == $r_val['date']){
                $newArr[$final_inc]['client'] = $r_val['value'];
            }
            if($r_val['type'] == 'sale' && $newArr[$final_inc]['date'] == $r_val['date']){
                $newArr[$final_inc]['sale'] = $r_val['value'];
            }
        }
    }
    return $newArr;
}

function replaceWithEtemplateVar($beforeStr, $arrToExtract){
    extract($arrToExtract);
    preg_match_all('/{(\w+)}/', $beforeStr, $matches);
    $afterStr = $beforeStr;
    foreach ($matches[0] as $index => $var_name) {
        if (isset(${$matches[1][$index]})) {
            $afterStr = str_replace($var_name, ${$matches[1][$index]}, $afterStr);
        }
    }
    return $afterStr;
}

function pre($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    exit;
}

function pr($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    exit;
}

if (! function_exists('pr')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function pr()
    {
        array_map(function ($x) {
            echo '<pre>';
            print_r($x);
            echo '</pre>';
        }, func_get_args());

        die(1);
    }
}




