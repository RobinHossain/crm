<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['title', 'category_id', 'content', 'author_id', 'status'];

    public function category()
    {
        return $this->belongsTo('App\ArticleCategory', 'category_id');
    }

    public function author(){
        return $this->belongsTo('App\User', 'author_id');
    }
}
