<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'details', 'product_category_id', 'variation', 'price', 'status', 'quantity', 'show_client'];

    public function category()
    {
        return $this->belongsTo('App\ProductCategory', 'product_category_id');
    }
}
