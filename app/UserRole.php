<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
	protected $table = 'user_roles';
	protected $fillable = ['name', 'slug', 'manage_client', 'manage_order_invoice', 'manage_support', 'manage_finance', 'manage_product', 'manage_admin_option'];
	public $timestamps = false;
}
