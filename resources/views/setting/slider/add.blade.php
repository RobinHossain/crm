@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmAppActivity">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <h4 class="table-title">{{ $title }}</h4>
                </div>
            </div>

            <?php if(isset($settings) && !empty($settings)){ ?>
            {!! Form::model($settings, array('class' => '', 'role' => 'form', 'files' => true)) !!}
            <?php }else{?>
            {!! Form::open(array('class' => '', 'role' => 'form', 'files' => true)) !!}
            <?php } ?>
            {{ csrf_field() }}


                <div class="row">

                    <div class="file-field input-field col s12 m12">
                        <div class="btn">
                            <span>Business Logo</span>
                            {{ Form::file('logo') }}
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" name="pay_address" type="text"
                                   placeholder="Upload businesss logo">
                        </div>
                        <small>This will appear in client portal, invoice, on email etc.</small>
                        <br>
                        <span>You can also change the logo manually by replacing the logo.png file (assets/uploads/logo.png) .</span>
                    </div>
                </div>


            <div class="row">
                <div class="col s4 m3 l2"></div>
                <div class="input-field col s12 m6">
                    {{ Form::submit('Save Setting', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
