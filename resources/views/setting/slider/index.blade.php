@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmAppActivity">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <h4 class="table-title center-align">{{ $title }}</h4>
                </div>
            </div>

            <div class="row right-align">
                <a href="{{ url('slider/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i
                            class="fa fa-user-plus" aria-hidden="true"></i> Add a new slider</a>
            </div>

            <hr class="top_title">



            <div id="sliders" class="row">
                @foreach($sliders as $slider)
                    <div class="col s12 m3 l3 slider">
                        <img src="{{ $slider->image }}" alt="{{ $slider->title }}" />
                    </div>
                @endforeach
            </div>


        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
