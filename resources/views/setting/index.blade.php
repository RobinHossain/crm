@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmAppActivity">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <h4 class="table-title">{{ $title }}</h4>
                </div>
            </div>

            <?php if(isset($settings) && !empty($settings)){ ?>
            {!! Form::model($settings, array('class' => '', 'role' => 'form', 'files' => true)) !!}
            <?php }else{?>
            {!! Form::open(array('class' => '', 'role' => 'form', 'files' => true)) !!}
            <?php } ?>
            {{ csrf_field() }}

            <div class="row">
                <div class="tabs-vertical" ng-controller="activityCtrl">
                    <div class="col s12 m3 l2">
                        <ul class="tabs">
                            <li class="tab"><a class="active" href="#business-info" ng-click="showName()">Business
                                    Info</a></li>
                            <li class="tab"><a href="#localization">Localization</a></li>
                            <li class="tab"><a href="#web-config">Website Config</a></li>
                            <li class="tab"><a href="#cron-service">Cron Service</a></li>
                        </ul>
                    </div>
                    <div class="col s12 m9 l8">
                        <div id="business-info" class="tab-content">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">turned_in</i>
                                    {{ Form::text('company_name', null, ['placeholder' => 'Please Input Company Name','class' => 'validate']) }}
                                    {{ Form::label('company_name', 'Company Name') }}
                                    <small>Company Name as you want it to appear throughout the system</small>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">email</i>
                                    {{ Form::text('default_email', null, ['placeholder' => 'info@info.com','class' => 'validate']) }}
                                    {{ Form::label('default_email', 'Default Email Address') }}
                                    <small>This email address will be used for sending email from system.</small>
                                </div>

                            </div>

                            <div class="row">
                                @if($settings->logo)
                                <div class="input-field col s12 m6">
									<div class="profile-image-login">
										<img src="{{ asset('imgs')  }}/logo.png" alt="" />
									</div>
                                    
                                    <span>Your Previous Business Logo.</span>
                                </div>
                                @endif
                                <div class="file-field input-field col s12 m6">
                                    <div class="btn">
                                        <span>Business Logo</span>
                                        {{ Form::file('logo') }}
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" name="pay_address" type="text"
                                               placeholder="Upload businesss logo">
                                    </div>
                                    <small>This will appear in client portal, invoice, on email etc.</small>
                                    <br>
                                    <span>You can also change the logo manually by replacing the logo.png file (assets/uploads/logo.png) .</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">address</i>
                                    {{ Form::textarea('pay_address', null, ['placeholder'=>'Input Address to Pay', 'class' => 'materialize-textarea', 'row' => 1]) }}
                                    {{ Form::label('pay_address', 'Add Address') }}
                                    <small>This text is displayed on the invoice as the Pay To details.</small>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">website</i>
                                    {{ Form::text('client_panel', null, ['placeholder' => 'http://website.com/client','class' => 'validate']) }}
                                    {{ Form::label('client_panel', 'Application Url') }}
                                    <small>The Application Url where you installed your client panel</small>
                                </div>
                            </div>


                        </div>
                        <div id="localization" class="tab-content">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">class</i>
                                    {{ Form::select('default_country_id', $countries, null, ['placeholder' => 'Select Default Country']) }}
                                    <small>Default Country Where Your Business Located.</small>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">invert_colors</i>
                                    {{ Form::text('default_currency', null, ['placeholder' => 'Please Input Company Name','class' => 'validate']) }}
                                    {{ Form::label('default_currency', 'Default Currency') }}
                                    <small>Default currency for transaction.</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">attach_money</i>
                                    {{ Form::text('default_currency_symbol', null, ['placeholder' => 'Input Default Currencty Symbol','class' => 'validate']) }}
                                    {{ Form::label('default_currency_symbol', 'Default Currencty Symbol') }}
                                    <small>All currency will show with symbol.</small>
                                </div>
                            </div>
                        </div>
                        {{--<div id="theme" class="tab-content">--}}

                        {{--</div>--}}
                        <div id="web-config" class="tab-content">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">local_library</i>
                                    {{ Form::text('website_title', null, ['placeholder' => 'Please Input Website Title','class' => 'validate']) }}
                                    {{ Form::label('website_title', 'Website Title') }}
                                    <small>The title will show on the browser title and would be use internally.</small>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">assistant</i>
                                    {{ Form::text('brand_name', null, ['placeholder' => 'Please Input Brand Name','class' => 'validate']) }}
                                    {{ Form::label('brand_name', 'Brand Name') }}
                                    <small>The brand name will show on the browser title and would be use internally.</small>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">receipt</i>
                                    {{ Form::text('copyright_text', null, ['placeholder' => 'Please Input Copyright Text','class' => 'validate']) }}
                                    {{ Form::label('copyright_text', 'Copyright Text') }}
                                    <small>The title will show on the browser title and would be use internally.</small>
                                </div>


                            </div>
                        </div>
                        <div id="cron-service" class="tab-content">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">website</i>
                                    {{ Form::text('cron_url', null, ['placeholder' => 'http://website.com/cron.php','class' => 'validate']) }}
                                    {{ Form::label('cron_url', 'Cron Service Url') }}
                                    <small>The title will show on the browser title and would be use internally.</small>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s4 m3 l2"></div>
                <div class="input-field col s12 m6">
                    {{ Form::submit('Save Setting', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
