@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmAppActivity">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <h4 class="table-title center-align">{{ $title }}</h4>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                <pre>


/***************************** Installation: ****************************/

=> Firstly download the zip file and extract in the root directory.
=> Create a mysql database and import the db.sql file in your created database
=> Open .env file and change these setting as your database configuration:
DB_DATABASE=your_database
DB_USERNAME=db_username
DB_PASSWORD=db_password
=> Consider to host your app in your Main/Sub domain root directory
like: http://crm.website.com not http://website.com/crm

/*************************** End - Installation: ***************************/

/*************************** Development: *********************************/

=> For Development please install all dev dependancy with npm command:
npm install
composer install

// For Update Laravel Vendor Version use followting command:
composer update


// There are 2 type of output html in view(Development Mode and Production Mode)
you will get this setting in "config/view.php" file.
'paths' => [
          realpath(base_path('resources/views')),
//      realpath(base_path('resources/views/prod')),
    ],

First one for developmend mode and 2nd one for production mode, to go production
just comment 1st line and uncomment 2nd line also use this command to generate production view.
gulp --production
So that after running this command all view folder's file will compress and minify
& copy to "prod" folder.

// Changing Mail API
We used mailgun api for all email functionality, change your config here
config/mailgun.php
config/services.php

// Changing PDF configuration
We used dompdf for all pdf functionality, you can change your configuration here,
config/dompdf.php

// For changing url you can change by editing web.php
route/web.php

To change the functionality vew type you can navigate the controllers and change functionality
app/Http/Controllers/

To change the veiw/output html you can navigate the view files and change view
resources/view

/******************************** End - Development *********************************/
                </pre>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
