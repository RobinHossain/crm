@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">




                <div class="row">
                    <div class="col s12">
                        <div class="header">
                            <h5 class="title center-align">{{ $title }}</h5>
                        </div>
                    </div>
                </div>

                <hr class="top_title">

            <div class="row full-width right-align">

                    <a href="{{ url('transaction/income/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon tooltipped" data-position="bottom" data-delay="50" data-tooltip="Add a new income entry on the current list"><i class="fa fa-plus-circle white-text" aria-hidden="true"></i> Add Income</a>
                    <a href="{{ url('transaction/expense/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon tooltipped" data-position="bottom" data-delay="50" data-tooltip="Add a new expense entry on the current list"><i class="fa fa-plus-circle white-text" aria-hidden="true"></i> Add Expense</a>
                    <a href="{{ url('transaction/transfer/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon tooltipped" data-position="bottom" data-delay="50" data-tooltip="Add a new Transfer entry on the current list"><i class="fa fa-plus-circle white-text" aria-hidden="true"></i> Add Transfer</a>

            </div>
                <section>
                    <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Dr.</td>
                            <td>Cr.</td>
                            <td>Amount</td>
                            <td>Type</td>
                            <td>Date</td>
                            <td>Memo</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>

                        @if($transactions)

                        @foreach ($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->id }}</td>
                                <td>@if($transaction->tfor) {{ $transaction->tfor->fname }} {{ $transaction->tfor->lname }} @endif</td>
                                <td>@if($transaction->tfrom) {{ $transaction->tfrom->fname }} {{ $transaction->tfrom->lname }} @endif</td>
                                <td>{{ $transaction->amount }}</td>
                                <td>{{ $transaction->type->name }}</td>
                                <td> {{ date('F d, Y', strtotime($transaction->date)) }}</td>
                                <td>{{ $transaction->memo }}</td>
                                <td>
                                    <a href="#!" onclick="editTrnsaction({{$transaction->id}})"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="#!" onclick="deleteConfirmation({{$transaction->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        @endif

                        </tbody>
                    </table>
                </section>




        </div>


        <!-- For Edit Modal --->

        <div id="editTransaction" class="modal">
            <div class="modal-content">
                <h4 class="center-align">Edit Transaction</h4>
                <hr class="top_title">
                <div class="row">

                    {!! Form::open(array('class' => '', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/edit')) !!}

                    {{ csrf_field() }}

                    <div class="row">


                        <input type="hidden" name="id" id="trnasactionId" value="">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">date_range</i>  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Due Date
                            {{ Form::date('date', '', ['id' => 'TransactionDate', 'class' => 'datepicker', 'placeholder' => 'Due Date']) }}

                        </div>

                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('memo', '', ['id' => 'TransactionMemo', 'class' => 'materialize-textarea', 'row' => 1]) }}
                        </div>


                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Edit Transaction', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>

                </div>
            </div>
        </div>

        <!-- For Edit Modal --->

        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p class="card-title center-align">Are you sure you want to delete this client?</p>
                <ul class="cd-buttons">
                    <li><a href="#!" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Yes</a></li>
                    <li><a href="#!" class="client-popup-close popUpClose">No</a></li>
                </ul>
                <a href="#0" class="cd-popup-close img-replace popUpClose"></a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <form id="delete-form" action="{{ url('/transaction/delete') }}" method="POST"  autocomplete="off" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id" id="deleteOrderId">
        </form>

    </div>
@endsection

@section('scripts')

    <script>
        function deleteConfirmation(id) {
            $('#deleteOrderId').val(id);
            $('.cd-popup').addClass('is-visible');
        }

        function editTrnsaction(id) {
            $.get( "json/transaction/edit/"+id, function( data ) {
                $('#editTransaction').openModal({dismissible:!0,opacity:.5,in_duration:300,out_duration:200,ready:function(){},complete:function(){}});
                $("#trnasactionId").val(id);
                $("#TransactionDate").val(data.date);
                $("#TransactionMemo").val(data.memo);
            });
        }
    </script>

@stop

