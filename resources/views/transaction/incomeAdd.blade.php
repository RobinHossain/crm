@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off')) !!}
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">date_range</i>
                        {{ Form::date('date', '', ['class' => 'datepicker']) }}
                    </div>
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">polymer</i>
                        {{ Form::text('amount', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('amount', 'Amount') }}
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">account_circle</i>
                        {{ Form::select('from', $accounts, null, ['placeholder' => 'Income From']) }}
                    </div>

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">account_circle</i>
                        {{ Form::select('for', $accounts, null, ['placeholder' => 'Target Account']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m12">
                        <i class="material-icons prefix">comment</i>
                        {{ Form::textarea('memo', '', ['class' => 'materialize-textarea', 'row' => 1]) }}
                        {{ Form::label('memo', 'Memo') }}
                    </div>

                </div>


                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Add Income Entry', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>

            <div class="row">
                <div class="col s12">
                    <h5 class="card-title center-align">List of income entry</h5>
                </div>
            </div>


            <hr class="top_title">

            <div class="row">

                <section>
                    <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Dr.</td>
                            <td>Cr.</td>
                            <td>Amount</td>
                            <td>Date</td>
                            <td>Memo</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->id }}</td>
                                <td>@if($transaction->tfor) {{ $transaction->tfor->fname }} {{ $transaction->tfor->lname }} @endif</td>
                                <td>@if($transaction->tfrom) {{ $transaction->tfrom->fname }} {{ $transaction->tfrom->lname }} @endif</td>
                                <td>{{ $transaction->amount }}</td>
                                <td> {{ date('F d, Y', strtotime($transaction->date)) }}</td>
                                <td>{{ $transaction->memo }}</td>
                                <td>
                                    <a href="#!"><i class="Tiny material-icons" ng-click="editTransaction({{ $transaction->id }})">mode_edit</i></a>
                                    <a href="#!"><i class="Tiny material-icons" ng-click="deleteTransaction({{ $transaction->id }})">delete</i></a>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </section>
            </div>

        </div>
        <br><br>

        @if(isset($transaction))
                <!-- Model For Edit -->
        <div id="editTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/edit/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">perm_contact_calendar</i>
                            {{ Form::date('date', $transaction->date, ['class' => 'datepicker']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('memo', $transaction->memo, ['class' => 'materialize-textarea', 'row' => 1]) }}
                            {{ Form::label('memo', 'Memo') }}
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Edit * End -->

        <!-- Model For Delete -->
        <div id="deleteTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/delete/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <h4>Are You Sure ?</h4>
                            <p>You are about to delete the transaction, If you are ready to delete then click the yes button.</p>
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Yes', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Delete * End -->

        @endif

    </div>
@endsection

