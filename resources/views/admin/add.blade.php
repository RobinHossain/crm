@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

                <div class="row">
                    @if (count($errors) > 0)
                        <div class="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <div id="card-alert" class="card red">
                                        <div class="card-content white-text">
                                            <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                        </div>
                                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        </button>
                                    </div>
                                @endforeach
                            </ul>
                        </div>
                    @endif



                </div>

                <div class="row">
                    <div class="col s12">
                        <div class="header">
                            <h3 class="title">{{ $title }}</h3>

                        </div>
                    </div>
                </div>

            <hr class="top_title">

                <div class="row">
                        {!! Form::open(array('class' => 'col s12', 'role' => 'form', 'files' => true, 'method' => 'POST', 'autocomplete' => 'off')) !!}
                        {{ csrf_field() }}


                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_box</i>
                                {{ Form::text('fname', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                                {{ Form::label('fname', 'First Name') }}
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_box</i>
                                {{ Form::text('lname', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                                {{ Form::label('lname', 'Last Name') }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">perm_identity</i>
                                {{ Form::text('username', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                                {{ Form::label('username', 'Username') }}
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">email</i>
                                {{ Form::text('email', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                                {{ Form::label('email', 'Email Address') }}
                            </div>
                        </div>

                        <div class="row">

                            <div class="file-field input-field col s12 m12">
                                <div class="btn">
                                    <span>Account Photo</span>
                                    {{ Form::file('photo') }}
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="pay_address" type="text"
                                           placeholder="Upload Account Image/Photo">
                                </div>

                            </div>
                        </div>

                        <div class="row">

                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">group</i>
                                {{ Form::select('group_id', $user_roles, null, ['placeholder' => 'Select Custom Group']) }}
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">lock</i>
                                {{ Form::password('password', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                                {{ Form::label('password', 'Password') }}
                            </div>
                        </div>


                        <div class="row">

                            <div class="input-field col s12 m6">
                                {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                            </div>
                        </div>


                    </form>
                </div>




        </div>
    </div>
@endsection
