@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif


            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h4 class="title center-align">{{ $title }}</h4>

                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {{--<form class="col s12" id="account-form" action="{{ url('/administrator/role/add') }}" method="POST"  autocomplete="off">--}}

				<?php if(isset( $role ) && ! empty( $role )){ ?>
                {!! Form::model($role, array('class' => '', 'role' => 'form', 'files' => true)) !!}
				<?php }else{?>
                {!! Form::open(array('method' => 'POST', 'class' => '', 'role' => 'form', 'autocomplete' => 'off')) !!}
				<?php } ?>
                {{ csrf_field() }}


                <div class="row">
                    <div class="input-field col s12 m6 l6">
                        <i class="material-icons prefix">supervisor_account</i>
                        {{ Form::text('name', null, ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('name', 'Role Name') }}
                    </div>
                    <div class="input-field col s12 m6 l6">
                        <i class="material-icons prefix">supervisor_account</i>
                        {{ Form::text('slug', null) }}
                        {{ Form::label('slug', 'Slug') }}
                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_client" class="filled-in"
                               name="manage_client" @if (isset($role->manage_client)){{ $role->manage_client == 1?'checked':'' }} @endif />
                        <label for="manage_client">Manage Client</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_order_invoice" class="filled-in"
                               name="manage_order_invoice" @if (isset($role->manage_order_invoice)){{ $role->manage_order_invoice == 1?'checked':'' }} @endif />
                        <label for="manage_order_invoice">Manage Order and Invoice</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_support" class="filled-in"
                               name="manage_support" @if (isset($role->manage_support)){{ $role->manage_support == 1?'checked':'' }} @endif />
                        <label for="manage_support">Manage Support</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_finance" class="filled-in"
                               name="manage_finance" @if (isset($role->manage_finance)){{ $role->manage_finance == 1?'checked':'' }} @endif />
                        <label for="manage_finance">Manage Finance</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_product" class="filled-in"
                               name="manage_product" @if (isset($role->manage_product)){{ $role->manage_product == 1?'checked':'' }} @endif />
                        <label for="manage_product">Manage Product</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <input type="checkbox" id="manage_admin_option" class="filled-in"
                               name="manage_admin_option" @if (isset($role->manage_admin_option)){{ $role->manage_admin_option == 1?'checked':'' }} @endif />
                        <label for="manage_admin_option">Manage Administrative Options</label>
                    </div>
                </div>

                <br>


                <div class="row">

                    <div class="input-field col s12 m12">
                        {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
    </div>
@endsection
