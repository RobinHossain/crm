@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                <div id="admin" class="col s12">
                    <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">{{ $title }}</span>
                            <div class="actions">
                                <a href="{{url('ticket/add')}}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon"><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Add new</a>
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Client</td>
                                <td>Subject</td>
                                <td>Category</td>
                                <td>Status</td>
                                <td>Last Activity</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($tickets as $ticket)
                                <tr>
                                    <td>{{ $ticket->id }}</td>
                                    <td>{{ $ticket->account->fname }} {{ $ticket->account->lname }}</td>
                                    <td>{{ $ticket->subject }}</td>
                                    <td>{{ $ticket->category->name }}</td>
                                    <td>{{ $ticket->status == 1 ? "Open" : "Close" }}</td>
                                    <td>{{ $ticket->updated_at->format('M d Y') }}</td>
                                    <td>
                                        <a href="{{url('ticket/edit')}}/{{$ticket->id}}" class="waves-effect waves-light btn light-blue lighten-1 white-text tiny"><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Edit</a>
                                        <a onclick="event.preventDefault();document.getElementById('product-delete-form').submit();" class="waves-effect waves-light btn light-blue lighten-1 white-text tiny"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>

                                        <form id="product-delete-form" action="{{ url('ticket/delete') }}" method="POST"  autocomplete="off"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="product_id" value={{$ticket->id}}>
                                            <input type="hidden" name="status" value="delete">
                                        </form>

                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
