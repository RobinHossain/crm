@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col s12">
                <h5 class="card-title center-align">{{ $title }}</h5>

            </div>
        </div>


        <hr class="top_title">


        <div class="section">

            <div class="row">
                <div class="col m12">
                    @foreach ($histories as $history)
                    <div class="ticket_history_box">
                        <div class="user-info">
                            <div class="user_profile_box"><?php echo substr($history->message_from,0,1); ?></div>
                            <div class="user_name_time">
                                <strong>{{ $history->message_from }}</strong><br>
                                <small>{{ $history->date }}</small>
                            </div>
                        </div>
                        <div class="history-info">
                            {{ $history->message }}
                        </div>
                        @if($history->file)
                        <div class="history-file">
                            <a href="{{ $client_panel }}/imgs/tickets/{{ $history->file }}" download target="_blank">{{ $history->file }}</a>
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12">
                    <form class="col s12" id="message-form" action="{{ url('/ticket/reply/message') }}/{{ $history_id }}" method="POST"  autocomplete="off">
                        {{ csrf_field() }}
                        <h6 class="reply">Reply Now</h6>
                        <hr class="top_title">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('reply', '', ['class' => 'materialize-textarea', 'row' => 1]) }}
                            {{ Form::label('reply', 'Message') }}
                        </div>

                        <button type="submit" class="waves-effect waves-light btn light-blue lighten-1 white-text"> &nbsp; Reply
                        </button>
                    </form>
                </div>
            </div>



        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
