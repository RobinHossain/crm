@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'files'=>true, 'enctype'=>"multipart/form-data" )) !!}
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 m4">
                        <i class="material-icons prefix">account_circle</i>
                        {{ Form::select('ticket_for', $accounts, null, ['placeholder' => 'Select Client']) }}
                        {{ Form::label('ticket_for', 'Ticket For') }}
                    </div>
                    <div class="input-field col s12 m8">
                        <i class="material-icons prefix">label</i>
                        {{ Form::text('subject', '', ['placeholder' => 'Need help for product, hosting help etc.', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('subject', 'Ticket Subject') }}
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m4">
                        <i class="material-icons prefix">toc</i>
                        {{ Form::select('category', $ticket_categories, null, ['placeholder' => 'Select Category']) }}
                        {{ Form::label('category', 'Category') }}
                    </div>



                    <div class="file-field input-field col s12 m5">
                        <div class="btn">
                            <span>Upload Attachment</span>
                            {{ Form::file('file') }}
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" name="pay_address" type="text"
                                   placeholder="Upload if need to attach file/image">
                        </div>

                    </div>

                    <div class="input-field col s12 m2">
                        {{ Form::checkbox('status', null, false, ['id' => 'status'] ) }}
                        {{ Form::label('status', null, ['class' => 'control-label']) }}
                    </div>

                </div>


                <div class="row">
                    <div class="input-field col s12 m12">
                        <i class="material-icons prefix">comment</i>
                        {{ Form::textarea('message', '', ['class' => 'materialize-textarea', 'row' => 1]) }}
                        {{ Form::label('message', 'Message') }}
                    </div>

                </div>


                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Add Ticket', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
        <br><br>

        @if(isset($transaction))
                <!-- Model For Edit -->
        <div id="editTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/edit/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">perm_contact_calendar</i>
                            {{ Form::date('date', $transaction->date, ['class' => 'datepicker']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('memo', $transaction->memo, ['class' => 'materialize-textarea', 'row' => 1]) }}
                            {{ Form::label('memo', 'Memo') }}
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Edit * End -->

        <!-- Model For Delete -->
        <div id="deleteTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/delete/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <h4>Are You Sure ?</h4>
                            <p>You are about to delete the transaction, If you are ready to delete then click the yes button.</p>
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Yes', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Delete * End -->

        @endif

    </div>
@endsection
