@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off')) !!}
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">toc</i>
                        {{ Form::text('name', '', ['placeholder' => 'like computer, website, service etc', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('name', 'Category Name') }}
                    </div>

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">label_outline</i>
                        {{ Form::text('slug', '', ['placeholder' => 'slug with small letter, without space and special character', 'aria-required' => 'true']) }}
                        {{ Form::label('slug', 'Slug') }}
                    </div>

                </div>



                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Add Category', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
        <br><br>


    </div>
@endsection

@section('scripts')

    <script type="text/javascript" src="{{url('/js/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/ang_app.js')}}"></script>
@stop