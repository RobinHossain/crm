@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off')) !!}
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">label</i>
                        {{ Form::text('title', '', ['placeholder' => 'any topics like how, what is the best way', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('title', 'Article Title') }}
                    </div>
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">view_quilt</i>
                        {{ Form::select('category_id', $categories, null, ['placeholder' => 'Select Category']) }}
                        {{ Form::label('category_id', 'Category') }}
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">perm_identity</i>
                        {{ Form::select('author_id', $authors, null, ['placeholder' => 'Select an Author/Writer']) }}
                        {{ Form::label('author_id', 'Author/Writer') }}
                    </div>
                    <div class="input-field col s12 m6">
                        {{ Form::checkbox('status', null, false, ['id' => 'status'] ) }}
                        {{ Form::label('status', null, ['class' => 'control-label']) }}
                    </div>

                </div>


                <div class="row">
                    <div class="col s12 m12">
                        <h5 class="left-align">Add Article Content</h5>
                        {{ Form::textarea('content', '', ['id' => 'content']) }}
                        {{ Form::label('content', 'Article Content') }}
                    </div>

                </div>



                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Add Article', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
        <br><br>


    </div>
@endsection

@section('scripts')
    <script src="{{url('/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{url('/js/page/article.js')}}"></script>
@stop