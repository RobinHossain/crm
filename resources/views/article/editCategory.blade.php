@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                <?php if(isset($category) && !empty($category)){ ?>
                {!! Form::model($category, array('class' => '', 'role' => 'form', 'files' => true)) !!}
                <?php }else{?>
                {!! Form::open(array('class' => '', 'role' => 'form')) !!}
                <?php } ?>
                {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">toc</i>
                            {{ Form::text('name', null, ['placeholder' => 'like computer, website, service etc', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('name', 'Category Name') }}
                        </div>


                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">label_outline</i>
                            {{ Form::text('slug', null, ['placeholder' => 'slug with small letter, without space and special character', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('slug', 'Slug') }}
                        </div>

                    </div>


                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Update Category', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
        <br><br>

        @if(isset($transaction))
                <!-- Model For Edit -->
        <div id="editTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/edit/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">perm_contact_calendar</i>
                            {{ Form::date('date', $transaction->date, ['class' => 'datepicker']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('memo', $transaction->memo, ['class' => 'materialize-textarea', 'row' => 1]) }}
                            {{ Form::label('memo', 'Memo') }}
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Edit * End -->

        <!-- Model For Delete -->
        <div id="deleteTransaction" class="modal">
            <div class="modal-content">
                <div class="row">
                    {!! Form::open(array('id' => 'transaction-form','class' => 'col s12', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'transaction/delete/'.$transaction->id)) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <h4>Are You Sure ?</h4>
                            <p>You are about to delete the transaction, If you are ready to delete then click the yes button.</p>
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Yes', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        </div>
        <!-- Model For Delete * End -->

        @endif

    </div>
@endsection

@section('scripts')

    <script type="text/javascript" src="{{url('/js/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/ang_app.js')}}"></script>
@stop