@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row">
            <div class="col s12">
                <h5 class="card-title center-align">{{ $title }}</h5>

            </div>
        </div>


        <hr class="top_title">

        <div class="row right-align">
            {{--<a href="{{ url('clients/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i class="fa fa-user-plus" aria-hidden="true"></i> Add a new client</a>--}}
            <a href="{{url('article/add')}}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon"><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Add new</a>

        </div>

        <div class="section">

            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Title</td>
                    <td>Category</td>
                    <td>Status</td>
                    <td>Created</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($articles as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->title }}</td>
                        <td>@if(isset($article->category->name)){{ $article->category->name }}@endif</td>
                        <td>{{ $article->status == 1 ? "Published" : "Draft" }}</td>
                        <td>{{ $article->updated_at->format('M d Y') }}</td>
                        <td>
                            <a href="{{url('article/edit')}}/{{$article->id}}" ><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Edit</a>
                            <a href="{{url('article/delete')}}/{{$article->id}}"><i class="fa fa-trash-o white-text" aria-hidden="true"></i> Delete</a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>



        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
