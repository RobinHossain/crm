@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif


            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h3 class="title">{{ $title }} ({{ $order->status }})</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m12">
                    <a class="btn waves-effect light-blue lighten-1"
                       href="{{ url('/order/manage/accept') }}"
                       onclick="event.preventDefault();document.getElementById('order-accept-form').submit();"><i
                                class="fa fa-pencil-square-o"
                                aria-hidden="true"></i> Accept</a>
                    <a class="btn waves-effect light-blue lighten-1"
                       href="{{ url('/order/manage/cancel') }}"
                       onclick="event.preventDefault();document.getElementById('order-cancel-form').submit();"><i
                                class="fa fa-exclamation"
                                aria-hidden="true"></i> Cancel</a>
                    <a class="btn waves-effect light-blue lighten-1"
                       href="{{ url('/order/manage/pending') }}"
                       onclick="event.preventDefault();document.getElementById('order-pending-form').submit();"><i
                                class="fa fa-exclamation"
                                aria-hidden="true"></i> Pending</a>
                    <a class="btn waves-effect light-blue lighten-1"
                       href="{{ url('/order/manage/fraud') }}"
                       onclick="event.preventDefault();document.getElementById('order-fraud-form').submit();"><i
                                class="fa fa-exclamation"
                                aria-hidden="true"></i> Fraud</a>
                    <a class="btn waves-effect light-blue lighten-1"
                       href="#!"
                       onclick="deleteConfirmation({{$order->id}})"><i
                                class="fa fa-trash"
                                aria-hidden="true"></i> Delete</a>
                </div>

                <!-- Hidde Post Forms For Change Order Status -->
                <form id="order-accept-form" action="{{ url('/order/manage/accept') }}" method="POST"  autocomplete="off"
                      style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value={{$order->id}}>
                    <input type="hidden" name="status" value="active">
                </form>
                <form id="order-cancel-form" action="{{ url('/order/manage/cancel') }}" method="POST"  autocomplete="off"
                      style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value={{$order->id}}>
                    <input type="hidden" name="status" value="cancel">
                </form>
                <form id="order-fraud-form" action="{{ url('/order/manage/fraud') }}" method="POST"  autocomplete="off"
                      style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value={{$order->id}}>
                    <input type="hidden" name="status" value="fraud">
                </form>
                <form id="order-pending-form" action="{{ url('/order/manage/pending') }}" method="POST"  autocomplete="off"
                      style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value={{$order->id}}>
                    <input type="hidden" name="status" value="pending">
                </form>

                <!-- Post Forms For Change Order Status -->
            </div>

            <hr class="top_title">
                <?php echo Form::model($order, ['url' => 'order/save_note', 'method' => 'POST', 'autocomplete' => 'off']); ?>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col s12 m4">
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <h5>Order Number : {{ $order->id }}</h5>

                        <p>
                            <span class="title">Order Date :</span> {{ $order->created_at }}<br>
                            <span class="title">Order Status :</span> {{ $order->status }}<br>
                        </p>
                        <h5>Products/Services:</h5>
                        <table class="bordered">
                            <thead>
                            <tr>
                                <th data-field="id">ID</th>
                                <th data-field="name">Name</th>
                                <th data-field="price">Item Price</th>
                                <th data-field="price">Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product['id'] }}</td>
                                    <td>@if(isset($product['name'])) {{ $product['name'] }} @else Product Deleted @endif</td>
                                    <td>@if(isset($product['price'])) {{ $product['price'] }} @else Product Deleted @endif</td>
                                    <td>{{ $product['quantity'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <h5 class="border-top-dotted padding-top-20">Order Invoice Price= {{ $order->amount }}</h5>
                    </div>
                    <div class="col s12 m8">
                        <?php echo Form::textarea('note', null, ['id' => 'note']); ?><br>
                        <button type="submit" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i
                                    class="fa fa-floppy-o" aria-hidden="true"></i> &nbsp; Save Note
                        </button>
                    </div>
                </div>
            </form>


            <div class="cd-popup" role="alert">
                <div class="cd-popup-container">
                    <p class="card-title center-align">Are you sure you want to delete this client?</p>
                    <ul class="cd-buttons">
                        <li><a href="#!" onclick="event.preventDefault();document.getElementById('order-delete-form').submit();">Yes</a></li>
                        <li><a href="#!" class="client-popup-close">No</a></li>
                    </ul>
                    <a href="#0" class="cd-popup-close img-replace"></a>
                </div> <!-- cd-popup-container -->
            </div> <!-- cd-popup -->
            <form id="order-delete-form" action="{{ url('/order/manage/delete') }}" method="POST"  autocomplete="off"
                  style="display: none;">
                {{ csrf_field() }}
                <input type="hidden" name="order_id" value="{{$order->id}}">
                <input type="hidden" name="status" value="delete">
            </form>


        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{url('/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{url('/js/page/order.js')}}"></script>
    <script>

        function deleteConfirmation(clientId) {
//            $('#deleteClientId').val(clientId);
            $('.cd-popup').addClass('is-visible');
        }

    </script>
@stop
