@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif


            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h3 class="title">{{ $title }}</h3>


                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                <form class="col s12" id="account-form" action="{{ url('/order/add_confirm') }}" method="POST"  autocomplete="off">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 m6">
                            {{ Form::select('customer', $customers, null, ['placeholder' => 'Select A Client' ,'required'=> '', 'aria-required' => 'true']) }}
                        </div>
                        <div class="input-field col s12 m6">
                            {{ Form::select('products[]', $products, null, ['multiple' => true, 'class' => 'validate multiple', 'required'=> '', 'aria-required' => 'true']) }}
                        </div>
                    </div>

                    <div class="row">

                    </div>

                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                </form>
            </div>


        </div>
    </div>
@endsection


