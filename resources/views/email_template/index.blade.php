@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">

        <div class="row">
            <div class="col s12">
                <h5 class="card-title center-align">{{ $title }}</h5>

            </div>
        </div>


        <hr class="top_title">


        <div class="section">
            <table id="table" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Template Name</td>
                    <td>Subject</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($templates as $template)
                    <tr>
                        <td>{{ $template->id }}</td>
                        <td>{{ $template->name }}</td>
                        <td>{{ $template->subject }}</td>
                        <td>{{ $template->status == 1? "Active" : "Inactive" }}</td>


                        <td>
                            <a href="{{ url('template/manage') }}/{{ $template->id }}" class="waves-effect waves-light btn light-blue lighten-1 white-text tiny"><i class="fa fa-pencil-square-o white-text"></i> Manage</a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>


        </div>
        <br><br>


    </div>
@endsection
