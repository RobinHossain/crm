@extends('layouts.app')

@section('css')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ url('/css/page/home.css') }}" type="text/css" media="all" />
@stop

@section('content')
    <div class="container">

        <div class="section">
            <h5 class="title center-align">{{ $title }}</h5>
            <hr class="top_title">
            <div id="chartdiv"></div>
        </div>
        <br><br>


    </div>
@endsection

@section('scripts')

    <script src="{{ url('js/chart/amcharts.js') }}"></script>
    <script src="{{ url('js/chart/serial.js') }}"></script>
    <script src="{{ url('js/chart/export.min.js') }}"></script>
    <script src="{{ url('js/chart/light.js') }}"></script>
    <script src="{{ url('js/page/charts.js') }}"></script>
    <script>
        var revenue = <?php echo $revenue; ?>;
        CRM.charts.lineChart('chartdiv', revenue);

    </script>
@stop