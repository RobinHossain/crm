@extends('layouts.app')

@section('content')

    <div class="container">
        <script>
            var id = <?php echo $id; ?>;
        </script>
        <div class="section" ng-app="crmApplication">

            <div class="row">
                <div id="admin" class="col s12">
                    <h4 class="table-title">{{ $title }}</h4>
                </div>
            </div>

            <div class="row" ng-controller="accActivityCtrl">
                <div class="tabs-vertical">
                    <div class="col s12 m3 l3">
                        <ul class="tabs">
                            <li class="tab"><a class="active" href="#profile" ng-click="showAccountInfo()">Profile</a></li>
                            <li class="tab"><a href="#transactions" ng-click="showTransactionInfo()">Transactions</a></li>
                            <li class="tab"><a href="#orders" ng-click="showOrderInfo()">Orders</a></li>
                            <li class="tab"><a href="#invoices" ng-click="showInvoiceInfo()">Invoices</a></li>
                            <li class="tab"><a href="#tickets" ng-click="showTicketInfo()">Tickets</a></li>
                        </ul>
                    </div>
                    <div class="col s12 m9 l9">
                        <div id="profile" class="tab-content">
                            <div class="row">
                                <div class="account col s12 m12 l12">
                                    <h4>Account Id: <span ng-bind="user.id"></span> </h4>
                                    <h4>Name: <span ng-bind="user.name"></span> </h4>
                                    <h4>Company: <span ng-bind="user.company"></span></h4>
                                    <p ng-bind="user.city"></p>
                                    <p ng-bind="user.state"></p>
                                    <p ng-bind="user.address"></p>
                                    <p ng-bind="user.phone"></p>
                                    <h5>Registered Email Address: <span ng-bind="user.email"></span></h5>
                                    <h5>Account Credit: <span ng-bind="user.balance"></span></h5>

                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m3">
                                    <a href="#!" class="btn waves-effect light-blue lighten-1">Add Fund</a>
                                </div>
                                <div class="input-field col s12 m3">
                                    <a href="#!" class="btn waves-effect light-blue lighten-1">Send Email</a>
                                </div>
                                <div class="input-field col s12 m3">
                                    <a href="{{ url('clients/edit') }}/{{ $id }}" class="btn waves-effect light-blue lighten-1">Edit</a>
                                </div>
                                <div class="input-field col s12 m3">
                                    <a href="#!" onclick="deleteConfirmation({{$id}})" class="btn waves-effect light-blue lighten-1">Delete</a>
                                </div>
                            </div>

                        </div>
                        <div id="transactions" class="tab-content">
                            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Dr.</td>
                                    <td>Cr.</td>
                                    <td>Amount</td>
                                    <td>Type</td>
                                    <td>Date</td>
                                    <td>Memo</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>


                                <tr ng-repeat="transaction in transactions">
                                    <td><% transaction.id %></td>
                                    <td><% transaction.from %></td>
                                    <td><% transaction.for %></td>
                                    <td><% transaction.amount %></td>
                                    <td><% transaction.type %></td>
                                    <td><% transaction.date %></td>
                                    <td><% transaction.memo %></td>
                                    <td>
                                        <a href="{{ url('transaction/edit') }}/<% transaction.id %>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="{{ url('transaction/delete') }}/<% transaction.id %>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>



                                </tbody>
                            </table>
                        </div>
                        <div id="orders" class="tab-content">
                            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>Order ID</td>
                                    <td>Amount</td>
                                    <td>Status</td>
                                    <td>Total Items</td>
                                    <td>Date</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>


                                <tr ng-repeat="order in orders">
                                    <td><% order.id %></td>
                                    <td><% order.amount %></td>
                                    <td><% order.status %></td>
                                    <td><% order.total_item %></td>
                                    <td><% order.created %></td>

                                    <td>
                                        <a href="{{ url('order/manage') }}/<% order.id %>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>

                                <tr class="odd" ng-if="orders == false"><td valign="top" colspan="7" class="dataTables_empty">No order for this Account</td></tr>



                                </tbody>
                            </table>
                        </div>
                        <div id="invoices" class="tab-content">
                            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>Invoice ID</td>
                                    <td>Amount</td>
                                    <td>Status</td>
                                    <td>Due Date</td>
                                    <td>Invoice Created</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>


                                <tr ng-repeat="invoice in invoices">
                                    <td><% invoice.id %></td>
                                    <td><% invoice.amount %></td>
                                    <td><% invoice.status %></td>
                                    <td><% invoice.due_date %></td>
                                    <td><% invoice.created %></td>

                                    <td>
                                        <a href="{{ url('invoice/edit') }}/<% invoice.id %>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="{{ url('invoice/delete') }}/<% invoice.id %>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>

                                <tr class="odd" ng-if="invoices == false"><td valign="top" colspan="7" class="dataTables_empty">No invoice created for this Account</td></tr>



                                </tbody>
                            </table>
                        </div>
                        <div id="tickets" class="tab-content">
                            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>Ticket ID</td>
                                    <td>Subject</td>
                                    <td>Message</td>
                                    <td>Date</td>

                                </tr>
                                </thead>
                                <tbody>


                                <tr ng-repeat="ticket in tickets">
                                    <td><% ticket.id %></td>
                                    <td><% ticket.subject %></td>
                                    <td><% ticket.message %></td>
                                    <td><% ticket.date %></td>

                                </tr>

                                <tr class="odd" ng-if="tickets == false"><td valign="top" colspan="7" class="dataTables_empty">No ticket created for this Account</td></tr>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <br><br>

        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p class="card-title center-align">Are you sure you want to delete this transaction?</p>
                <ul class="cd-buttons">
                    <li><a href="#!" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Yes</a></li>
                    <li><a href="#!" class="client-popup-close popUpClose">No</a></li>
                </ul>
                <a href="#0" class="cd-popup-close img-replace popUpClose"></a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <form id="delete-form" action="{{ url('/clients/delete') }}" method="POST"  autocomplete="off" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id" id="deleteOrderId">
        </form>

    </div>
@endsection

@section('scripts')

    <script>
        function deleteConfirmation(id) {
            $('#deleteOrderId').val(id);
            $('.cd-popup').addClass('is-visible');
        }
    </script>

    <script type="text/javascript" src="{{url('/js/angular/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/angular/app.js')}}"></script>

@stop
