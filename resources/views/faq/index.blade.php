@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row">
            <div class="col s12">
                <h5 class="card-title center-align">{{ $title }}</h5>

            </div>
        </div>


        <hr class="top_title">

        <div class="row right-align">
            {{--<a href="{{ url('clients/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i class="fa fa-user-plus" aria-hidden="true"></i> Add a new client</a>--}}
            <a href="{{url('faq/add')}}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon"><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Add new</a>

        </div>

        <div class="section">

            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Title</td>
                    <td>Content</td>
                    <td>Created</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($faqs as $faq)
                    <tr>
                        <td>{{ $faq->id }}</td>
                        <td>{{ $faq->title }}</td>
                        <td>{{ $faq->content }}</td>
                        <td>{{ $faq->created_at }}</td>
                        <td>
                            <a href="{{url('faq/edit')}}/{{$faq->id}}" class=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="{{url('faq/delete')}}/{{$faq->id}}" class=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>





        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
