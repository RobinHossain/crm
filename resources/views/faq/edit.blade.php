@extends('layouts.app')

@section('content')
    <div class="container" ng-app="crmApp">
        <div class="section">

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h5 class="title center-align">{{ $title }}</h5>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                <?php if(isset($faq) && !empty($faq)){ ?>
                {!! Form::model($faq, array('class' => '', 'role' => 'form', 'files' => true, 'autocomplete' => 'off')) !!}
                <?php }else{?>
                {!! Form::open(array('class' => '', 'role' => 'form')) !!}
                <?php } ?>
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">label</i>
                        {{ Form::text('title', null, ['placeholder' => 'any topics like how, what is the best way', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        {{ Form::label('title', 'Faq Title') }}
                    </div>
                    
                </div>

               
                <div class="row">

                    <div class="input-field col s12 m12">
                        <i class="material-icons prefix">comment</i>
                        {{ Form::textarea('content', null, ['id' => 'content', 'class'=>'materialize-textarea']) }}
                        {{ Form::label('content', 'Edit Faq Content') }}
                    </div>

                </div>



                <div class="row">

                    <div class="input-field col s12 m6">
                        {{ Form::submit('Edit Faq', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                    </div>
                </div>


                </form>
            </div>


        </div>
        <br><br>

    </div>
@endsection

