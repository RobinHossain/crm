@extends('layouts.app')
@section('css')
    <link href="{{ url('/css/page/invoice.css') }}" rel="stylesheet">
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            .noPrint {
                display: none !important;
            }

            #noPrint {
                display: none !important;
            }

            #page.print_area{
                visibility: hidden;
                display: block !important;
            }
            .pay-buttons{
                display: none;
            }
        }

        #page.print_area * {
            visibility: visible;
        }



    </style>
@stop
@section('content')


    <div class="container" ng-app="crmApp">



                <div class="alert alert-info">

                    {{--<strong>View Invoice Link: </strong> <a href="http://demo.bdinfosys.com/bmsapp/cp/invoice.php?_cmd=139&amp;_token=7205367529" target="_blank">http://demo.bdinfosys.com/bmsapp/cp/invoice.php?_cmd=139&amp;_token=7205367529</a> </div>--}}
                <div id="page" class="print_area">

                    <!--To change the status of the invoice, update class name. for example "status paid" or "status overdue". Possible options are: draft, sent, paid, overdue-->
                    <div class="status {{ $invoice->invoice_status_id==2? 'paid': 'unpaid' }}">
                        <p>Paid</p>
                    </div>

                    <div class="logo-invoice">
                        @if($setting->logo)
                            <img src="{{ asset('/imgs') }}/{{ $setting->logo }}" height="80" alt="Logo" class="company-logo">
                        @endif
                    </div>

                    <p class="recipient-address">
                        <strong>Invoiced To-</strong><br>
                        <strong>{{ $invoice->account->fname }} {{ $invoice->account->lname }}</strong><br>
                        <abbr title="Phone">{{ $invoice->account->phone }}</abbr> </p>
                        <abbr title="email">{{ $invoice->account->email }}</abbr> </p>
                    {{ $invoice->account->city }} - {{ $invoice->account->zipcode }}<br>
                        {{ $invoice->account->address_1 }}<br>



                    <h1>Invoice {{ $invoice->id }}</h1>
                    <p class="terms"><strong>Date Created: {{ $invoice->created_at }}</strong><br>
                        <strong>Payment due by {{ $invoice->due_date }}</strong></p>



                    <p class="company-address">
                        {{ $setting->pay_address }}

                    </p>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="30px">S/L</th>
                            <th>Product/Service Description</th>
                            <th>Qty</th>
                            <th width="100px">Amount</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php $invoiceCount = 1; ?>
                        @foreach($invoiceItems as $item)
                            <tr>
                                <td>{{ $invoiceCount }}</td>
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ $item->product->price }}</td>

                            </tr>
                            <?php $invoiceCount++; ?>
                        @endforeach


                        </tbody>
                    </table>

                    <br>

                    <div class="total-due">
                        <div class="total-heading"><p>Amount Due</p></div>
                        <div class="total-amount"><p>{{ $invoice->price }}</p></div>
                    </div>

                    <hr>
                    <div class="pay-buttons">
                            @if($invoice->invoice_status_id==1)
                                <a href="{{ url('invoice/mark_cancel') }}/{{ $invoice->id }}" class="color red button waves-effect waves-light btn lighten-1 white-text medium">Mark Cancelled</a>
                                <a href="{{ url('invoice/mark_paid') }}/{{ $invoice->id }}" class="color green button waves-effect waves-light btn light-blue lighten-1 white-text medium">Mark Paid</a>
                            @elseif($invoice->invoice_status_id==2)
                                <a href="{{ url('invoice/mark_cancel') }}/{{ $invoice->id }}" class="color red button waves-effect waves-light btn lighten-1 white-text medium">Mark Cancelled</a>
                                <a href="{{ url('invoice/mark_unpaid') }}/{{ $invoice->id }}" class="color green button waves-effect waves-light btn light-blue lighten-1 white-text medium">Mark Unpaid</a>
                            @elseif($invoice->invoice_status_id==3)
                                <a href="{{ url('invoice/remove_cancel') }}/{{ $invoice->id }}" class="color red button waves-effect waves-light btn lighten-1 white-text medium">Remove From Cancel</a>
                            @endif
                        <a href="{{ url('invoice/send_email') }}/{{ $invoice->id }}" class="color blue button waves-effect waves-light btn light-blue lighten-1 white-text medium">Send Email</a>
                        <a href="#" id="print_invoice" class="color blue button waves-effect waves-light btn light-blue lighten-1 white-text medium">Print</a>
                        <a href="{{ url('invoice/view_pdf') }}/{{ $invoice->id }}" class="color blue button waves-effect waves-light btn light-blue lighten-1 white-text medium">View PDF</a>
                        <a href="{{ url('invoice/download') }}/{{ $invoice->id }}" class="color blue button waves-effect waves-light btn light-blue lighten-1 white-text medium">Download PDF</a>
                        <a href="{{ url('invoice/edit') }}/{{ $invoice->id }}" class="color red button waves-effect waves-light btn lighten-1 white-text medium">Edit</a>
                        <a onclick="deleteConfirmation({{ $invoice->id }})" href="#!" class="color red button waves-effect waves-light btn lighten-1 white-text medium">Delete</a>
                    </div>

                </div>
                {{--<div class="page-shadow"></div>--}}
            </div>




    </div>
        <!-- Delete Confirmation -->
        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p class="card-title center-align">Are you sure you want to delete this client?</p>
                <ul class="cd-buttons">
                    <li><a href="#!" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Yes</a></li>
                    <li><a href="#!" class="client-popup-close popUpClose">No</a></li>
                </ul>
                <a href="#0" class="cd-popup-close img-replace popUpClose"></a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <form id="delete-form" action="{{ url('/invoice/delete') }}" method="POST"  autocomplete="off" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id" id="deleteID">
        </form>
        <!-- End # Delete Confirmation -->



@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#print_invoice").click(function() {
                window.print();
            });

        });

        function deleteConfirmation(id) {
            $('#deleteID').val(id);
            $('.cd-popup').addClass('is-visible');
        }
    </script>
@stop


