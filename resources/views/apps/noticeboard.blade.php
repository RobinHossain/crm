@extends('layouts.app')

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Aref+Ruqaa" rel="stylesheet">
    <link href="{{url('/css/page/noticeboard.css')}}" rel="stylesheet">
@stop

@section('content')
    <div class="container" ng-app="crmApplication">
        <div class="section" ng-controller="todoCtrl">

            <div class="row">
                <div class="col-md-12">
                    <h4 class="title center-align">All Notice Here </h4>
                </div>
                <div class="col-md-12 right-align">
                    <a class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon modal-trigger"
                       href="#addNotice"><i class="fa fa-pencil-square-o white-text"></i> Add A New Notice</a>
                </div>
            </div>
            <hr class="top_title">

            <div class="row">
                <div class="col-md-12">
                    <section id='frame'>

                        <?php $inc = 0; ?>
                        @foreach($notices as $notice)
                            <?php $inc++; ?>
                            <a class="note <?php if ($inc == 1) {
                                echo 'sticky1';
                            } elseif ($inc == 2) {
                                echo 'sticky2';
                            } elseif ($inc == 3) {
                                echo 'sticky3';
                            } elseif ($inc == 4) {
                                echo 'sticky4';
                            } elseif ($inc == 5) {
                                echo 'sticky5';
                            } elseif ($inc == 6) {
                                echo 'sticky6';
                            } elseif ($inc == 7) {
                                echo 'sticky0';
                            } ?> modal-trigger" onclick="showNoteFunc({{$notice->id}})" href="#viewNotice">
                                <div class='pin'></div>
                                <div class='text'>{{ strlen($notice->notice)>125 ? substr($notice->notice, 0, 125).'....':$notice->notice }}</div>
                            </a>
                            <?php if ($inc == 7) {
                                $inc = 0;
                            } ?>
                        @endforeach

                    </section>

                </div>
            </div>

        </div>

        <!-- Modal For Add a New Notice -->
        <div id="addNotice" class="modal">
            <div class="modal-content">
                <h4 class="center-align">Add a New Notice</h4>
                <hr class="top_title">
                <div class="row">

                    {!! Form::open(array('class' => '', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'notice/add')) !!}

                    {{ csrf_field() }}

                    <div class="row">


                        <div class="input-field col s12 m5">
                            <i class="material-icons prefix">subtitles</i>
                            {{ Form::text('title', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('title', 'Title') }}
                        </div>

                        <div class="input-field col s12 m4">
                            <i class="material-icons prefix">group</i>
                            {{ Form::select('client_id', $clients, null, ['placeholder' => 'For All Clients']) }}
                        </div>


                        <div class="input-field col s12 m3">
                            {{ Form::checkbox('status', null, true, ['id' => 'status'] ) }}
                            {{ Form::label('status', null, ['class' => 'control-label']) }}
                        </div>

                    </div>
                    <div class="row">

                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('notice', '', ['class' => 'materialize-textarea']) }}
                            {{ Form::label('notice', 'Notice') }}
                        </div>


                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Add Notice', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>

                </div>
            </div>
        </div>
        <!-- End Modal For Add a New Notice -->

        <!-- Modal For Edit a  Notice -->
        <div id="editNotice" class="modal">
            <div class="modal-content">
                <h4 class="center-align">Edit Notice</h4>
                <hr class="top_title">
                <div class="row">

                    {!! Form::open(array('class' => '', 'role' => 'form', 'method' => 'POST', 'autocomplete' => 'off', 'url' => 'apps/notice/edit')) !!}

                    {{ csrf_field() }}

                    <div class="row">


                        <input type="hidden" name="id" id="noticeId" value="">
                        <div class="input-field col m6 m6">
                            <i class="material-icons prefix">subtitles</i>
                            {{ Form::text('title', '', ['id' => 'noticeTitle', 'class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                        </div>

                        <div class="input-field col s6 m6">
                            {{ Form::checkbox('status', null, true, ['id' => 'noticeStatus'] ) }}
                            {{ Form::label('status', null, ['class' => 'control-label']) }}
                        </div>

                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">comment</i>
                            {{ Form::textarea('notice', '', ['id' => 'noticeMain', 'class' => 'materialize-textarea', 'row' => 1]) }}
                        </div>


                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Edit Notice', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                    </form>

                </div>
            </div>
        </div>
        <!-- End Modal For Edit a New Notice -->

        <!-- Modal For View Notice -->
        <div id="viewNotice" class="modal">
            <div class="modal-content">
                <input type="hidden" id="editNote">
                <div class="edit-delete">
                    <a href="#!" onclick="editNotice()"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Edit</a>
                    <a href="#!" onclick="deleteNotice()"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                </div>
                <h4 class="notice-title center-align"></h4>
                <hr class="top_title">
                <div class="row">
                    <p id="Notice"></p>
                </div>
            </div>
        </div>
        <!-- End # Modal For View Notice -->

    </div>

@endsection

@section('scripts')
    <script>
        function showNoteFunc(id) {
            $.get("notice/" + id, function (data) {
                $("#viewNotice #editNote").val(data.id);
                $("#viewNotice #deleteNote").val(data.id);
                $("#viewNotice .notice-title").text(data.title);
                $("#viewNotice #Notice").text(data.notice);
            });
        }
        function deleteNotice() {
            var nId = $('#editNote').val();
            $.get("notice/delete/" + nId, function (data) {
                if (data == 'deleted') {
                    console.log('Deleted');
                    location.reload();
                }
            });
        }
        function editNotice() {
            $('#viewNotice').closeModal({dismissible: false});

            var nId = $('#editNote').val();
            $.get("notice/" + nId, function (data) {
                $('#editNotice').openModal({
                    dismissible: !0,
                    opacity: .5,
                    in_duration: 300,
                    out_duration: 200,
                    ready: function () {
                    },
                    complete: function () {
                    }
                });
                $("#noticeTitle").val(data.title);
                $("#noticeId").val(data.id);
                $("#noticeMain").val(data.notice);
            });
        }
    </script>

@stop
