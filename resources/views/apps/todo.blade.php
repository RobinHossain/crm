@extends('layouts.app')
@section('css')
    <link href="{{url('/css/page/todo.css')}}" rel="stylesheet">
@stop

@section('content')
    <div class="container" ng-app="crmApplication">
        <div class="section" ng-controller="todoCtrl">

            <div class="row">
                <div class="col-md-12">
                    <h4 class="title center-align">Add you daily to do list </h4>
                    <hr class="top_title">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section id="todoapp" class="ng-scope">
                        <header id="header">
                            <form id="todo-form" ng-submit="addTodo()" class="ng-valid ng-dirty ng-submitted">
                                <input id="new-todo" placeholder="type here to add new todo item..." ng-model="newTodo" ng-disabled="saving" autofocus="" class="ng-valid ng-touched ng-dirty">
                            </form>
                        </header>



                        <section id="main" ng-show="todos" class="">



                            <input id="toggle-all" type="checkbox" ng-model="allChecked" class="ng-pristine ng-untouched ng-valid">
                            <ul id="todo-list">
                                <li ng-repeat="todo in todos | filter:statusFilter track by $index" ng-class="{completed: todo.completed, editing: todo == editedTodo}" class="ng-scope">
                                    <div class="view">
                                        <input class="filled-in toggle ng-valid ng-dirty ng-valid-parse ng-touched" id="single-todo-<% todo.id %>" type="checkbox" ng-model="todo.completed" ng-change="toggleCompleted(todo)">
                                        <label ng-dblclick="editTodo(todo)" for="single-todo-<% todo.id %>" class="ng-binding" ng-bind="todo.title"></label>
                                        <a class="destroy" ng-click="removeTodo(todo)"></a>
                                    </div>
                                    <form ng-submit="saveEdits(todo, 'submit')" class="ng-pristine ng-valid">
                                        <input class="edit ng-pristine ng-untouched ng-valid" ng-trim="false" ng-model="todo.title" todo-escape="revertEdits(todo)" ng-blur="saveEdits(todo, 'blur')" todo-focus="todo == editedTodo">
                                    </form>
                                </li>
                            </ul>
                        </section>
                        <footer id="todoFooter" ng-show="todos.length" class="">
					<span id="todo-count"><strong class="ng-binding">Total <span ng-bind="todos.length"></span> items</strong>
					</span>
                            <ul id="filters">
                                <li>
                                    <a ng-class="{selected: status == 'all'} " href="#!" ng-click="getLatestTodos()" class="selected">All</a>
                                </li>
                                <li>
                                    <a ng-class="{selected: status == 'active'}" href="#!" ng-click="getLatestTodosActive()">Active</a>
                                </li>
                                <li>
                                    <a ng-class="{selected: status == 'completed'}" href="#!" ng-click="getLatestTodosCompleted()">Completed</a>
                                </li>
                            </ul>
                            <button id="clear-completed" ng-click="clearCompletedTodos()" ng-show="completedCount" class="ng-hide">Clear completed</button>
                        </footer>
                    </section>
                </div>
            </div>

        </div>

         <div class="section">

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('/js/page/todo.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/angular/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/angular/app.js')}}"></script>
@stop