@extends('layouts.login')

<!-- Main Content -->
@section('content')

<form class="login-form" role="form" method="POST"  autocomplete="off" action="{{ url('/password/email') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="input-field col s12 center">
            <a id="logo-container" href="#!" class="brand-logo circle responsive-img valign profile-image-login"><img src="{{ url('imgs/logo.png') }}" alt="CRM Login" /> </a>
        </div>
        <div class="input-field col s12 center">
            <a id="logo-container" href="#!" class="brand-logo circle responsive-img valign profile-image-login">Reset Password </a>
        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>

    <div class="row margin {{ $errors->has('email') ? ' has-error' : '' }}">
        <div class="input-field col s12">
            <i class="fa fa-user prefix"></i>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
            <label for="email" class="">E-Mail Address</label>
            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <button type="submit" class="btn  waves-effect waves-light col s12 btn-primary">Send Password Reset Link</button>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12 center-align">
            <h5>Back to <a href="{{ url('/login') }}">login</a> page</h5>
        </div>
    </div>

</form>
@endsection
