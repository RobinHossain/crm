@extends('layouts.login')

@section('content')

    <form class="login-form" role="form" method="POST"  autocomplete="off" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="input-field col s12 center">
                <a id="logo-container" href="#!"
                   class="brand-logo circle responsive-img valign profile-image-login"><img
                            src="{{ url('imgs/logo.png') }}" alt="CRM Login"/> </a>
            </div>
        </div>

        <div class="row margin">
            <div class="input-field col s12 m6 l6 {{ $errors->has('fname') ? ' has-error' : '' }}">
                <i class="fa fa-user prefix"></i>
                <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required autofocus>
                <label for="fname" class="">First Name</label>
                @if ($errors->has('fname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input-field col s12 m6 l6 {{ $errors->has('lname') ? ' has-error' : '' }}">
                <i class="fa fa-user prefix"></i>
                <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required autofocus>
                <label for="lname" class="">Last Name</label>
                @if ($errors->has('lname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lname') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="row margin {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="input-field col s12">
                <i class="fa fa-envelope prefix"></i>
                <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                <label for="email" class="">Email Address</label>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="row">
            <div class="input-field col s12 m6 l6 {{ $errors->has('password') ? ' has-error' : '' }}">
                <i class="fa fa-lock prefix"></i>
                <input id="password" type="password" name="password" required>
                <label for="password" class="">Password</label>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input-field col s12 m6 l6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <i class="fa fa-lock prefix"></i>
                <input id="password_confirmation" type="password" name="password_confirmation" required>
                <label for="password_confirmation" class="">Password Confirmation</label>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="row">
            <div class="input-field col s12">
                <button type="submit" class="btn waves-effect waves-light col s12 light-green">Register</button>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 center-align">
                <h5>Already registered, <a href="{{ url('/login') }}">Login</a> Here</h5>
            </div>
        </div>

    </form>

@endsection
