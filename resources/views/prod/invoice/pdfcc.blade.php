<!DOCTYPE html><html lang=en><head><meta http-equiv=Content-Type content="text/html; charset=UTF-8"><meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1"><meta name=csrf-token content=QTenS5D8CAbrRFa5WRVLgAShmfWSevgoaRhBHEJg><title>CRM</title><link href=http://localhost:8000/css/app.css rel=stylesheet><link href=http://localhost:8000/css/page/invoice.css rel=stylesheet><style>@media  print {
            body * {
                visibility: hidden;
            }
            .noPrint {
                display: none !important;
            }

            #noPrint {
                display: none !important;
            }

            #page.print_area{
                visibility: hidden;
                display: block !important;
            }
            .pay-buttons{
                display: none;
            }
        }

        #page.print_area * {
            visibility: visible;
        }</style></head><body><div class=container ng-app=crmApp><div class="alert alert-info"><div id=page class=print_area><div class="status unpaid"><p>Paid</p></div><p class=recipient-address><strong>Invoiced To-</strong><br><strong>Sarwar Khanom</strong><br><abbr title=Phone>98237489834</abbr></p><abbr title=email>sarwar@khanom.com</abbr><p></p>dhaka - 23423<br>aAddres<br><h1>Invoice 26</h1><p class=terms><strong>Date Created: 2016-12-02 23:12:54</strong><br><strong>Payment due by 2016-12-14</strong></p><img src=http://localhost:8000/imgs/logo.png height=80 alt=Logo class=company-logo><p class=company-address>W3BD, World Width Web Development Bangladesh, X Plaza, House 3, Road 4, Section 2, Mirpur Dhaka, Bangladesh</p><table class="table table-striped"><thead><tr><th width=30px>S/L</th><th>Product/Service Description</th><th>Qty</th><th width=100px>Amount</th></tr></thead><tbody><tr><td>1</td><td>Product Any type of</td><td>1</td><td>3422</td></tr></tbody></table><div class=total-due><div class=total-heading><p>Amount Due</p></div><div class=total-amount><p>3422</p></div></div><hr></div></div></div><div class=cd-popup role=alert><div class=cd-popup-container><p class="card-title center-align">Are you sure you want to delete this client?</p><ul class=cd-buttons><li><a href=#! onclick='event.preventDefault(),document.getElementById("delete-form").submit()'>Yes</a></li><li><a href=#! class="client-popup-close popUpClose">No</a></li></ul><a href=#0 class="cd-popup-close img-replace popUpClose"></a></div></div><form id=delete-form action=http://localhost:8000/invoice/delete method=POST style="display: none"><input type=hidden name=_token value=QTenS5D8CAbrRFa5WRVLgAShmfWSevgoaRhBHEJg> <input type=hidden value="" name=id id=deleteID></form><footer class="page-footer orange"><div class=footer-copyright><div class=container>Made by <a class="orange-text text-lighten-3" href=http://w3bd.com>W3BD</a></div></div></footer><script src=http://localhost:8000/js/app.js></script><script>function deleteConfirmation(i){$("#deleteID").val(i),$(".cd-popup").addClass("is-visible")}$(document).ready(function(){$("#print_invoice").click(function(){window.print()})})</script></body></html>