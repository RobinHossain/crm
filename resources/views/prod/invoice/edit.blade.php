@extends('layouts.app') @section('css')<link href="{{ url('/css/page/invoice.css') }}" rel=stylesheet><style>@media print {
            body * {
                visibility: hidden;
            }
            .noPrint {
                display: none !important;
            }

            #noPrint {
                display: none !important;
            }

            #page.print_area{
                visibility: hidden;
                display: block !important;
            }
            .pay-buttons{
                display: none;
            }
        }

        #page.print_area * {
            visibility: visible;
        }</style>@stop @section('content')<div class=container ng-app=crmApp><div class=section><div class=container><div class="alert alert-info">{{--<strong>View Invoice Link: </strong><a href="http://demo.bdinfosys.com/bmsapp/cp/invoice.php?_cmd=139&amp;_token=7205367529" target=_blank>http://demo.bdinfosys.com/bmsapp/cp/invoice.php?_cmd=139&amp;_token=7205367529</a></div>--}}<div id=page class=print_area><div class="status {{ $invoice->invoice_status_id==2? 'paid': 'unpaid' }}"><p>Paid</p></div><div class=logo-invoice>@if($setting->logo) <img src="{{ asset('/imgs') }}/{{ $setting->logo }}" height=80 alt=Logo class=company-logo> @endif</div><p class=recipient-address><strong>Invoiced To-</strong><br><strong>{{ $invoice->account->fname }} {{ $invoice->account->lname }}</strong><br><abbr title=Phone>{{ $invoice->account->phone }}</abbr></p><abbr title=email>{{ $invoice->account->email }}</abbr><p></p>{{ $invoice->account->city }} - {{ $invoice->account->zipcode }}<br>{{ $invoice->account->address_1 }}<br><h1>Invoice {{ $invoice->id }}</h1><p class=terms><strong>Date Created: {{ $invoice->created_at }}</strong><br><strong>Payment due by {{ $invoice->due_date }}</strong></p><p class=company-address>{{ $setting->pay_address }}</p>{!! Form::open(array('class' => '', 'role' => 'form')) !!}<table class="table table-striped"><thead><tr><th width=30px>S/L</th><th>Product/Service Description</th><th>Qty</th><th width=100px>Amount</th></tr></thead><tbody> <?php $invoiceCount = 1; $product_count = 0; ?> @foreach($invoiceItems as $item) <input type=hidden name="product[{{ $product_count }}][id]" value="{{ $item->product_id }}"><tr><td>{{ $invoiceCount }}</td><td>{{ $item->product->name }}</td><td><input type=text name="product[{{ $product_count }}][quantity]" value="{{ $item->quantity }}"></td><td><input type=text name="product[{{ $product_count }}][price]" value="{{ $item->price }}"></td></tr> <?php $invoiceCount++; $product_count++; ?> @endforeach</tbody></table>{{--<a class="green button waves-effect waves-light btn light-blue lighten-1 white-text">Add More Product</a>--}}<br><div class=total-due><div class=total-heading><p>Amount Due</p></div><div class=total-amount><p>{{ $invoice->price }}</p></div></div><hr class=top_title><div class=row><div class="input-field col s12 m12"><i class="material-icons prefix">comment</i> {{ Form::textarea('note', $invoice->note, ['class' => 'materialize-textarea', 'row' => 1]) }} {{ Form::label('note', 'Invoice Note') }}</div></div><hr><div class=pay-buttons>{{ Form::submit('Submit', [ 'class' => 'green button waves-effect waves-light btn light-blue lighten-1 white-text']) }}</div>{!! Form::close() !!}</div>{{--<div class=page-shadow></div>--}}</div></div></div>@endsection @section('scripts')<script>$(document).ready(function(){$("#print_invoice").click(function(){window.print()})})</script>@stop