<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRM') }}</title>

    <!-- CSS  -->
    <!-- Styles -->
    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
    <link href="{{ url('/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Styles * End -->

    <!-- Page Specific Sytes -->
    @yield('css')
            <!-- Page Specific Sytes # End -->
</head>
<body>
<div class="navbar-fixed noPrint">
    <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container">
            @if($configurations->logo)
                <a id="logo-container" href="{{ url('/') }}" class="brand-logo center crm-logo"
                   style="background-image: url('{{ asset('imgs/') }}/logo.png');">
                </a>
            @elseif($configurations->company_name)
                <a id="logo-container" href="{{ url('/') }}" class="brand-logo center">

                    {{ $configurations->company_name }}

                    CRM

                </a>
            @else
                <a id="logo-container" href="{{ url('/') }}" class="brand-logo center">
                    <img src="{{ url('imgs/logo.png') }}" alt="CRM"/>

                    CRM

                </a>
            @endif

            <ul class="left hide-on-med-and-down">
                @if($configurations->manage_client == 1)
                <li><a href="#!" class="dropdown-button lft" data-activates="clients-dropdown"
                       data-beloworigin="true"><i class="fa fa-users"></i> Clients</a>
                </li>
                @endif
                    @if($configurations->manage_order_invoice == 1)
                <li><a href="#!" class="dropdown-button lft" data-activates="orders-dropdown" data-beloworigin="true"><i
                                class="fa fa-first-order" aria-hidden="true"></i> Orders</a>
                </li>
                    @endif
                    @if($configurations->manage_support == 1)
                <li><a href="#!" class="dropdown-button lft" data-activates="support-dropdown"
                       data-beloworigin="true"><i class="fa fa-life-ring" aria-hidden="true"></i> Support</a>
                </li>
                    @endif
                    @if($configurations->manage_finance == 1)
                <li><a href="#!" class="dropdown-button lft" data-activates="payments-dropdown" data-beloworigin="true"><i
                                class="fa fa-credit-card" aria-hidden="true"></i> Payments</a>
                </li>
                    @endif
            </ul>

            <!-- Sub Menues -->
            <ul id="clients-dropdown" class="dropdown-content">
                <li><a href="{{ url('/clients') }}">Manage Clients</a></li>
                <li><a href="{{ url('/clients/add') }}">Add new clients</a></li>
                <li><a href="{{ url('/clients/groups') }}">Client groups</a></li>
                <li><a href="{{ url('/clients/bulk-email') }}">Send bulk email</a></li>
            </ul>

            <ul id="orders-dropdown" class="dropdown-content">
                <li><a href="{{ url('/order') }}">All orders</a></li>
                <li><a href="{{ url('/order/add') }}">Add new order</a></li>
                <li><a href="{{ url('/order/pending') }}">Pending orders</a></li>
                <li><a href="{{ url('/order/active') }}">Active orders</a></li>
                <li><a href="{{ url('/order/fraud') }}">Fraud orders</a></li>
                <li><a href="{{ url('/order/cancel') }}">Cancelled orders</a></li>
            </ul>

            <ul id="support-dropdown" class="dropdown-content">
                <li><a href="{{ url('/faq') }}">FAQs</a></li>
                <li><a href="{{ url('/article') }}">Kb Articles</a></li>
                <li><a href="{{ url('/article/categories') }}">Kb Categories</a></li>
                <li><a href="{{ url('/ticket') }}">All tickets</a></li>
                <li><a href="{{ url('/ticket/active') }}">Active tickets</a></li>
                <li><a href="{{ url('/ticket/customer/replied') }}">Customer replied tickets</a></li>
                <li><a href="{{ url('/ticket/answered') }}">Answered tickets</a></li>
                <li><a href="{{ url('/ticket/close') }}">Closed tickets</a></li>
                <li><a href="{{ url('/ticket/add') }}">Create a new ticket</a></li>
                <li><a href="{{ url('/ticket/categories') }}">Ticket Categories</a></li>
            </ul>

            <ul id="payments-dropdown" class="dropdown-content">
                <li><a href="{{ url('transactions') }}">Transitions</a></li>
                <li><a href="{{ url('/invoices') }}">Invoices</a></li>
                <li><a href="{{ url('/invoices/add') }}">Create invoice</a></li>
                <li><a href="{{ url('/recurring') }}">Recurring invoices</a></li>
            </ul>
            <!-- Sub Menues ** END -->

            <ul class="right hide-on-med-and-down">
                <li><a href="#!" class="dropdown-button rht" data-activates="applications-dropdown"
                       data-beloworigin="true"><i class="fa fa-archive" aria-hidden="true"></i> Apps</a></li>
                @if($configurations->manage_finance == 1)
                <li><a href="#!" class="dropdown-button rht" data-activates="finance-dropdown"
                       data-beloworigin="true"><i class="fa fa-money" aria-hidden="true"></i> Finance</a>
                </li>
                @endif
                @if($configurations->manage_admin_option == 1)
                <li><a href="#!" class="dropdown-button rht" data-activates="setup-dropdown" data-beloworigin="true"><i
                                class="fa fa-wrench" aria-hidden="true"></i> Setup</a>
                </li>
                @endif
                <!--<li><a href="#!" class="dropdown-button rht" data-activates="tools-dropdown" data-beloworigin="true">Tools</a>-->
                <!--</li>-->
                <li class="account avatar dropdown-button rht" data-activates="account-dropdown"
                    data-beloworigin="true">
                    <a href="#!">
                        <div class="account-image"><img class="circle" width="38px" src="{{ Auth::user()->photo ? asset('imgs/account').'/'.Auth::user()->photo : asset('imgs/account-logo.png') }}" alt="">
                        </div>
                        <small>{{ Auth::user()->fname }} {{ Auth::user()->lname }}</small>
                    </a>
                </li>
            </ul>

            <!-- Sub Menues -->
            <ul id="applications-dropdown" class="dropdown-content">
                <li><a href="{{ url('apps/todo') }}">To Do</a></li>
                <li><a href="{{ url('apps/noticeboard') }}">Notice Board</a></li>
                {{--<li><a href="{{ url('apps/sticky') }}">Sticky notes</a></li>--}}
            </ul>

            <ul id="finance-dropdown" class="dropdown-content">
                <li><a href="{{ url('/accounts') }}">All accounts</a></li>
                <li><a href="{{ url('/transaction/income') }}">Income entries</a></li>
                <li><a href="{{ url('/revenue') }}">Revenue graph</a></li>
                <li><a href="{{ url('/transaction/expense') }}">Express entries</a></li>
                <li><a href="{{ url('/transaction/transfer') }}">Transfers</a></li>
                <li><a href="{{ url('/transactions') }}">Transactions</a></li>
                <li><a href="{{ url('/balance-sheet') }}">Balance sheet</a></li>
            </ul>

            <ul id="setup-dropdown" class="dropdown-content">
                <li><a href="{{ url('/products') }}">Products and services</a></li>
                <li><a href="{{ url('/product/categories') }}">Prod/Services Categories</a></li>
                <li><a href="{{ url('/administrator') }}">Administrators</a></li>
                <li><a href="{{ url('/administrator/roles') }}">Administrators role</a></li>
                <li><a href="{{ url('/payments') }}">Payments gatways</a></li>
                <li><a href="{{ url('/email-templates') }}">Email templates</a></li>
                <li><a href="{{ url('/support') }}">Support departments</a></li>
                <li><a href="{{ url('/settings') }}">System settings</a></li>
                {{--<li><a href="{{ url('/sliders') }}">Slider Setup(Client-panel)</a></li>--}}
                <li><a href="https://w3bd.com/public/doc/crm/master">Documentation</a></li>
            </ul>

            <ul id="tools-dropdown" class="dropdown-content">
                <li><a href="#!">System logs</a></li>
                <li><a href="#!">Send email logs</a></li>
                <li><a href="#!">System status</a></li>
                <li><a href="#!">Database cleanup</a></li>
                <li><a href="#!">Developer tools</a></li>
            </ul>
            <ul id="account-dropdown" class="dropdown-content">
                <!--<small>Acount Name</small>-->
                <li><a href="{{ url('administrator/edit') }}/{{ auth()->user()->id }}">Profile</a></li>
                <li><a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST"  autocomplete="off" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
            <!-- Sub Menues ** END -->


            <ul class="side-nav" id="mobile-menu">
                <li><a href="#!" class="dropdown-button lft"><i class="fa fa-users"></i> Clients</a>
                    <ul id="clients-dropdown">
                        <li><a href="{{ url('/clients') }}">Manage Clients</a></li>
                        <li><a href="{{ url('/clients/add') }}">Add new clients</a></li>
                        <li><a href="{{ url('/clients/groups') }}">Client groups</a></li>
                        <li><a href="{{ url('/clients/bulk-email') }}">Send bulk email</a></li>
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button lft"><i class="fa fa-first-order"></i> Orders</a>
                    <ul id="orders-dropdown">
                        <li><a href="{{ url('/order') }}">All orders</a></li>
                        <li><a href="{{ url('/order/add') }}">Add new order</a></li>
                        <li><a href="{{ url('/order/pending') }}">Pending orders</a></li>
                        <li><a href="{{ url('/order/active') }}">Active orders</a></li>
                        <li><a href="{{ url('/order/fraud') }}">Fraud orders</a></li>
                        <li><a href="{{ url('/order/cancel') }}">Cancelled orders</a></li>
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button lft"><i class="fa fa-life-ring"></i> Support</a>
                    <ul id="support-dropdown">
                        <li><a href="{{ url('/faq') }}">FAQs</a></li>
                        <li><a href="{{ url('/article') }}">Kb Articles</a></li>
                        <li><a href="{{ url('/article/categories') }}">Kb Categories</a></li>
                        <li><a href="{{ url('/ticket') }}">All tickets</a></li>
                        <li><a href="{{ url('/ticket/active') }}">Active tickets</a></li>
                        {{--<li><a href="#!">Replied tickets</a></li>--}}
                        {{--<li><a href="#!">Answer ticket</a></li>--}}
                        <li><a href="{{ url('/ticket/close') }}">Closed tickets</a></li>
                        <li><a href="{{ url('/ticket/add') }}">Create New ticket</a></li>
                        <li><a href="{{ url('/ticket/categories') }}">Ticket Categories</a></li>
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button lft"><i class="fa fa-credit-card"></i> Payments</a>
                    <ul id="payments-dropdown">
                        <li><a href="{{ url('transactions') }}">Transitions</a></li>
                        <li><a href="{{ url('/invoices') }}">Invoices</a></li>
                        <li><a href="{{ url('/invoices/add') }}">Create invoice</a></li>
                        <li><a href="{{ url('/recurring') }}">Recurring invoices</a></li>
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button"><i class="fa fa-archive" aria-hidden="true"></i> Apps</a>
                    <ul id="applications-dropdown">
                        <li><a href="{{ url('apps/todo') }}">To Do</a></li>
                        <li><a href="{{ url('apps/noticeboard') }}">Notice Board</a></li>
                        {{--<li><a href="{{ url('apps/sticky') }}">Sticky notes</a></li>--}}
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button"><i class="fa fa-money" aria-hidden="true"></i> Finance</a>
                    <ul id="finance-dropdown">
                        <li><a href="{{ url('/accounts') }}">All accounts</a></li>
                        <li><a href="{{ url('/transaction/income') }}">Income entries</a></li>
                        <li><a href="{{ url('/revenue') }}">Revenue graph</a></li>
                        <li><a href="{{ url('/transaction/expense') }}">Express entries</a></li>
                        <li><a href="{{ url('/transaction/transfer') }}">Transfers</a></li>
                        <li><a href="{{ url('/transactions') }}">Transactions</a></li>
                        <li><a href="{{ url('/balance-sheet') }}">Balance sheet</a></li>
                    </ul>
                </li>
                <li><a href="#!" class="dropdown-button"><i class="fa fa-wrench" aria-hidden="true"></i> Setup</a>
                    <ul id="setup-dropdown">
                        <li><a href="{{ url('/products') }}">Products and services</a></li>
                        <li><a href="{{ url('/administrator') }}">Administrators</a></li>
                        <li><a href="{{ url('/administrator/roles') }}">Administrators role</a></li>
                        <li><a href="{{ url('/payments') }}">Payments gatways</a></li>
                        <li><a href="{{ url('/email-templates') }}">Email templates</a></li>
                        <li><a href="{{ url('/support') }}">Support departments</a></li>
                        <li><a href="{{ url('/settings') }}">System settings</a></li>
                        {{--<li><a href="{{ url('/sliders') }}">Slider Setup(Client-panel)</a></li>--}}
                        <li><a href="https://w3bd.com/public/doc/crm/master">Documentation</a></li>
                    </ul>
                </li>
                <li class="account avatar">
                    <a href="{{ url('administrator/edit') }}/{{ auth()->user()->id }}">
                        <div class="account-image"><img class="circle" width="38px"
                                                        src="https://randomuser.me/api/portraits/thumb/women/19.jpg"
                                                        alt="">
                        </div>
                    </a>
                    <ul id="account-dropdown">
                        <li><a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        </li>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"  autocomplete="off" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>

            </ul>


            <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>

        </div>
    </nav>
</div>


@if(Session::has('message'))
    <div class="container">
        <div id="card-alert" class="card green">
            <div class="card-content white-text">
                <p><i class="mdi-navigation-check"></i> {{ Session::get('message') }}.</p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div class="container">
        <div id="card-alert" class="card red">
            <div class="card-content white-text">
                <p><i class="material-icons">error</i> {{ Session::get('error') }}.</p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
@endif

@yield('content')


<footer class="page-footer orange">
    <div class="footer-copyright">
        <div class="container">
            2017 &copy; All Rights Reserved
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="{{url('js/app.js')}}"></script>
<!--  Scripts-- * End JS -->
@yield('scripts')


</body>
</html>
