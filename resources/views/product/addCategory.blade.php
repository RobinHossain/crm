@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif



            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h3 class="title">{{ $title }}</h3>

                        <p class="caption">Add a new category with following input field.</p>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                    <?php if(isset($category) && !empty($category)){ ?>
                    {!! Form::model($category, array('class' => '', 'role' => 'form', 'autocomplete' => 'off')) !!}
                    <?php }else{?>
                    {!! Form::open(array('class' => '', 'role' => 'form', 'autocomplete' => 'off')) !!}
                    <?php } ?>
                    {{ csrf_field() }}


                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">shopping_basket</i>
                            {{ Form::text('name', null, ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('name', 'Category Name') }}
                        </div>

                    </div>


                    <div class="row">
                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>

                </form>

            </div>




        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{url('/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/page/product.js')}}"></script>
@stop