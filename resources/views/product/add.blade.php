@extends('layouts.app')

@section('css')
<style>
    #addVariation span.add-variation{
        cursor: pointer;
        font-size: 20px;
    }
</style>

@stop

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif



            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h3 class="title">{{ $title }}</h3>

                        <p class="caption">Add a new product with following input field.</p>
                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                {{--<form class="col s12" id="account-form" action="{{ url('/product/add') }}" method="POST"  autocomplete="off">--}}

                    <?php if(isset($product) && !empty($product)){ ?>
                    {!! Form::model($product, array('class' => '', 'role' => 'form', 'autocomplete' => 'off')) !!}
                    <?php }else{?>
                    {!! Form::open(array('class' => '', 'role' => 'form', 'autocomplete' => 'off')) !!}
                    <?php } ?>
                    {{ csrf_field() }}


                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">shopping_basket</i>
                            {{ Form::text('name', null, ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('name', 'Product/Service Name') }}
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">group</i>
                            {{ Form::select('product_category_id', $categories, null, ['placeholder' => 'Select A Category']) }}
                        </div>
                    </div>

                    <div id="addVariation">
                    <div class="row center-align">
                        <div class="col s12 m12">
                            <a class="btn-floating btn-large waves-effect waves-light red" onclick="addVariation()"><i class="material-icons">add</i></a> &nbsp; <span class="add-variation" onclick="addVariation()">Add Custom Variation</span>
                        </div>
                    </div>
                    </div>

                    @if( isset($product->variation) && $product->variation !== 'null' && count($product->variation))
                    <?php $field_count = 1; $current_field = count($product->variation); ?>

                    @foreach($product->variation as $variation)
                        <div id="field_<?php echo $field_count; ?>" class="row">
                            <div class="input-field col s12 m6">
                                <input type="text" name="product_variation[<?php echo $field_count; ?>][name]" id="variation_name_<?php echo $field_count; ?>" value="{{$variation['name']}}" /><label for="variation_name_<?php echo $field_count; ?>">Product Variation <?php echo $field_count; ?></label></div>
                            <div class="input-field col s12 m6">
                                <input type="text" name="product_variation[<?php echo $field_count; ?>][price]" id="variation_price_<?php echo $field_count; ?>" value="{{$variation['price']}}" /><label for="variation_price_<?php echo $field_count; ?>">Product Variation <?php echo $field_count; ?> Price</label></div>
                            <a onclick="removeVariation(<?php echo $field_count; ?>)" href="javascript:void(0);" class="btn-floating red"><i class="material-icons">delete</i></a>
                        </div>
                        <?php $field_count++; ?>
                    @endforeach
                    @else
                        <?php $current_field = 0; ?>
                    @endif

                    <div class="row">
                        <div class="input-field col s12 m12">
                            <h5>Product/Service Details</h5>
                            {{ Form::textarea('details', null, ['id' => 'details']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">attach_money</i>
                            {{ Form::text('price', null, ['class' => 'validate']) }}
                            {{ Form::label('price', 'Price') }}
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">loyalty</i>
                            {{ Form::text('quantity', null, ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('quantity', 'Quantity') }}
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input type="checkbox" id="status" name="status" @if (isset($product->status)){{ $product->status == 1?'checked':'' }} @endif />
                            <label for="status">Product/Service Status</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">spellcheck</i>
                            <input type="checkbox" id="show_client" name="show_client" @if (isset($product->show_client)) {{ $product->show_client == 1?'checked':'' }} @endif />
                            <label for="show_client">Show Product/Service On Client Page</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>

                </form>

            </div>




        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{url('/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/page/product.js')}}"></script>
    <script>
        var field_count = '<?php echo $current_field; ?>';
        field_count = parseInt(field_count);
        var max_fields = 10;
        function addVariation(){
            if(field_count < max_fields){ //max input box allowed
                field_count++; //text box increment
                $('#addVariation').append('<div id="field_'+ field_count +'" class="row">' +
                        '<div class="input-field col s12 m6">' +
                        '<input type="text" name="product_variation['+ field_count +'][name]" id="variation_name_'+field_count+'" /><label for="variation_name_'+field_count+'">Product Variation '+field_count+'</label></div>' +
                        '<div class="input-field col s12 m6">' +
                        '<input type="text" name="product_variation['+ field_count +'][price]" id="variation_price_'+field_count+'"/><label for="variation_price_'+field_count+'">Product Variation '+field_count+' Price</label></div>' +
                        '<a onclick="removeVariation(field_count)" href="javascript:void(0);" class="btn-floating red"><i class="material-icons">delete</i></a>' +
                        '</div>'); //add input box
            }
        }
        function removeVariation(id){
            $('#field_'+id).remove(); field_count--;
        }

    </script>
@stop