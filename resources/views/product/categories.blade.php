@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col s12">
                <div class="header">
                    <h5 class="title center-align">{{ $title }}</h5>
                </div>
            </div>
        </div>

        <hr class="top_title">

        <div class="row full-width right-align">

            <a href="{{url('product/category/add')}}" class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon"><i class="fa fa-pencil-square-o white-text" aria-hidden="true"></i> Add a new category</a>

        </div>
        <div class="section">

            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Slug</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->slug }}</td>
                        <td>
                            <a href="{{url('product/category/edit')}}/{{$category->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="#!" onclick="deleteConfirmation({{$category->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></a>


                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>


        </div>


        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p class="card-title center-align">Are you sure you want to delete this product?</p>
                <ul class="cd-buttons">
                    <li><a href="#!" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Yes</a></li>
                    <li><a href="#!" class="client-popup-close popUpClose">No</a></li>
                </ul>
                <a href="#0" class="cd-popup-close img-replace popUpClose"></a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <form id="delete-form" action="{{ url('/product/category/delete') }}" method="POST"  autocomplete="off" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id" id="deleteID">
        </form>

        <div class="section">


        </div>
    </div>
@endsection

@section('scripts')
    <script>

        function deleteConfirmation(id) {
            $('#deleteID').val(id);
            $('.cd-popup').addClass('is-visible');
        }

    </script>

@endsection
