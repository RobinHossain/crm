@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="section">

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div id="card-alert" class="card red">
                                    <div class="card-content white-text">
                                        <p><i class="mdi-alert-error"></i> {{ $error }}</p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endif


            </div>

            <div class="row">
                <div class="col s12">
                    <div class="header">
                        <h3 class="title">{{ $title }}</h3>

                    </div>
                </div>
            </div>

            <hr class="top_title">

            <div class="row">
                <form class="col s12" id="account-form" action="{{ url('/account/add') }}" method="POST"  autocomplete="off">
                    {{ csrf_field() }}


                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_circle</i>
                            {{ Form::text('fname', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('fname', 'First Name') }}
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_circle</i>
                            {{ Form::text('lname', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('lname', 'Last Name') }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">phone</i>
                            {{ Form::text('phone', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('phone', 'Phone') }}
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">email</i>
                            {{ Form::text('email', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('email', 'Email Address') }}
                        </div>
                    </div>

                    <div class="row">


                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_balance_wallet</i>
                            {{ Form::select('account_type_id', $account_types, null, ['placeholder' => 'Select Account Type']) }}
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">supervisor_account</i>
                            {{ Form::select('group_id', $account_groups, null, ['placeholder' => 'Select Custom Group']) }}
                        </div>
                    </div>

                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">work</i>
                            {{ Form::text('company', '', ['class' => 'validate']) }}
                            {{ Form::label('company', 'Company Name') }}
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">location_on</i>
                            {{ Form::text('city', '', ['class' => 'validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('city', 'City') }}
                        </div>


                    </div>

                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">home</i>
                            {{ Form::textarea('address_1','' ,['class' => 'materialize-textarea validate', 'required'=> '', 'aria-required' => 'true']) }}
                            {{ Form::label('city', 'Address One') }}
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">work</i>
                            {{ Form::textarea('address_2','' ,['class' => 'materialize-textarea']) }}
                            {{ Form::label('city', 'Address Two') }}
                        </div>


                    </div>

                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">location_city</i>
                            {{ Form::text('state', '', ['class' => 'validate']) }}
                            {{ Form::label('state', 'State') }}
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">my_location</i>
                            {{ Form::text('zipcode', '', ['class' => 'validate']) }}
                            {{ Form::label('zipcode', 'Zip Code') }}
                        </div>

                    </div>


                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">terrain</i>
                            {{ Form::select('country_id', $countries, null, ['placeholder' => 'Select Country' ,'required'=> '', 'aria-required' => 'true']) }}
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">add_alert</i>
                            <input type="checkbox" id="notification_status" name="notification_status" value="1"/>
                            <label for="notification_status">Turn On Notification</label>
                        </div>


                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6">
                            {{ Form::submit('Submit', ['class' => 'btn waves-effect light-blue lighten-1']) }}
                        </div>
                    </div>


                </form>
            </div>


        </div>
    </div>
@endsection
