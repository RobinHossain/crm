@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h5 class="card-title center-align">{{ $title }}</h5>

            </div>
        </div>


        <hr class="top_title">
        <div class="row right-align">
            <a href="{{ url('clients/add') }}" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i
                        class="fa fa-user-plus" aria-hidden="true"></i> Add a new client</a>
        </div>

        <div class="section">

            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Created Date</td>
                    <td>Balance</td>

                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($clients as $client)
                    <tr>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->fname }} {{ $client->lname }}</td>
                        <td> {{ date('F d, Y', strtotime($client->created_at)) }}</td>
                        <td>{{ $client->balance }}</td>

                        <td>
                            <a href="{{url('clients/edit')}}/{{$client->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="#!" onclick="deleteConfirmation({{$client->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>


        </div>

        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p class="card-title center-align">Are you sure you want to delete this client?</p>
                <ul class="cd-buttons">
                    <li><a href="#!" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Yes</a></li>
                    <li><a href="#!" class="client-popup-close popUpClose">No</a></li>
                </ul>
                <a href="#0" class="cd-popup-close img-replace popUpClose"></a>
            </div> <!-- cd-popup-container -->
        </div> <!-- cd-popup -->
        <form id="delete-form" action="{{ url('/clients/delete') }}" method="POST"  autocomplete="off" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id" id="deleteOrderId">
        </form>


    </div>
@endsection

@section('scripts')
    <script>

        function deleteConfirmation(id) {
            $('#deleteOrderId').val(id);
            $('.cd-popup').addClass('is-visible');
        }

    </script>

@endsection