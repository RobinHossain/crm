@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col s12">
                <div class="header">
                    <h5 class="title center-align">{{ $title }}</h5>
                </div>
            </div>
        </div>

        <hr class="top_title">

        <div class="row full-width right-align">

            <a href="{{ url('account/add') }}"
               class="waves-effect waves-light btn light-blue lighten-1 white-text table-head-icon tooltipped"
               data-position="bottom" data-delay="50"
               data-tooltip="Add a new recurring invoice entry on the current list"><i
                        class="fa fa-plus-circle white-text" aria-hidden="true"></i> Add A New Account</a>



        </div>
        <div class="section">

            <table id="Dtable" class="display responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Created Date</td>
                    <td>Balance</td>
                    <td>Type</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>

                @foreach ($accounts as $account)
                    <tr>
                        <td>{{ $account['id'] }}</td>
                        <td>{{ $account['name'] }}</td>
                        <td> {{ date('F d, Y', strtotime($account['created_at'])) }}</td>
                        <td>{{ $account['balance'] }}</td>
                        <td>{{ $account['account_type'] }}</td>
                        <td>
                            <a href="{{url('account-activity')}}/{{$account['id']}}" class="waves-effect waves-light btn light-blue lighten-1 white-text"><i class="fa fa-list white-text" aria-hidden="true"></i> &nbsp; Manage</a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>



        </div>
        <br><br>

        <div class="section">

        </div>
    </div>
@endsection
