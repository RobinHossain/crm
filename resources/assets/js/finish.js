$(function() {

    var selectBatch = $('select');
    if (selectBatch.length > 0) {
        selectBatch.each(function () {
            var element = $(this);
            element.find("option").eq(0).attr('disabled', 'disabled');
        });
    }

})